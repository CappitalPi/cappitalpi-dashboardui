import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { AuthGuard } from './guards/auth.guard';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: ''
    }, 
    canActivate: [AuthGuard],
    children: [
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      },
      {
        path: 'customer/customer-search',
        loadChildren: () => import('./views/customer/customer.module').then(m => m.CustomerModule)
      },
      {
        path: 'supplier/supplier-search',
        loadChildren: () => import('./views/supplier/supplier.module').then(m => m.SupplierModule)
      },
      {
        path: 'employee/employee-search',
        loadChildren: () => import('./views/employee/employee.module').then(m => m.EmployeeModule)
      },
      {
        path: 'inscription-type/inscription-type-search',
        loadChildren: () => import('./views/inscription-type/inscription-type.module').then(m => m.InscriptionTypeModule)
      },
      {
        path: 'user/user-search',
        loadChildren: () => import('./views/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'financier/account/account-search',
        loadChildren: () => import('./views/financier/account/account.module').then(m => m.AccountModule)
      },
      {
        path: 'financier/chart-of-account/chart-of-account-search',
        loadChildren: () => import('./views/financier/chart-of-account/chart-of-account.module').then(m => m.ChartOfAccountModule)
      },
      {
        path: 'financier/transaction/transaction-search',
        loadChildren: () => import('./views/financier/transaction/transaction.module').then(m => m.TransactionModule)
      },
      {
        path: 'financier/account-payable/account-payable-search',
        loadChildren: () => import('./views/financier/account-payable/account-payable.module').then(m => m.AccountPayableModule)
      },
      {
        path: 'financier/account-receivable/account-receivable-search',
        loadChildren: () => import('./views/financier/account-receivable/account-receivable.module').then(m => m.AccountReceivableModule)
      },
      {
        path: 'settings/auto-account-receivable/auto-account-receivable-search',
        loadChildren: () => import('./views/settings/auto-account-receivable/auto-account-receivable.module').then(m => m.AutoAccountReceivableModule)
      },
      {
        path: 'settings/auto-account-payable/auto-account-payable-search',
        loadChildren: () => import('./views/settings/auto-account-payable/auto-account-payable.module').then(m => m.AutoAccountPayableModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
