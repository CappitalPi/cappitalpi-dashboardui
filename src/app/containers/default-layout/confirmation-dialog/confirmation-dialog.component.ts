import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html'
})
export class ConfirmationDialogComponent implements OnInit {

  @Input() public message: string;
  @Input() public title: string;
  @Input() public okButtonLabel: string;
  @Input() public closeButtonLabel: string;
  public isOk : boolean;

  constructor(public modalRef: BsModalRef) {
  }

  ngOnInit(): void {
    this.isOk = false;
  }

  public ok(): void{
    this.isOk = true;
    this.close();
  }

  public close(): void  {
    this.modalRef.content.isOk = this.isOk;
    this.modalRef.hide();
  }

}