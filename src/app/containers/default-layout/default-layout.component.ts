import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { INavData } from '@coreui/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../service/auth/auth-service';
import { ROUTE_LOGIN } from '../../service/navigation-constants';
import { navItems } from '../../_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {

  public sidebarMinimized = false;
  public username: string;
  private navItems = navItems;

  constructor(private authService: AuthService, private router: Router, public translateService: TranslateService) {
    this.username = authService.currentUserValue.username.toUpperCase();
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  public logout(): void {
    this.authService.logout()
    .subscribe(
      () => {
        this.authService.removeCredentials();
        this.router.navigate([`/${ROUTE_LOGIN}`]);
      },
    err => {
      console.debug(`Error ${err}`);
    });
  }
  public getNavItems(): INavData[] {
    this.navItems.forEach(element => {
      element.name = this.translateService.instant(element.name);
      if (element.children?.length > 0) {
        element.children.forEach(children => {
          if (children.name?.length > 0) {
            children.name = this.translateService.instant(children.name);
          }
        });
      }
    });
    return this.navItems;
  }
}
