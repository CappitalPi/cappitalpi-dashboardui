import { NgModule } from '@angular/core';
import { SingleClickDirective } from './button/single-click.directive';
import { CurrencyInputDirective } from './currency/currency-pipe.directive';
@NgModule({
	declarations: [CurrencyInputDirective, SingleClickDirective],
	imports: [],
	exports: [CurrencyInputDirective, SingleClickDirective]
})
export class DirectivesModule {}
