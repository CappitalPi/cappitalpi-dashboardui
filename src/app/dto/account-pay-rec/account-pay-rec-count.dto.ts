export class AccountPayRecCountDto {

    accountReceivableOverdue: number;
    accountPayableOverdue: number;
    accountReceivableNext7Days: number;
    accountPayableNext7Days: number;

}

