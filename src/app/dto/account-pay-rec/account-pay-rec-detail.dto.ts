import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";
import { PersonSearchDetailDto } from "../person/person-search-detail.dto";

export class AccountPayRecDetailDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    person: PersonSearchDetailDto;
    chartOfAccount: ChartOfAccountDto;
    type: string;
    description: string;
    documentNumber: string;
    barCode: string;
    issueDate: Date;
    dueDate: Date;
    amount: number;
    discount: number;
    interest: number;
    amountPaid: number;
    balance: number;
    amountToPay: number;
    status: string;
    paymentDate: Date;
    createdDate: Date;
    updatedDate: Date;

}

