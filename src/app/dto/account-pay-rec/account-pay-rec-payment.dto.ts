import { PaymentMethod } from "../../enum/payment-method.enum";

export class AccountPayRecPaymentDto {

    accountPayRecUid: string;
    registerUid: string;
    cashAccountUid: string;
    notes: string;
    documentNumber: string;
    checkNumber: string;
    discount: number;
    interest: number;
    amount: number;
    paymentMethod: PaymentMethod;
    paymentDate: Date;

}

