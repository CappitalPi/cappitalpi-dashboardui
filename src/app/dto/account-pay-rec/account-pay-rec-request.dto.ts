export class AccountPayRecRequestDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    personUid: string;
    chartOfAccountUid: string;
    description: string;
    documentNumber: string;
    barCode: string;
    issueDate: Date;
    dueDate: Date;
    amount: number;
    discount: number;
    interest: number;
    balance: number;
    amountPaid: number;
    amountToPay: number;
    updatedDate: Date;

}

