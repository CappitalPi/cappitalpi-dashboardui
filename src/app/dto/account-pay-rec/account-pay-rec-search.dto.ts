import { AccountPayRecStatus } from "../../enum/account-pay-rec-status.enum";
import { PersonSearchDetailDto } from "../person/person-search-detail.dto";

export class AccountPayRecSearchDto {

    uid: string;
    registerUid: string;
    appAccountUid: string;
    code: number;
    person: PersonSearchDetailDto;
    chartOfAccountUid: string
    description: string;
    type: string;
    issueDate: Date;
    dueDate: Date;
    amount: number;
    discount: number;
    interest: number;
    amountPaid: number;
    balance: number;
    amountToPay: number;
    status: AccountPayRecStatus;
    paymentDate: Date;
    createdDate: Date;
    updatedDate: Date;
}

