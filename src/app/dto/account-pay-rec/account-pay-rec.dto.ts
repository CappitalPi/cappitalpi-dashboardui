import { AccountPayRecPaymentDto } from "./account-pay-rec-payment.dto";

export class AccountPayRecDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    personUid: string;
    chartOfAccountUid: string;
    type: string;
    description: string;
    documentNumber: string;
    barCode: string;
    issueDate: Date;
    dueDate: Date;
    amount: number;
    discount: number;
    interest: number;
    amountPaid: number;
    balance: number;
    amountToPay: number;
    status: string;
    paymentDate: Date;
    createdDate: Date;
    updatedDate: Date;
    payments: AccountPayRecPaymentDto[];

}

