import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";
import { PersonSearchDetailDto } from "../person/person-search-detail.dto";
import { CostType } from "./cost-type.enum";
import { FrequencyType } from "./frequency-type.enum";
import { RepeatType } from "./repeat-type.enum";

export class AutoAccountPayRecDetailDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    person: PersonSearchDetailDto;
    chartOfAccount: ChartOfAccountDto;
    type: string;
    frequencyType: FrequencyType;
    repeatType: RepeatType;
    costType: CostType;
    description: string;
    documentNumber: string;
    amount: number;
    discount: number;
    interest: number;
    createdDate: Date;
    updatedDate: Date;
    frequencyStartDate: Date;
    frequencyEndDate: Date;
    latestDate: Date;
    frequencyAmount: number;
    skipWeekend: boolean;
    disabled: boolean;

}

