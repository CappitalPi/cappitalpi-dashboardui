import { CostType } from "./cost-type.enum";
import { FrequencyType } from "./frequency-type.enum";
import { RepeatType } from "./repeat-type.enum";

export class AutoAccountPayRecRequestDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    personUid: string;
    chartOfAccountUid: string;
    frequencyType: FrequencyType;
    repeatType: RepeatType;
    costType: CostType;
    description: string;
    documentNumber: string;
    amount: number;
    discount: number;
    interest: number;
    frequencyStartDate: Date;
    frequencyEndDate: Date;
    frequencyAmount: number;
    sunday: boolean;
    monday: boolean;
    tuesday: boolean;
    wednesday: boolean;
    thursday: boolean;
    friday: boolean;
    saturday: boolean;
    skipWeekend: boolean;
    disabled: boolean;

}

