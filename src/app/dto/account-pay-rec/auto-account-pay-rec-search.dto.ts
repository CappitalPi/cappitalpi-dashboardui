import { PersonSearchDetailDto } from "../person/person-search-detail.dto";
import { CostType } from "./cost-type.enum";
import { FrequencyType } from "./frequency-type.enum";
import { RepeatType } from "./repeat-type.enum";

export class AutoAccountPayRecSearchDto {

    uid: string;
    registerUid: string;
    appAccountUid: string;
    code: number;
    person: PersonSearchDetailDto;
    chartOfAccountUid: string
    type: string;
    frequencyType: FrequencyType;
    repeatType: RepeatType;
    costType: CostType;
    description: string;
    amount: number;
    discount: number;
    interest: number;
    balance: number;
    createdDate: Date;
    updatedDate: Date;
    frequencyStartDate: Date;
    frequencyEndDate: Date;
    latestDate: Date;
    frequencyAmount: number;
    skipWeekend: boolean;
    disabled: boolean;
}

