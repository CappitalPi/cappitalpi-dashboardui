import { CostType } from "./cost-type.enum";
import { FrequencyType } from "./frequency-type.enum";
import { RepeatType } from "./repeat-type.enum";

export class AutoAccountPayRecDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    personUid: string;
    chartOfAccountUid: string;
    type: string;
    frequencyType: FrequencyType;
    repeatType: RepeatType;
    costType: CostType;
    description: string;
    documentNumber: string;
    amount: number;
    discount: number;
    interest: number;
    createdDate: Date;
    updatedDate: Date;
    frequencyStartDate: Date;
    frequencyEndDate: Date;
    latestDate: Date;
    frequencyAmount: number;
    sunday: boolean;
    monday: boolean;
    tuesday: boolean;
    wednesday: boolean;
    thursday: boolean;
    friday: boolean;
    saturday: boolean;
    skipWeekend: boolean;
    disabled: boolean;

}

