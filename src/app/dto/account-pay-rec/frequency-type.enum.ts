export enum FrequencyType {
    DAY="DAY",
    WEEK="WEEK",
    MONTH="MONTH",
    YEAR="YEAR"
}
