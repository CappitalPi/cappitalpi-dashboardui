export enum RepeatType {
    BY_AMOUNT_OF_DAYS="BY_AMOUNT_OF_DAYS",
    BY_DUE_DATE="BY_DUE_DATE"
}
