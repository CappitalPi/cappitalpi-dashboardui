export class AccountSearchDto {

    uid: string;
    code: number;
    type: string;
    name: string;
    branchNumber: string;
    accountNumber: string;
    institution: string;
    disabled: boolean;
    updateBalanceDisabled: boolean;
}

