export class AccountDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    type: string;
    name: string;
    description: string;
    branchNumber: string;
    accountNumber: string;
    institution: string;
    startBalance: number;
    balance: number;
    creditLimit: number;
    disabled: boolean;
    createdDate: Date;
    updatedDate: Date;
    updateBalanceDisabled: boolean;
}

