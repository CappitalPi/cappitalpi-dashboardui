export class AddressTypeDto {
    uid: string;
    code: number;
    name: string;
    description: string;
}

