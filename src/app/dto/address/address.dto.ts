import { PersonDto } from "../person/person.dto";
import { AddressTypeDto } from "./address-type.dto";
import { CityDto } from "./city.dto";
import { CountryDto } from "./country.dto";
import { StateDto } from "./state.dto";

export class AddressDto {
    uid: string;
    code: number;
    type: AddressTypeDto;
    person: PersonDto;
    addressLine1: string;
    addressLine2: string;
    city: CityDto;
    state: StateDto;
    country: CountryDto;
    postalCode: string;
    notes: string;
    latitude: number;
    longitude: number;
    preferred: boolean;
    createdDate: Date;
    updatedDate: Date;
}

