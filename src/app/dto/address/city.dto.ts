import { StateDto } from "./state.dto";

export class CityDto {
    uid: string;
    code: number;
    state: StateDto;
    name: string;
}

