export class CountryDto {
    uid: string;
    code: number;
    name: string;
}

