import { CountryDto } from "./country.dto";

export class StateDto {
    uid: string;
    code: number;
    country: CountryDto;
    name: string;
    stateCode: string;
}

