
export class ChartOfAccountDto {
    uid: string;
    registerUid: string;
    appAccountUid: string;
    code: number;
    accountNumber: string;
    parent: ChartOfAccountDto;
    name: string;
    description: string;
    type: string;
    category: string;
    lastLevel: boolean;
    disabled: boolean;
}

