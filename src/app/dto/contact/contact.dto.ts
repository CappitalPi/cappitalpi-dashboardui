import { PersonDto } from "../person/person.dto";

export class ContactDto {
    uid: string;
    code: number;
    person: PersonDto;
    name: string;
    role: string;
    notes: string;
    disabled: boolean;
}

