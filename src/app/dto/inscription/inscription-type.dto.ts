export class InscriptionTypeDto {
    uid: string;
    registerUid: string
    code: number;
    name: string;
    description: string;
    format: string;
    disabled: boolean;
}

