import { PersonDto } from "../person/person.dto";
import { InscriptionTypeDto } from "./inscription-type.dto";

export class InscriptionDto {
    uid: string;
    code: number;
    type: InscriptionTypeDto;
    person: PersonDto;
    value: string;
    description: string;
    inscriptionDate: Date;
}

