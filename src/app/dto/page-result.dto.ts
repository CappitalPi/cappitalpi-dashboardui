export class PageResultDto<T> {

    currentPage: number;
    totalPages: number;
    totalElements: number;
    content: Array<T>;
    balances: Map<String, number>;
}