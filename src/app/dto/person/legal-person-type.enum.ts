export enum LegalPersonType {
    HEAD_OFFICE = "H",
    BRANCH_OFFICE = "B",
    WAREHOUSE = "W" 
}
