import { PersonDetailDto } from "./person-detail.dto";

export class LegalPersonDto extends PersonDetailDto {
    type: string;
    businessName: string;
    startedDate: Date;
}

