export enum MaritalStatus {
    SINGLE = "S",
    MARRIED = "M",
    DIVORCED = "D",
    COMMON_LAW = "C"
}
