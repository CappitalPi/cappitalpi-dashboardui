import { PhoneDto } from "../phone/phone.dto";

export class PersonSearchDetailDto {

    uid: string;
    code: number;
    name: string;
    businessName: string;
    phones: PhoneDto[]
    createdDate: Date;
    updatedDate: Date;
    disabledDate: Date;
    disabled: boolean;

}

