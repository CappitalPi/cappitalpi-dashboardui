export class PersonSearchDto {

    uid: string;
    code: number;
    name: string;
    createdDate: Date;
    updatedDate: Date;
    disabledDate: Date;
    disabled: boolean;

}

