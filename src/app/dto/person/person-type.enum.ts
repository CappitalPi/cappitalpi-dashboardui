export enum PersonType {
    PHYSICAL_PERSON = "P",
    LEGAL_PERSON = "L",
}
