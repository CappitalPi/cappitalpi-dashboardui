import { AddressDto } from "../address/address.dto";
import { ContactDto } from "../contact/contact.dto";
import { EmailDto } from "../email/email.dto";
import { InscriptionDto } from "../inscription/inscription.dto";
import { PhoneDto } from "../phone/phone.dto";
import { PersonDetailDto } from "./person-detail.dto";

export class PersonDto {

    uid: string;
    registerUid: string
    code: number;
    type: string;
    name: string;
    disabled: boolean;
    notes: string;
    imagePath: string;
    website: string;
    createdDate: Date;
    updatedDate: Date;
    disabledDate: Date;
    detail: PersonDetailDto;
    inscriptions: InscriptionDto[];
    addresses: AddressDto[];
    contacts: ContactDto[];
    phones: PhoneDto[];
    emails: EmailDto[];
}

