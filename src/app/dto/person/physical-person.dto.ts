import { PersonDetailDto } from "./person-detail.dto";

export class PhysicalPersonDto extends PersonDetailDto {
    birthDate: Date;
    sex: string;
    maritalStatus: string;
    nationality: string
}

