export class PhoneTypeDto {
    uid: string;
    code: number;
    name: string;
    description: string;
}

