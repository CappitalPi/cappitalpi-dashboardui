import { AddressDto } from "../address/address.dto";
import { ContactDto } from "../contact/contact.dto";
import { PersonDto } from "../person/person.dto";
import { PhoneTypeDto } from "./phone-type.dto";

export class PhoneDto {
    uid: string;
    code: number;
    person: PersonDto;
    contact: ContactDto;
    address: AddressDto;
    type: PhoneTypeDto;
    areaCode: string;
    phoneNumber: string;
    extension: string;
    notes: string;
    disabled: boolean;
}

