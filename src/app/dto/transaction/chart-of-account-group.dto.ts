import { ChartOfAccountTotalDto } from "./chart-of-account-total.dto";

export class ChartOfAccountGroupDto {

    master: ChartOfAccountTotalDto;
    details: ChartOfAccountGroupDto[];
    total: number;
    
}

