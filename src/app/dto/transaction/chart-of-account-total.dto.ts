import { TransactionType } from "../../enum/transaction-type.enum";

export class ChartOfAccountTotalDto {

    uid: string;
    code: number;
    accountNumber: string;
    name: string;
    description: string;
    parent: ChartOfAccountTotalDto;
    transactionType: TransactionType;
    category: string;
    total: number;
    
}

