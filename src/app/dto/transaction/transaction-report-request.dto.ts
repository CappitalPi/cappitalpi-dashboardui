import { AccountSearchDto } from "../account/account-search.dto";
import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";

export class TransactionReportRequestDto {

    selectedReport: string;
    startDate : string;
    endDate : string;
    accountSelectedList: AccountSearchDto[]; 
    chartOfAccountCreditSelectedList: ChartOfAccountDto[]; 
    chartOfAccountDebitSelectedList: ChartOfAccountDto[]; 
    
}

