import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";

export class TransactionSearchDto {

    uid: string;
    registerUid: string;
    appAccountUid: string;
    code: number;
    description: string;
    transactionType: string;
    chartOfAccount: ChartOfAccountDto;
    status: string;
    creationType: string;
    amount: number;
    transactionDate: Date;
}

