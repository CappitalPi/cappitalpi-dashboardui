export class TransactionTotalByDateDto {

    date: Date;
    totalCredit: number;
    totalDebit: number;
    balance: number;

}

