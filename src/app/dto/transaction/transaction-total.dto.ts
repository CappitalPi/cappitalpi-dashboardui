import { TransactionDto } from "./transaction.dto";

export class TransactionTotalDto {

    transactions: TransactionDto[];
    totalCredit: number;
    totalDebit: number;
    total: number;

}

