import { PaymentMethod } from "../../enum/payment-method.enum";
import { AccountSearchDto } from "../account/account-search.dto";
import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";

export class TransactionDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    cashAccount: AccountSearchDto;
    chartOfAccount: ChartOfAccountDto;
    transactionType: string;
    description: string;
    documentNumber: string;
    checkNumber: string;
    paymentMethod: PaymentMethod;
    amount: number;
    status: string;
    creationType: string;
    transactionDate: Date;
    createdDate: Date;
    updatedDate: Date;

}

