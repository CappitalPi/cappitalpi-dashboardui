import { AccountDto } from "../account/account.dto";
import { ChartOfAccountDto } from "../chart-of-account/chart-of-account.dto";

export class TransferRequestDto {

    registerUid: string;
    appAccountUid: string;
    debitCashAccount: AccountDto;
    creditCashAccount: AccountDto;
    debitChartOfAccount: ChartOfAccountDto;
    creditChartOfAccount: ChartOfAccountDto;
    description: string;
    amount: number;
    transferDate: Date;

}

