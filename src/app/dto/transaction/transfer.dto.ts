import { TransactionDto } from "./transaction.dto";

export class TransferDto {

    uid: string;
    code: number;
    registerUid: string;
    appAccountUid: string;
    creditTransaction: TransactionDto;
    debitTransaction: TransactionDto;
    amount: number;
    transferDate: Date;

}

