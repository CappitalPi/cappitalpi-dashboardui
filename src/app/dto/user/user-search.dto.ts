export class UserSearchDto {

    uid: string;
    code: Number;
    name: string;
    email: string;
    username: string;
    disabled: boolean;
    createdDate: Date;
    updatedDate: Date;
    disabledDate: Date;

}

