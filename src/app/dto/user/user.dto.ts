export class UserDto {

    uid: string;
    code: number;
    registerUid: string;
    externalEnterpriseId: number;
    name: string;
    email: string;
    username: string;
    password: string;
    currentPassword: string;
    confirmPassword: string;
    disabled: boolean;
    createdDate: Date;
    updatedDate: Date;
    disabledDate: Date;

}

