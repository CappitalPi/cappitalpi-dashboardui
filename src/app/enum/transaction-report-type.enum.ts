export enum TransactionReportType {
    GROUPED_BY_CHART_OF_ACCOUNT="GBCOA",
    BY_TRANSACTION_DATE="BTD"
}