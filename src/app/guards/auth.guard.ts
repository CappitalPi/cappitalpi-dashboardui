import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, CanActivateChild, Route, UrlSegment, NavigationExtras } from '@angular/router';
import { AuthService } from '../service/auth/auth-service';
import { ROUTE_DASHBOARD, ROUTE_LOGIN } from '../service/navigation-constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  
  constructor(private router: Router, private authService : AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url = state.url;
    return this.checkLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    const url = `/${route.path}`;
    return this.checkLogin(url);
  }

  private checkLogin(url: string): boolean {
    const uniqueSession = this.getUniqueId();
    const navigationExtras: NavigationExtras = {
      queryParams: { se: uniqueSession }
    };
    
    const isRouteToLogin  = url && url.includes(`/${ROUTE_LOGIN}`);
    const currentUser = this.authService.currentUserValue;
    if (currentUser && isRouteToLogin) {
      this.router.navigate([`/${ROUTE_DASHBOARD}`], navigationExtras);
      return false;
    }
     
    if (!currentUser && !isRouteToLogin) {
      this.router.navigate([`/${ROUTE_LOGIN}`], navigationExtras);
      return false;
    }
    return true;
  }

  private getUniqueId(): string {
    const stringArr = [];
    for(let i = 0; i< 4; i++){
      // eslint-disable-next-line no-bitwise
      const S4 = (((5 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      stringArr.push(S4);
    }
    return stringArr.join('-');
  }

}