const SEPARATOR = "#";
export class AppHelper {

    public static generateUniqueCode(index: number, code: string): string {
        return code ? code : (index + 1).toString().padStart(4, "0");
    }

    public static generateItemLabel(code: string, itemValue: string): string {
        return code + SEPARATOR + " "+itemValue;
    }
}