import { LegalPersonDto } from "../../dto/person/legal-person.dto";
import { PersonType } from "../../dto/person/person-type.enum";
import { PersonDto } from "../../dto/person/person.dto";
import { PhysicalPersonDto } from "../../dto/person/physical-person.dto";

export class PersonHelper {

    public static personByType(person: PersonDto, legalPerson: LegalPersonDto, physicalPerson: PhysicalPersonDto): PersonDto {
      if (person == null) {
        return null;
      }
      if (PersonType.LEGAL_PERSON == person.type) {
        person.detail = legalPerson;
        person.detail.detailType = PersonType.LEGAL_PERSON;
      }
      if (PersonType.PHYSICAL_PERSON == person.type) {
        person.detail = physicalPerson;
      }
      person.detail.detailType = person.type;
      return person;
    }

}