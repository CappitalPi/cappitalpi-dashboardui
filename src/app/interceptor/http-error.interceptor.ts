import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { AuthService } from '../service/auth/auth-service';
import { Router } from '@angular/router';
import { ROUTE_LOGIN } from '../service/navigation-constants';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(private authService : AuthService, private router: Router) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            timeout(15000),
            catchError(err => {
            if (err.status === 401) {
                this.authService.logout().subscribe(() => {
                    this.authService.removeCredentials();
                    this.router.navigate([`/${ROUTE_LOGIN}`]);
                },
                err => {
                    console.debug(`Error ${err}`);
                });
            }
            if (err.status === 0 || err.status === 500) {
                err.message = 'erro';
            }
            return throwError(err);
        }))
    }
}