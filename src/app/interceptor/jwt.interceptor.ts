import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize} from 'rxjs/operators';
import { SessionService } from '../service/auth/session-service';
import { TokenJwt } from '../service/auth/token-jwt';
import { SpinnerService } from '../service/general/spinner.service';
import { TranslateService } from '@ngx-translate/core';
import { I18NUtils } from '../utils/i18n.utils';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    private totalRequests = 0;
    
    constructor(private session : SessionService, public spinnerService: SpinnerService, private translateService: TranslateService) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let httpHeaders : HttpHeaders = new HttpHeaders();
        //httpHeaders = httpHeaders.set('apikey', environment.api_key);
        this.totalRequests++;
        this.spinnerService.setLoading(true);

        let sessionToken = this.session.getToken();
        if (sessionToken) {
            var tokenJwt = this.buildTokenJwt(sessionToken);
            httpHeaders = httpHeaders.set('Authorization', `Bearer ${tokenJwt.accessToken}`);
        }

        request = request.clone({
            headers: httpHeaders,
            params: request.params.set(
                "language", I18NUtils.getBackEndLanguage(this.translateService.getDefaultLang())
            ),
        });
        
        return next.handle(request).pipe(
            finalize(() => {
                this.totalRequests--;
                if (this.totalRequests == 0) {
                    this.spinnerService.setLoading(false);
                }
            })
        )
    }
    
    private buildTokenJwt(token : string){
        var tokenJwtDto : TokenJwt = new TokenJwt();
        tokenJwtDto.accessToken = null;
        tokenJwtDto.tokenType = null;
        
        if (token != null && token.length > 0){
            tokenJwtDto = JSON.parse(token);
        }
        return tokenJwtDto;
    }
 }