import { TestBed } from '@angular/core/testing';
import { AccountPayRecService } from './account-pay-rec.service';


describe('AccountPayRecService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountPayRecService = TestBed.get(AccountPayRecService);
    expect(service).toBeTruthy();
  });
});
