import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ACCOUNT_PAY_REC_REPORT_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { DateUtils } from '../../utils/date.utils';
import { AccountPayRecCountDto } from '../../dto/account-pay-rec/account-pay-rec-count.dto';

@Injectable({
  providedIn: 'root'
})
export class AccountPayRecService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findAccountPayRecCountOverdue(date: Date) {
    const params = new HttpParams()
    .set('date', DateUtils.formatToShortDate(date));
    return super.get<AccountPayRecCountDto>(ACCOUNT_PAY_REC_REPORT_ENDPOINT+"/date", params);
  }

}
