import { TestBed } from '@angular/core/testing';

import { AccountPayableService } from './account-payable.service';

describe('AccountPayableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountPayableService = TestBed.get(AccountPayableService);
    expect(service).toBeTruthy();
  });
});
