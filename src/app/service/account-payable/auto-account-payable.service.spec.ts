import { TestBed } from '@angular/core/testing';

import { AutoAccountPayableService } from './auto-account-payable.service';

describe('AutoAccountPayableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoAccountPayableService = TestBed.get(AutoAccountPayableService);
    expect(service).toBeTruthy();
  });
});
