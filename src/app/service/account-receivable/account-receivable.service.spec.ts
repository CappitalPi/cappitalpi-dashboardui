import { TestBed } from '@angular/core/testing';

import { AccountReceivableService } from './account-receivable.service';

describe('AccountReceivableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountReceivableService = TestBed.get(AccountReceivableService);
    expect(service).toBeTruthy();
  });
});
