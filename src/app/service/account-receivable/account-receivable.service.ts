import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ACCOUNT_RECEIVABLE_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { AccountPayRecSearchDto } from '../../dto/account-pay-rec/account-pay-rec-search.dto';
import { AccountPayRecRepairDto } from '../../dto/account-pay-rec/account-pay-rec-repair.dto';
import { AccountPayRecRequestDto } from '../../dto/account-pay-rec/account-pay-rec-request.dto';
import { AccountPayRecDto } from '../../dto/account-pay-rec/account-pay-rec.dto';
import { DateUtils } from '../../utils/date.utils';
import { DateType } from '../../enum/date-type.enum';
import { StringUtils } from '../../utils/string-utils';
import { AccountPayRecPaymentDto } from '../../dto/account-pay-rec/account-pay-rec-payment.dto';
import { AccountPayRecDetailDto } from '../../dto/account-pay-rec/account-pay-rec-detail.dto';

@Injectable({
  providedIn: 'root'
})
export class AccountReceivableService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findByCode(code : number) {
    return super.get<AccountPayRecDto>(ACCOUNT_RECEIVABLE_ENDPOINT+`/${code}/code`);
  }

  public findByCodeWithPayments(code : number) {
    return super.get<AccountPayRecDto>(ACCOUNT_RECEIVABLE_ENDPOINT+`/${code}/code/payments`);
  }

  public findByUidDetailed(code : number) {
    return super.get<AccountPayRecDetailDto>(ACCOUNT_RECEIVABLE_ENDPOINT+`/${code}/detail`);
  }

  public filter(pageNumber: number, pageSize: number, code: number, startDate: Date, endDate: Date, dateType: DateType, personSearch: string, chartOfAccountUid: string, documentNumber: string, status: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('startDate', DateUtils.formatToShortDate(startDate))
      .set('endDate', DateUtils.formatToShortDate(endDate))
      .set('dateType', dateType ? dateType.toString() : StringUtils.EMPTY)
      .set('code', code ? code.toString() : StringUtils.EMPTY)
      .set('personSearch', personSearch ? personSearch : StringUtils.EMPTY)
      .set('chartOfAccountUid', chartOfAccountUid ? chartOfAccountUid : StringUtils.EMPTY)
      .set('documentNumber', documentNumber ? documentNumber : StringUtils.EMPTY)
      .set('status', status ? status : StringUtils.EMPTY);
    return super.get<PageResultDto<AccountPayRecSearchDto>>(ACCOUNT_RECEIVABLE_ENDPOINT + "/filter", params);
  }

  public persist(accountPayRec: AccountPayRecRequestDto) {
    if (!accountPayRec) {
      throw new Error(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
    }
    if (accountPayRec.uid) {
      return this.create(accountPayRec);
    } else {
      return this.update(accountPayRec);
    }
  }

  public repair(accountPayRecRepair: AccountPayRecRepairDto) {
    return super.put(accountPayRecRepair, ACCOUNT_RECEIVABLE_ENDPOINT+"/repair");
  }

  public create(accountReceivable: AccountPayRecRequestDto) {
    return super.post(accountReceivable, ACCOUNT_RECEIVABLE_ENDPOINT);
  }

  public pay(accountReceivable: AccountPayRecPaymentDto) {
    return super.post(accountReceivable, ACCOUNT_RECEIVABLE_ENDPOINT + "/payment");
  }

  public update(accountReceivable: AccountPayRecRequestDto) {
    return super.put(accountReceivable, ACCOUNT_RECEIVABLE_ENDPOINT);
  }

  public remove(uid: string) {
    return super.delete(uid, ACCOUNT_RECEIVABLE_ENDPOINT);
  }
  
}
