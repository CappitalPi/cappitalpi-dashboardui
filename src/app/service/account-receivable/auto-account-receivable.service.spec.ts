import { TestBed } from '@angular/core/testing';

import { AutoAccountReceivableService } from './auto-account-receivable.service';

describe('AutoAccountReceivableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutoAccountReceivableService = TestBed.get(AutoAccountReceivableService);
    expect(service).toBeTruthy();
  });
});
