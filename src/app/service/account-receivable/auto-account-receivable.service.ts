import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AUTO_ACCOUNT_RECEIVABLE_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { StringUtils } from '../../utils/string-utils';
import { AutoAccountPayRecDetailDto } from '../../dto/account-pay-rec/auto-account-pay-rec-detail.dto';
import { AutoAccountPayRecDto } from '../../dto/account-pay-rec/auto-account-pay-rec.dto';
import { AutoAccountPayRecSearchDto } from '../../dto/account-pay-rec/auto-account-pay-rec-search.dto';
import { AutoAccountPayRecRequestDto } from '../../dto/account-pay-rec/auto-account-pay-rec-request.dto';

@Injectable({
  providedIn: 'root'
})
export class AutoAccountReceivableService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findByUidDetailed(code : number) {
    return super.get<AutoAccountPayRecDetailDto>(AUTO_ACCOUNT_RECEIVABLE_ENDPOINT+`/${code}/detail`);
  }

  public findByCode(code : number) {
    return super.get<AutoAccountPayRecDto>(AUTO_ACCOUNT_RECEIVABLE_ENDPOINT+`/${code}/code`);
  }


  public filter(pageNumber: number, pageSize: number, code: number, personSearch: string, chartOfAccountUid: string, documentNumber: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : StringUtils.EMPTY)
      .set('personSearch', personSearch ? personSearch : StringUtils.EMPTY)
      .set('chartOfAccountUid', chartOfAccountUid ? chartOfAccountUid : StringUtils.EMPTY)
      .set('documentNumber', documentNumber ? documentNumber : StringUtils.EMPTY);
    return super.get<PageResultDto<AutoAccountPayRecSearchDto>>(AUTO_ACCOUNT_RECEIVABLE_ENDPOINT + "/filter", params);
  }

  public persist(accountPayRec: AutoAccountPayRecRequestDto) {
    if (!accountPayRec) {
      throw new Error(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
    }
    if (accountPayRec.uid) {
      return this.create(accountPayRec);
    } else {
      return this.update(accountPayRec);
    }
  }

  public create(accountReceivable: AutoAccountPayRecRequestDto) {
    return super.post(accountReceivable, AUTO_ACCOUNT_RECEIVABLE_ENDPOINT);
  }

  public update(accountReceivable: AutoAccountPayRecRequestDto) {
    return super.put(accountReceivable, AUTO_ACCOUNT_RECEIVABLE_ENDPOINT);
  }

  public remove(uid: string) {
    return super.delete(uid, AUTO_ACCOUNT_RECEIVABLE_ENDPOINT);
  }
  
}
