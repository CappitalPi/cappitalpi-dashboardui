import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ACCOUNT_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { AccountDto } from '../../dto/account/account.dto';
import { PageResultDto } from '../../dto/page-result.dto';
import { AccountSearchDto } from '../../dto/account/account-search.dto';

@Injectable({
  providedIn: 'root'
})
export class AccountService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findAll() {
    return super.get<AccountSearchDto[]>(ACCOUNT_ENDPOINT);
  }

  public filter(pageNumber: number, pageSize: number, code: number, name: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : "")
      .set('name', name ? name : "");
    return super.get<PageResultDto<AccountSearchDto>>(ACCOUNT_ENDPOINT+"/filter", params);
  }

  public findByCode(code : number) {
    return super.get<AccountDto>(ACCOUNT_ENDPOINT+`/${code}/code`);
  }

  public create(account: AccountDto) {
    return super.post(account, ACCOUNT_ENDPOINT);
  }

  public update(account: AccountDto) {
    return super.put(account, ACCOUNT_ENDPOINT);
  }

  public remove(uid: string) {
    return super.delete(uid, ACCOUNT_ENDPOINT);
  }

  public getByUid(list: AccountSearchDto[], uid: string): AccountSearchDto {
    let response: AccountSearchDto = null;
    if (list?.length > 0 && uid) {
        list.forEach(item => {
            if (item?.uid === uid) {
                response = item;
            }
        })
    }
    return response;
  }

}
