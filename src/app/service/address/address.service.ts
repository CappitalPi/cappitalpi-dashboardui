import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient } from '@angular/common/http';
import { ADDRESS_TYPE_ENDPOINT, CITY_ENDPOINT, COUNTRY_ENDPOINT, STATE_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { AddressTypeDto } from '../../dto/address/address-type.dto';
import { CityDto } from '../../dto/address/city.dto';
import { StateDto } from '../../dto/address/state.dto';
import { CountryDto } from '../../dto/address/country.dto';

@Injectable({
  providedIn: 'root'
})
export class AddressService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findAllTypes() {
    return super.get<AddressTypeDto[]>(ADDRESS_TYPE_ENDPOINT);
  }

  public findCitiesByState(stateCode: number) {
    return super.get<CityDto[]>(CITY_ENDPOINT+`/${stateCode}/state`);
  }

  public findStatesByCountry(countryCode: number) {
    return super.get<StateDto[]>(STATE_ENDPOINT+`/${countryCode}/country`);
  }

  public findAllCountries() {
    return super.get<CountryDto[]>(COUNTRY_ENDPOINT);
  }

}
