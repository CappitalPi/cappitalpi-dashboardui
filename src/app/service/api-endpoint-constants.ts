// Accounts Payable and Receivable
export const ACCOUNT_PAY_REC_ENDPOINT_API = 'cappitalpi-account-pay-rec/api';
export const ACCOUNT_PAY_REC_REPORT_ENDPOINT = `${ACCOUNT_PAY_REC_ENDPOINT_API}/v1/account-report`;
export const ACCOUNT_PAYABLE_ENDPOINT = `${ACCOUNT_PAY_REC_ENDPOINT_API}/v1/accounts-payable`;
export const ACCOUNT_RECEIVABLE_ENDPOINT = `${ACCOUNT_PAY_REC_ENDPOINT_API}/v1/accounts-receivable`;
// Auto Accounts Payable and Receivable
export const AUTO_ACCOUNT_PAY_REC_ENDPOINT_API = 'cappitalpi-auto-account-pay-rec/api';
export const AUTO_ACCOUNT_PAYABLE_ENDPOINT = `${AUTO_ACCOUNT_PAY_REC_ENDPOINT_API}/v1/auto-accounts-payable`;
export const AUTO_ACCOUNT_RECEIVABLE_ENDPOINT = `${AUTO_ACCOUNT_PAY_REC_ENDPOINT_API}/v1/auto-accounts-receivable`;
// CHART_OF_ACCOUNT
export const CHART_OF_ACCOUNT_ENDPOINT_API = 'cappitalpi-chart-of-account/api';
export const CHART_OF_ACCOUNT_ENDPOINT = `${CHART_OF_ACCOUNT_ENDPOINT_API}/v1/chart-of-accounts`;
// ACCOUNT
export const ACCOUNT_ENDPOINT_API = 'cappitalpi-account/api';
export const ACCOUNT_ENDPOINT = `${ACCOUNT_ENDPOINT_API}/v1/accounts`;
// TRANSACTION
export const TRANSACTION_ENDPOINT_API = 'cappitalpi-transaction/api';
export const TRANSACTION_ENDPOINT = `${TRANSACTION_ENDPOINT_API}/v1/transactions`;
export const TRANSFER_ENDPOINT = `${TRANSACTION_ENDPOINT_API}/v1/transfers`;
// LOCATION
export const LOCATION_ENDPOINT_API = 'cappitalpi-location/api';
export const ADDRESS_TYPE_ENDPOINT = `${LOCATION_ENDPOINT_API}/v1/address-types`;
export const PHONE_TYPE_ENDPOINT = `${LOCATION_ENDPOINT_API}/v1/phone-types`;
export const CITY_ENDPOINT = `${LOCATION_ENDPOINT_API}/v1/cities`;
export const STATE_ENDPOINT = `${LOCATION_ENDPOINT_API}/v1/states`;
export const COUNTRY_ENDPOINT = `${LOCATION_ENDPOINT_API}/v1/countries`;
// PERSON
export const PERSON_ENDPOINT_API = 'cappitalpi-person/api';
export const CUSTOMER_ENDPOINT = `${PERSON_ENDPOINT_API}/v1/customers`;
export const SUPPLIER_ENDPOINT = `${PERSON_ENDPOINT_API}/v1/suppliers`;
export const EMPLOYEE_ENDPOINT = `${PERSON_ENDPOINT_API}/v1/employees`;
export const PERSON_ENDPOINT = `${PERSON_ENDPOINT_API}/v1/persons`;
export const INSCRIPTION_TYPE_ENDPOINT = `${PERSON_ENDPOINT_API}/v1/inscription-types`;
// USER
export const USER_ENDPOINT_API = 'cappitalpi-user/api/v1/users';
// SECURITY
export const SECURITY_ENDPOINT_API = `cappitalpi-security/api`;
export const LOGIN_ENDPOINT = `${SECURITY_ENDPOINT_API}/v1/auth/login`;
export const LOGOUT_ENDPOINT = `${SECURITY_ENDPOINT_API}/v1/auth/logout`;