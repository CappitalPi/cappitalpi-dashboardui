import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { LOGIN_ENDPOINT, LOGOUT_ENDPOINT } from '../api-endpoint-constants';
import { Router } from '@angular/router';
import { UserSession } from './user-session';
import { SessionService } from './session-service';
import { environment } from '../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private currentUserSubject: BehaviorSubject<UserSession>;
  public currentUser: Observable<UserSession>;
  
  constructor(private http: HttpClient, private session : SessionService, private router: Router, private translateService: TranslateService) {
    this.currentUserSubject = new BehaviorSubject<UserSession>(session.getUser());
    this.currentUser = this.currentUserSubject.asObservable();
  }
  
  public get currentUserValue(): UserSession {
    return this.currentUserSubject.value;
  }
  
  public login(values: any) {
    return this.http.post(environment.serverUrl+LOGIN_ENDPOINT, values, {responseType: 'text'})
    .pipe(map(token => {
      if (token) {
        this.session.setToken(token);
        this.currentUserSubject.next(this.session.getUser());
      }
      return token;
    }));
  }
  
  public logout() {
    return this.http.post(environment.serverUrl+LOGOUT_ENDPOINT, "")
    .pipe(
      catchError(() => {
        return throwError(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'))
      })
    );
  }

  public removeCredentials() {
    this.session.removeToken();
    this.currentUserSubject.next(null);
  }
  
  public appHasAuthority(authority : string) {
    let user : UserSession = this.session.getUser();
    if (user && authority) {
      return user.authorities.includes(authority)
    };
    return false;
  }

}