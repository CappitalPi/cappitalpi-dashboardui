import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserSession } from './user-session';

export const TOKEN_SESSION = 'opex3rp-token-session';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  
  constructor(private readonly jwtHelper: JwtHelperService) { }

  public setToken(token : string) : void {
    localStorage.setItem(TOKEN_SESSION, token);
  }

  public getToken() : string {
    return localStorage.getItem(TOKEN_SESSION);
  }

  public removeToken() : void {
    localStorage.removeItem(TOKEN_SESSION);
  }

  public getUser(): UserSession{
    const token : string = this.getToken();
    if (!token) {
      return null;
    }
    const decoded = this.jwtHelper.decodeToken(token);
    let connectedUser : UserSession = new UserSession();
    connectedUser.uid = decoded.sub;
    connectedUser.code = decoded.code;
    connectedUser.username = decoded.username;
    connectedUser.ownerUid = decoded.ownerUid;
    connectedUser.registerUid = decoded.registerUid;
    connectedUser.authorities = decoded.acl;
    return connectedUser;
  }
}
  