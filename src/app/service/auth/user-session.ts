export class UserSession {

    uid : string;
    code : Number;
    username : string;
    ownerUid: string;
    registerUid: string;
    authorities : string[];

}
