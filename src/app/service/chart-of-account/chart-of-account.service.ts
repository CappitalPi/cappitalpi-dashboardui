import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CHART_OF_ACCOUNT_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { ChartOfAccountDto } from '../../dto/chart-of-account/chart-of-account.dto';

@Injectable({
  providedIn: 'root'
})
export class ChartOfAccountService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findAll() {
    return super.get<ChartOfAccountDto[]>(CHART_OF_ACCOUNT_ENDPOINT);
  }

  public findByType(transactionType: string) {
    return super.get<ChartOfAccountDto[]>(CHART_OF_ACCOUNT_ENDPOINT+`/${transactionType}/type`);
  }

  public filter(code: number, name: string) {
    const params = new HttpParams()
      .set('code', code ? code.toString() : "")
      .set('name', name ? name : "");
    return super.get<PageResultDto<ChartOfAccountDto>>(CHART_OF_ACCOUNT_ENDPOINT+"/filter", params);
  }

  public findByCode(code : number) {
    return super.get<ChartOfAccountDto>(CHART_OF_ACCOUNT_ENDPOINT+`/${code}/code`);
  }

  public create(chartOfAccount: ChartOfAccountDto) {
    return super.post(chartOfAccount, CHART_OF_ACCOUNT_ENDPOINT);
  }

  public update(chartOfAccount: ChartOfAccountDto) {
    return super.put(chartOfAccount, CHART_OF_ACCOUNT_ENDPOINT);
  }

  public remove(uid: string) {
    return super.delete(uid, CHART_OF_ACCOUNT_ENDPOINT);
  }

}
