import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EMPLOYEE_ENDPOINT, INSCRIPTION_TYPE_ENDPOINT } from '../api-endpoint-constants';
import { PersonSearchDto } from '../../dto/person/person-search.dto';
import { PersonDto } from '../../dto/person/person.dto';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { InscriptionTypeDto } from '../../dto/inscription/inscription-type.dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public filter(pageNumber: number, pageSize: number, code: number, name: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : "")
      .set('name', name ? name : "");
    return super.get<PageResultDto<PersonSearchDto>>(EMPLOYEE_ENDPOINT, params);
  }

  public findByCode(code : number) {
    return super.get<PersonDto>(EMPLOYEE_ENDPOINT+`/${code}/code`);
  }

  public findInscriptionTypes() {
    return super.get<InscriptionTypeDto[]>(INSCRIPTION_TYPE_ENDPOINT);
  }

  public create(employee: PersonDto) {
    return super.post(employee, EMPLOYEE_ENDPOINT);
  }

  public update(employee: PersonDto) {
    return super.put(employee, EMPLOYEE_ENDPOINT);
  }

  public disable(code: number) {
    return super.put(null, EMPLOYEE_ENDPOINT+`/${code}/disable`);
  }

  public enable(code: number) {
    return super.put(null, EMPLOYEE_ENDPOINT+`/${code}/enable`);
  }
  
}
