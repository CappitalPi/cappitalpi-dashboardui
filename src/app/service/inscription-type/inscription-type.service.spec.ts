import { TestBed } from '@angular/core/testing';

import { InscriptionTypeService } from './inscription-type.service';

describe('InscriptionTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InscriptionTypeService = TestBed.get(InscriptionTypeService);
    expect(service).toBeTruthy();
  });
});
