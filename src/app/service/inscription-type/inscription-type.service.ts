import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { INSCRIPTION_TYPE_ENDPOINT } from '../api-endpoint-constants';
import { InscriptionTypeDto } from '../../dto/inscription/inscription-type.dto';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';

@Injectable({
  providedIn: 'root'
})
export class InscriptionTypeService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public persist(inscriptionType: InscriptionTypeDto) {
    if (!inscriptionType) {
      throw new Error(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
    }
    if (inscriptionType.uid) {
      return this.create(inscriptionType);
    } else {
      return this.update(inscriptionType);
    }
  }

  public filter(pageNumber: number, pageSize: number, code: number, name: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : "")
      .set('name', name?.length > 0 ? name : "");
    return super.get<PageResultDto<InscriptionTypeDto>>(INSCRIPTION_TYPE_ENDPOINT+'/filter', params);
  }

  public findByCode(code : number) {
    return super.get<InscriptionTypeDto>(INSCRIPTION_TYPE_ENDPOINT+`/${code}/code`);
  }

  public create(inscriptionType: InscriptionTypeDto) {
    return super.post(inscriptionType, INSCRIPTION_TYPE_ENDPOINT);
  }

  public update(inscriptionType: InscriptionTypeDto) {
    return super.put(inscriptionType, INSCRIPTION_TYPE_ENDPOINT);
  }

  public remove(uid: string) {
    return super.delete(uid, INSCRIPTION_TYPE_ENDPOINT);
  }
  
}
