import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PERSON_ENDPOINT } from '../api-endpoint-constants';
import { PersonSearchDto } from '../../dto/person/person-search.dto';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class PersonService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public filter(value: string) {
    const params = new HttpParams()
      .set('value', value);
    return super.get<PersonSearchDto[]>(PERSON_ENDPOINT, params);
  }

}
