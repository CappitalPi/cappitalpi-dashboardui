import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient } from '@angular/common/http';
import { PHONE_TYPE_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { PhoneTypeDto } from '../../dto/phone/phone-type.dto';

@Injectable({
  providedIn: 'root'
})
export class PhoneService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findAllTypes() {
    return super.get<PhoneTypeDto[]>(PHONE_TYPE_ENDPOINT);
  }

}
