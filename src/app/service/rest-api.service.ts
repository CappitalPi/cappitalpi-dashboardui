import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) { }
 
  public get<T>(endPoint: string, params: HttpParams = null) {
    if (params) {
      return this.http.get<T>(environment.serverUrl + endPoint, { params })
      .pipe(
        retry(1),
        catchError(err => this.handlerHttpError(err, this.translateService))
      );
    }
    return this.http.get<T>(environment.serverUrl + endPoint)
    .pipe(
      retry(1),
      catchError(err => this.handlerHttpError(err, this.translateService))
    );
  }

  public post(object : any, endPoint: string) {
    const url = environment.serverUrl + endPoint;
    return this.http.post(url, object)
    .pipe(
      catchError(err => this.handlerHttpError(err, this.translateService))
    );
  }

  public put(object : any, endPoint: string) {
    const url = environment.serverUrl + endPoint;
    return this.http.put(url, object)
    .pipe(
      catchError(err => this.handlerHttpError(err, this.translateService))
    );
  }

  public delete(uid : string, endPoint: string) {
    const url = environment.serverUrl + endPoint;
    return this.http.delete(url + `/${uid}`)
    .pipe(
      catchError(err => this.handlerHttpError(err, this.translateService))
    );
  }

  public deleteByUid(uid : string, endPoint: string) {
    const url = environment.serverUrl + endPoint;
    return this.http.delete(url + `/${uid}`);
  }
  
  private handlerHttpError(error: any, translateService: TranslateService) {
    if (error && error.status) {
      console.error(JSON.stringify(error));
      switch(error.status) {
        case 401: return throwError(translateService.instant('APP.ERROR.UNAUTHORIZED'));
        case 403: return throwError(translateService.instant('APP.ERROR.FORBIDDEN'));
        case 428: { //TODO: incluir uma variavel "business rule" para dizer que a mensagem vem de uma regra de negócios e não de DataIntegrityViolationException.class
          if (error.error != null && error.error.message != null){
            return throwError(error.error.message);
          } 
          if (error.message != null) {
            return throwError(error.message);
          } 
          return throwError(error);  
        }
        default: return throwError(translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG_CODE', {'code':error.status}));
      }
    }
    return throwError(translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
  }

}
