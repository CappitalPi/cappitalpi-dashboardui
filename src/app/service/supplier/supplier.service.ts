import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SUPPLIER_ENDPOINT, INSCRIPTION_TYPE_ENDPOINT } from '../api-endpoint-constants';
import { PersonSearchDto } from '../../dto/person/person-search.dto';
import { PersonDto } from '../../dto/person/person.dto';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { InscriptionTypeDto } from '../../dto/inscription/inscription-type.dto';

@Injectable({
  providedIn: 'root'
})
export class SupplierService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public filter(pageNumber: number, pageSize: number, code: number, name: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : "")
      .set('name', name ? name : "");
    return super.get<PageResultDto<PersonSearchDto>>(SUPPLIER_ENDPOINT, params);
  }

  public findByCode(code : number) {
    return super.get<PersonDto>(SUPPLIER_ENDPOINT+`/${code}/code`);
  }

  public findInscriptionTypes() {
    return super.get<InscriptionTypeDto[]>(INSCRIPTION_TYPE_ENDPOINT);
  }

  public create(supplier: PersonDto) {
    return super.post(supplier, SUPPLIER_ENDPOINT);
  }

  public update(supplier: PersonDto) {
    return super.put(supplier, SUPPLIER_ENDPOINT);
  }

  public disable(code: number) {
    return super.put(null, SUPPLIER_ENDPOINT+`/${code}/disable`);
  }

  public enable(code: number) {
    return super.put(null, SUPPLIER_ENDPOINT+`/${code}/enable`);
  }
  
}
