import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TRANSACTION_ENDPOINT } from '../api-endpoint-constants';
import { TranslateService } from '@ngx-translate/core';
import { DateUtils } from '../../utils/date.utils';
import { TransactionTotalDto } from '../../dto/transaction/transaction-total.dto';
import { ChartOfAccountGroupDto } from '../../dto/transaction/chart-of-account-group.dto';
import { ChartOfAccountTotalDto } from '../../dto/transaction/chart-of-account-total.dto';
import { TransactionTotalByDateDto } from '../../dto/transaction/transaction-total-by-date.dto';

@Injectable({
  providedIn: 'root'
})
export class TransactionReportService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findGroupedByChartOfAccounts(startDate: Date, endDate: Date, accounts: string[], chartOfAccounts: string[]) {
    const params = new HttpParams()
    .set('startDate', DateUtils.formatToShortDate(startDate))
    .set('endDate', DateUtils.formatToShortDate(endDate))
    .set('accounts', accounts?.length > 0 ? accounts.toString() : "")
    .set('chartOfAccounts', chartOfAccounts?.length > 0 ? chartOfAccounts.toString() : "");
    return super.get<ChartOfAccountTotalDto[]>(TRANSACTION_ENDPOINT+"/reports/chart-of-accounts", params);
  }

  public findByTransactionDate(startDate: Date, endDate: Date, accounts: string[], chartOfAccounts: string[]) {
    const params = new HttpParams()
    .set('startDate', DateUtils.formatToShortDate(startDate))
    .set('endDate', DateUtils.formatToShortDate(endDate))
    .set('accounts', accounts?.length > 0 ? accounts.toString() : "")
    .set('chartOfAccounts', chartOfAccounts?.length > 0 ? chartOfAccounts.toString() : "");
    return super.get<TransactionTotalDto>(TRANSACTION_ENDPOINT+"/reports/date", params);
  }

  public findTotalByDate(date: Date) {
    const params = new HttpParams()
    .set('date', DateUtils.formatToShortDate(date));
    return super.get<TransactionTotalByDateDto[]>(TRANSACTION_ENDPOINT+"/reports/chart/date", params);
  }

}
