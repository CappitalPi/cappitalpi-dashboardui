import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TRANSACTION_ENDPOINT, TRANSFER_ENDPOINT } from '../api-endpoint-constants';
import { TransactionSearchDto } from '../../dto/transaction/transaction-search.dto';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';
import { TransactionDto } from '../../dto/transaction/transaction.dto';
import { DateUtils } from '../../utils/date.utils';
import { TransactionRepairDto } from '../../dto/transaction/transaction-repair.dto';
import { TransferRequestDto } from '../../dto/transaction/transfer-request.dto';

@Injectable({
  providedIn: 'root'
})
export class TransactionService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public findByCode(code : number) {
    return super.get<TransactionDto>(TRANSACTION_ENDPOINT+`/${code}/code`);
  }

  public findAllByDay(pageNumber: number, pageSize: number, code: number, accountUid: string, chartOfAccountUid: string) {
    const params = new HttpParams()
    .set('pageNumber', pageNumber.toString())
    .set('pageSize', pageSize.toString())
    .set('currentDate', DateUtils.formatToShortDate(new Date()))
    .set('code', code ? code.toString() : "")
    .set('accountUid', accountUid ? accountUid : "")
    .set('chartOfAccountUid', chartOfAccountUid ? chartOfAccountUid : "");
    return super.get<PageResultDto<TransactionSearchDto>>(TRANSACTION_ENDPOINT+"/date", params);
  }

  public findHistory(pageNumber: number, pageSize: number, startDate: Date, endDate: Date, code: number, accountUid: string, chartOfAccountUid: string) {
    const params = new HttpParams()
    .set('pageNumber', pageNumber.toString())
    .set('pageSize', pageSize.toString())
    .set('startDate', DateUtils.formatToShortDate(startDate))
    .set('endDate', DateUtils.formatToShortDate(endDate))
    .set('code', code ? code.toString() : "")
    .set('accountUid', accountUid ? accountUid : "")
    .set('chartOfAccountUid', chartOfAccountUid ? chartOfAccountUid : "");
    return super.get<PageResultDto<TransactionSearchDto>>(TRANSACTION_ENDPOINT+"/histories", params);
  }

  public persist(transaction: TransactionDto) {
    if (!transaction) {
      throw new Error(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
    }
    if (transaction.uid) {
      return this.create(transaction);
    } else {
      return this.update(transaction);
    }
  }

  public repair(transaction: TransactionRepairDto) {
    return super.put(transaction, TRANSACTION_ENDPOINT+"/repair");
  }

  public create(transaction: TransactionDto) {
    return super.post(transaction, TRANSACTION_ENDPOINT);
  }
  
  public update(transaction: TransactionDto) {
    return super.put(transaction, TRANSACTION_ENDPOINT);
  }
  
  public remove(uid: string) {
    return super.delete(uid, TRANSACTION_ENDPOINT);
  }

  public transfer(transfer: TransferRequestDto) {
    return super.post(transfer, TRANSFER_ENDPOINT);
  }
  
}
