import { Injectable } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { USER_ENDPOINT_API } from '../api-endpoint-constants';
import { UserSearchDto } from '../../dto/user/user-search.dto';
import { UserDto } from '../../dto/user/user.dto';
import { TranslateService } from '@ngx-translate/core';
import { PageResultDto } from '../../dto/page-result.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService extends RestApiService {

  constructor(protected http: HttpClient, protected translateService: TranslateService) {
    super(http, translateService);
  }

  public persist(user: UserDto) {
    if (!user) {
      throw new Error(this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG'));
    }
    if (user.uid) {
      return this.create(user);
    } else {
      return this.update(user);
    }
  }

  public filter(pageNumber: number, pageSize: number, code: number, name: string) {
    const params = new HttpParams()
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString())
      .set('code', code ? code.toString() : "")
      .set('name', name ? name : "");
    return super.get<PageResultDto<UserSearchDto>>(USER_ENDPOINT_API, params);
  }

  public findById(uid : string) {
    return super.get<UserDto>(USER_ENDPOINT_API+`/${uid}`);
  }

  public create(user: UserDto) {
    return super.post(user, USER_ENDPOINT_API);
  }

  public update(user: UserDto) {
    return super.put(user, USER_ENDPOINT_API);
  }

  public remove(uid: string) {
    return super.delete(uid, USER_ENDPOINT_API);
  }
  
}
