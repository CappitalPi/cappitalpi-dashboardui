import { Injectable } from '@angular/core';
import { AccountPayRecPaymentDto } from '../../../dto/account-pay-rec/account-pay-rec-payment.dto';
import { AccountPaymentMode } from './account-payment-mode.enum';
import { PaymentAccountPayablePersistenceStrategy } from './payment-account-payable-persistence.strategy';
import { PaymentAccountReceivablePersistenceStrategy } from './payment-account-receivable-persistence.strategy';
import { PaymentPersistenceStrategy } from './payment-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AccountPaymentPersistenceContext {

  private strategies: Map<AccountPaymentMode, PaymentPersistenceStrategy<AccountPayRecPaymentDto>>;

  constructor(paymentAccountPayablePersistenceStrategy: PaymentAccountPayablePersistenceStrategy,
              paymentAccountReceivablePersistenceStrategy: PaymentAccountReceivablePersistenceStrategy) {
    this.strategies = new Map<AccountPaymentMode, PaymentPersistenceStrategy<AccountPayRecPaymentDto>>();
    this.strategies.set(paymentAccountPayablePersistenceStrategy.mode(), paymentAccountPayablePersistenceStrategy);
    this.strategies.set(paymentAccountReceivablePersistenceStrategy.mode(), paymentAccountReceivablePersistenceStrategy);
  }

  public context(mode: AccountPaymentMode) {
    return this.strategies.get(mode);
  }

}
