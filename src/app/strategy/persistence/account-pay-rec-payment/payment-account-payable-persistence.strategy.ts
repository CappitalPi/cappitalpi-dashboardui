import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountPayRecPaymentDto } from "../../../dto/account-pay-rec/account-pay-rec-payment.dto";
import { AccountPayableService } from "../../../service/account-payable/account-payable.service";
import { ACCOUNT_PAYABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { PaymentAccountPayRecValidator } from "../../../validator/account-pay-rec/payment-account-pay-rec.validator";
import { AccountPaymentMode } from "./account-payment-mode.enum";
import { PaymentPersistenceStrategy } from "./payment-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class PaymentAccountPayablePersistenceStrategy extends PaymentPersistenceStrategy<AccountPayRecPaymentDto> {
    
    constructor(protected validator: PaymentAccountPayRecValidator, protected accountService: AccountPayableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public mode(): AccountPaymentMode {
        return AccountPaymentMode.ACCOUNT_PAYABLE;
    }

    public pay(account: AccountPayRecPaymentDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        return super.pay(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_PAYABLE_ENDPOINT + "/payment";
    }

} 