import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RestApiService } from "../../../service/rest-api.service";
import { ObjectValidator } from "../../../validator/object-validator";
import { PersistenceStrategy } from "../persistence.strategy";
import { AccountPaymentMode } from "./account-payment-mode.enum";

@Injectable({
    providedIn: 'root',
})
export abstract class PaymentPersistenceStrategy<T> {

    constructor(protected validator: ObjectValidator<T>, protected service: RestApiService) {
    }

    public abstract mode(): AccountPaymentMode;

    public pay(t: T): Observable<any> {
        const error = this.validator.validate(t);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.service.post(t, this.endPoint()).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

    public abstract endPoint(): string;

} 