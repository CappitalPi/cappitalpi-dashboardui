import { Injectable } from '@angular/core';
import { AccountPayRecRequestDto } from '../../../dto/account-pay-rec/account-pay-rec-request.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateAccountPayablePersistenceStrategy } from './create-account-payable-persistence.strategy';
import { UpdateAccountPayablePersistenceStrategy } from './update-account-payable-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AccountPayablePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<AccountPayRecRequestDto>>;

  constructor(createAccountPayablePersistence: CreateAccountPayablePersistenceStrategy,
              updateAccountPayablePersistence: UpdateAccountPayablePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<AccountPayRecRequestDto>>();
    this.strategies.set(createAccountPayablePersistence.mode(), createAccountPayablePersistence);
    this.strategies.set(updateAccountPayablePersistence.mode(), updateAccountPayablePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
