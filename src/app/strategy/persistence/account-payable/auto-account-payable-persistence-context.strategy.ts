import { Injectable } from '@angular/core';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { AutoAccountPayRecRequestDto } from '../../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { CreateAutoAccountPayablePersistenceStrategy } from './auto-create-account-payable-persistence.strategy';
import { UpdateAutoAccountPayablePersistenceStrategy } from './auto-update-account-payable-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AutoAccountPayablePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<AutoAccountPayRecRequestDto>>;

  constructor(createAccountPayablePersistence: CreateAutoAccountPayablePersistenceStrategy,
              updateAccountPayablePersistence: UpdateAutoAccountPayablePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<AutoAccountPayRecRequestDto>>();
    this.strategies.set(createAccountPayablePersistence.mode(), createAccountPayablePersistence);
    this.strategies.set(updateAccountPayablePersistence.mode(), updateAccountPayablePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
