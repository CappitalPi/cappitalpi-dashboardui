import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AUTO_ACCOUNT_PAYABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";
import { AutoAccountPayableService } from "../../../service/account-payable/auto-account-payable.service";
import { AutoAccountPayRecRequestDto } from "../../../dto/account-pay-rec/auto-account-pay-rec-request.dto";
import { CreateAutoAccountPayableValidator } from "../../../validator/account-payable/create-auto-account-payable.validator";

@Injectable({
    providedIn: 'root',
})
export class CreateAutoAccountPayablePersistenceStrategy extends CreatePersistenceStrategy<AutoAccountPayRecRequestDto> {
    
    constructor(protected validator: CreateAutoAccountPayableValidator, protected accountService: AutoAccountPayableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AutoAccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return AUTO_ACCOUNT_PAYABLE_ENDPOINT;
    }

} 