import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AUTO_ACCOUNT_PAYABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";
import { AutoAccountPayRecRequestDto } from "../../../dto/account-pay-rec/auto-account-pay-rec-request.dto";
import { AutoAccountPayableService } from "../../../service/account-payable/auto-account-payable.service";
import { UpdateAutoAccountPayableValidator } from "../../../validator/account-payable/update-auto-account-payable.validator";

@Injectable({
    providedIn: 'root',
})
export class UpdateAutoAccountPayablePersistenceStrategy extends UpdatePersistenceStrategy<AutoAccountPayRecRequestDto> {
    
    constructor(protected validator: UpdateAutoAccountPayableValidator, protected accountService: AutoAccountPayableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AutoAccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return AUTO_ACCOUNT_PAYABLE_ENDPOINT;
    }

} 