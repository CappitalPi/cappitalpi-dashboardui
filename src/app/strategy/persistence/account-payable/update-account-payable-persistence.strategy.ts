import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountPayRecRequestDto } from "../../../dto/account-pay-rec/account-pay-rec-request.dto";
import { AccountPayableService } from "../../../service/account-payable/account-payable.service";
import { ACCOUNT_PAYABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { UpdateAccountPayableValidator } from "../../../validator/account-payable/update-account-payable.validator";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateAccountPayablePersistenceStrategy extends UpdatePersistenceStrategy<AccountPayRecRequestDto> {
    
    constructor(protected validator: UpdateAccountPayableValidator, protected accountService: AccountPayableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_PAYABLE_ENDPOINT;
    }

} 