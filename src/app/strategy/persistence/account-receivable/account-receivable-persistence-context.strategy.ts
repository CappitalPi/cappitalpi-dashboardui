import { Injectable } from '@angular/core';
import { AccountPayRecRequestDto } from '../../../dto/account-pay-rec/account-pay-rec-request.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateAccountReceivablePersistenceStrategy } from './create-account-receivable-persistence.strategy';
import { UpdateAccountReceivablePersistenceStrategy } from './update-account-receivable-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AccountReceivablePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<AccountPayRecRequestDto>>;

  constructor(createAccountReceivablePersistence: CreateAccountReceivablePersistenceStrategy,
              updateAccountReceivablePersistence: UpdateAccountReceivablePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<AccountPayRecRequestDto>>();
    this.strategies.set(createAccountReceivablePersistence.mode(), createAccountReceivablePersistence);
    this.strategies.set(updateAccountReceivablePersistence.mode(), updateAccountReceivablePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
