import { Injectable } from '@angular/core';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { AutoAccountPayRecRequestDto } from '../../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { CreateAutoAccountReceivablePersistenceStrategy } from './auto-create-account-receivable-persistence.strategy';
import { UpdateAutoAccountReceivablePersistenceStrategy } from './auto-update-account-receivable-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AutoAccountReceivablePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<AutoAccountPayRecRequestDto>>;

  constructor(createAccountReceivablePersistence: CreateAutoAccountReceivablePersistenceStrategy,
              updateAccountReceivablePersistence: UpdateAutoAccountReceivablePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<AutoAccountPayRecRequestDto>>();
    this.strategies.set(createAccountReceivablePersistence.mode(), createAccountReceivablePersistence);
    this.strategies.set(updateAccountReceivablePersistence.mode(), updateAccountReceivablePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
