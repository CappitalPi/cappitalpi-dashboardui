import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AUTO_ACCOUNT_RECEIVABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";
import { AutoAccountPayRecRequestDto } from "../../../dto/account-pay-rec/auto-account-pay-rec-request.dto";
import { AutoAccountReceivableService } from "../../../service/account-receivable/auto-account-receivable.service";
import { UpdateAutoAccountReceivableValidator } from "../../../validator/account-receivable/update-auto-account-receivable.validator";

@Injectable({
    providedIn: 'root',
})
export class UpdateAutoAccountReceivablePersistenceStrategy extends UpdatePersistenceStrategy<AutoAccountPayRecRequestDto> {
    
    constructor(protected validator: UpdateAutoAccountReceivableValidator, protected accountService: AutoAccountReceivableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AutoAccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return AUTO_ACCOUNT_RECEIVABLE_ENDPOINT;
    }

} 