import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountPayRecRequestDto } from "../../../dto/account-pay-rec/account-pay-rec-request.dto";
import { AccountReceivableService } from "../../../service/account-receivable/account-receivable.service";
import { ACCOUNT_RECEIVABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { CreateAccountReceivableValidator } from "../../../validator/account-receivable/create-account-receivable.validator";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateAccountReceivablePersistenceStrategy extends CreatePersistenceStrategy<AccountPayRecRequestDto> {
    
    constructor(protected validator: CreateAccountReceivableValidator, protected accountService: AccountReceivableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_RECEIVABLE_ENDPOINT;
    }

} 