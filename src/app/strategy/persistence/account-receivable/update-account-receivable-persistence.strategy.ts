import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountPayRecRequestDto } from "../../../dto/account-pay-rec/account-pay-rec-request.dto";
import { AccountReceivableService } from "../../../service/account-receivable/account-receivable.service";
import { ACCOUNT_RECEIVABLE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { UpdateAccountReceivableValidator } from "../../../validator/account-receivable/update-account-receivable.validator";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateAccountReceivablePersistenceStrategy extends UpdatePersistenceStrategy<AccountPayRecRequestDto> {
    
    constructor(protected validator: UpdateAccountReceivableValidator, protected accountService: AccountReceivableService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AccountPayRecRequestDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_RECEIVABLE_ENDPOINT;
    }

} 