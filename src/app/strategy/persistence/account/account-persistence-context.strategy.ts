import { Injectable } from '@angular/core';
import { AccountDto } from '../../../dto/account/account.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateAccountPersistenceStrategy } from './create-account-persistence.strategy';
import { UpdateAccountPersistenceStrategy } from './update-account-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class AccountPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<AccountDto>>;

  constructor(createAccountPersistence: CreateAccountPersistenceStrategy,
              updateAccountPersistence: UpdateAccountPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<AccountDto>>();
    this.strategies.set(createAccountPersistence.mode(), createAccountPersistence);
    this.strategies.set(updateAccountPersistence.mode(), updateAccountPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
