import { Injectable } from "@angular/core";
import { AccountService } from "../../../service/account/account.service";
import { CreateAccountValidator } from "../../../validator/account/create-account.validator";
import { AccountDto } from "../../../dto/account/account.dto";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";
import { ACCOUNT_ENDPOINT } from "../../../service/api-endpoint-constants";
import { Observable } from "rxjs";
import { SessionService } from "../../../service/auth/session-service";

@Injectable({
    providedIn: 'root',
})
export class CreateAccountPersistenceStrategy extends CreatePersistenceStrategy<AccountDto> {
    
    constructor(protected validator: CreateAccountValidator, protected accountService: AccountService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AccountDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_ENDPOINT;
    }

} 