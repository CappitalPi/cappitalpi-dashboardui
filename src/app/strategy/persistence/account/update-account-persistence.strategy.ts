import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountDto } from "../../../dto/account/account.dto";
import { AccountService } from "../../../service/account/account.service";
import { ACCOUNT_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { UpdateAccountValidator } from "../../../validator/account/update-account.validator";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateAccountPersistenceStrategy extends UpdatePersistenceStrategy<AccountDto> {
    
    constructor(protected validator: UpdateAccountValidator, protected accountService: AccountService, private sessionService: SessionService) {
        super(validator, accountService);
    }

    public persist(account: AccountDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return ACCOUNT_ENDPOINT;
    }

} 