import { Injectable } from '@angular/core';
import { AccountDto } from '../../../dto/account/account.dto';
import { ChartOfAccountDto } from '../../../dto/chart-of-account/chart-of-account.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateChartOfAccountPersistenceStrategy } from './create-chart-of-account-persistence.strategy';
import { UpdateChartOfAccountPersistenceStrategy } from './update-chart-of-account-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class ChartOfAccountPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<ChartOfAccountDto>>;

  constructor(createAccountPersistence: CreateChartOfAccountPersistenceStrategy,
              updateAccountPersistence: UpdateChartOfAccountPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<ChartOfAccountDto>>();
    this.strategies.set(createAccountPersistence.mode(), createAccountPersistence);
    this.strategies.set(updateAccountPersistence.mode(), updateAccountPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
