import { Injectable } from "@angular/core";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";
import { CHART_OF_ACCOUNT_ENDPOINT } from "../../../service/api-endpoint-constants";
import { ChartOfAccountDto } from "../../../dto/chart-of-account/chart-of-account.dto";
import { ChartOfAccountService } from "../../../service/chart-of-account/chart-of-account.service";
import { CreateChartOfAccountValidator } from "../../../validator/chart-of-account/create-chart-of-account.validator";
import { SessionService } from "../../../service/auth/session-service";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root',
})
export class CreateChartOfAccountPersistenceStrategy extends CreatePersistenceStrategy<ChartOfAccountDto> {
    
    constructor(protected validator: CreateChartOfAccountValidator, protected chartOfAccountService: ChartOfAccountService, private sessionService: SessionService) {
        super(validator, chartOfAccountService);
    }

    public persist(account: ChartOfAccountDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return CHART_OF_ACCOUNT_ENDPOINT;
    }

} 