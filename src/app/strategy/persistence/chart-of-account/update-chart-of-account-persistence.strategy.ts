import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ChartOfAccountDto } from "../../../dto/chart-of-account/chart-of-account.dto";
import { CHART_OF_ACCOUNT_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { ChartOfAccountService } from "../../../service/chart-of-account/chart-of-account.service";
import { UpdateChartOfAccountValidator } from "../../../validator/chart-of-account/update-chart-of-account.validator";
import { UpdatePersistenceStrategy } from "../update-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateChartOfAccountPersistenceStrategy extends UpdatePersistenceStrategy<ChartOfAccountDto> {
    
    constructor(protected validator: UpdateChartOfAccountValidator, protected chartOfAccountService: ChartOfAccountService, private sessionService: SessionService) {
        super(validator, chartOfAccountService);
    }

    public persist(account: ChartOfAccountDto): Observable<any> {
        account.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        account.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        return super.persist(account);
    }
    
    public endPoint(): string {
        return CHART_OF_ACCOUNT_ENDPOINT;
    }

} 