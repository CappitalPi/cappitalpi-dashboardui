import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { RestApiService } from "../../service/rest-api.service";
import { ObjectValidator } from "../../validator/object-validator";
import { PersistenceMode } from "./persistence-mode.enum";
import { PersistenceStrategy } from "./persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export abstract class CreatePersistenceStrategy<T> implements PersistenceStrategy<T> {

    constructor(protected validator: ObjectValidator<T>, protected service: RestApiService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(t: T): Observable<any> {
        const error = this.validator.validate(t);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.service.post(t, this.endPoint()).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

    public abstract endPoint(): string;

} 