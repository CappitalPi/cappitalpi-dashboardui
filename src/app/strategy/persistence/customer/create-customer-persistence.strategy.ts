import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ContactDto } from "../../../dto/contact/contact.dto";
import { PersonDto } from "../../../dto/person/person.dto";
import { SessionService } from "../../../service/auth/session-service";
import { CustomerService } from "../../../service/customer/customer.service";
import { CreatePersonValidator } from "../../../validator/person/create-person.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateCustomerPersistenceStrategy implements PersistenceStrategy<PersonDto> {

    constructor(private validator: CreatePersonValidator, private customerService: CustomerService, private sessionService: SessionService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(customer: PersonDto): Observable<any> {
        const error = this.validator.validate(customer);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.customerService.create(customer).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

    private addContacts(customer: PersonDto, newContacts: ContactDto[]) {
        if (newContacts?.length > 0) {
            let contacts = customer.contacts;
            if (!contacts) {
                contacts = []
            }
            newContacts.forEach(contact => {
                contacts.push(contact);
            });
        } 
    }

    private mergeContacts(customer: PersonDto): void {
        if (customer.emails?.length > 0) {
            const contacts = []
            customer.emails.forEach(element => {
                contacts.push(element.contact);
            });
            this.addContacts(customer, contacts);
        }
    }

} 