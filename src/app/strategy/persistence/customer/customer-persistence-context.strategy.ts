import { Injectable } from '@angular/core';
import { PersonDto } from '../../../dto/person/person.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateCustomerPersistenceStrategy } from './create-customer-persistence.strategy';
import { UpdateCustomerPersistenceStrategy } from './update-customer-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class CustomerPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<PersonDto>>;

  constructor(createCustomerPersistence: CreateCustomerPersistenceStrategy,
              updateCustomerPersistence: UpdateCustomerPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<PersonDto>>();
    this.strategies.set(createCustomerPersistence.mode(), createCustomerPersistence);
    this.strategies.set(updateCustomerPersistence.mode(), updateCustomerPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
