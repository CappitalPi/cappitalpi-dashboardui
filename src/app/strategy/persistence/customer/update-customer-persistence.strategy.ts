import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PersonDto } from "../../../dto/person/person.dto";
import { CustomerService } from "../../../service/customer/customer.service";
import { CreatePersonValidator } from "../../../validator/person/create-person.validator";
import { UpdatePersonValidator } from "../../../validator/person/update-person.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateCustomerPersistenceStrategy implements PersistenceStrategy<PersonDto> {

    constructor(private validator: UpdatePersonValidator, private customerService: CustomerService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.UPDATE;
    }

    public persist(customer: PersonDto): Observable<any> {
        const error = this.validator.validate(customer);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.customerService.update(customer).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 