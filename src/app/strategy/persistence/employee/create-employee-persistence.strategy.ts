import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PersonDto } from "../../../dto/person/person.dto";
import { EMPLOYEE_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { EmployeeService } from "../../../service/employee/employee.service";
import { CreatePersonValidator } from "../../../validator/person/create-person.validator";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateEmployeePersistenceStrategy extends CreatePersistenceStrategy<PersonDto> {

    constructor(protected validator: CreatePersonValidator, protected employeeService: EmployeeService, private sessionService: SessionService) {
        super(validator, employeeService);
    }

    public persist(person: PersonDto): Observable<any> {
        person.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        return super.persist(person);
    }
    
    public endPoint(): string {
        return EMPLOYEE_ENDPOINT;
    }
} 