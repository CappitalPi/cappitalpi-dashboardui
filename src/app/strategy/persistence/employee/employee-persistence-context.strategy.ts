import { Injectable } from '@angular/core';
import { PersonDto } from '../../../dto/person/person.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateEmployeePersistenceStrategy } from './create-employee-persistence.strategy';
import { UpdateEmployeePersistenceStrategy } from './update-employee-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class EmployeePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<PersonDto>>;

  constructor(createEmployeePersistence: CreateEmployeePersistenceStrategy,
              updateEmployeePersistence: UpdateEmployeePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<PersonDto>>();
    this.strategies.set(createEmployeePersistence.mode(), createEmployeePersistence);
    this.strategies.set(updateEmployeePersistence.mode(), updateEmployeePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
