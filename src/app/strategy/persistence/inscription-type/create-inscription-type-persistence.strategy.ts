import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InscriptionTypeDto } from "../../../dto/inscription/inscription-type.dto";
import { SessionService } from "../../../service/auth/session-service";
import { InscriptionTypeService } from "../../../service/inscription-type/inscription-type.service";
import { CreateInscriptionTypeValidator } from "../../../validator/inscription-type/create-inscription-type.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateInscriptionTypePersistenceStrategy implements PersistenceStrategy<InscriptionTypeDto> {

    constructor(private validator: CreateInscriptionTypeValidator, private inscriptionTypeService: InscriptionTypeService, private sessionService: SessionService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(inscriptionType: InscriptionTypeDto): Observable<any> {
        inscriptionType.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        const error = this.validator.validate(inscriptionType);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.inscriptionTypeService.create(inscriptionType).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 