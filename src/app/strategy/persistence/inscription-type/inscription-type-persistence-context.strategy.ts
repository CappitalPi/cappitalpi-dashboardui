import { Injectable } from '@angular/core';
import { InscriptionTypeDto } from '../../../dto/inscription/inscription-type.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateInscriptionTypePersistenceStrategy } from './create-inscription-type-persistence.strategy';
import { UpdateInscriptionTypePersistenceStrategy } from './update-inscription-type-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class InscriptionTypePersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<InscriptionTypeDto>>;

  constructor(createInscriptionTypePersistence: CreateInscriptionTypePersistenceStrategy,
              updateInscriptionTypePersistence: UpdateInscriptionTypePersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<InscriptionTypeDto>>();
    this.strategies.set(createInscriptionTypePersistence.mode(), createInscriptionTypePersistence);
    this.strategies.set(updateInscriptionTypePersistence.mode(), updateInscriptionTypePersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
