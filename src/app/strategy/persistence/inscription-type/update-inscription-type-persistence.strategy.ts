import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { InscriptionTypeDto } from "../../../dto/inscription/inscription-type.dto";
import { InscriptionTypeService } from "../../../service/inscription-type/inscription-type.service";
import { UpdateInscriptionTypeValidator } from "../../../validator/inscription-type/update-inscription-type.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateInscriptionTypePersistenceStrategy implements PersistenceStrategy<InscriptionTypeDto> {

    constructor(private validator: UpdateInscriptionTypeValidator, private inscriptionTypeService: InscriptionTypeService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.UPDATE;
    }

    public persist(inscriptionType: InscriptionTypeDto): Observable<any> {
        const error = this.validator.validate(inscriptionType);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.inscriptionTypeService.update(inscriptionType).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 