export enum PersistenceMode {

    CREATE,
    UPDATE,
    DELETE,
    SEARCH
    
}
