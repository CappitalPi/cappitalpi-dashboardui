import { Observable } from "rxjs";
import { PersistenceMode } from "./persistence-mode.enum";

export interface PersistenceStrategy<T> {

    persist(t: T): Observable<any>;

    mode(): PersistenceMode;

} 