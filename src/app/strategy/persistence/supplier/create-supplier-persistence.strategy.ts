import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PersonDto } from "../../../dto/person/person.dto";
import { SUPPLIER_ENDPOINT } from "../../../service/api-endpoint-constants";
import { SessionService } from "../../../service/auth/session-service";
import { SupplierService } from "../../../service/supplier/supplier.service";
import { CreatePersonValidator } from "../../../validator/person/create-person.validator";
import { CreatePersistenceStrategy } from "../create-persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateSupplierPersistenceStrategy extends CreatePersistenceStrategy<PersonDto> {

    constructor(protected validator: CreatePersonValidator, protected supplierService: SupplierService, private sessionService: SessionService) {
        super(validator, supplierService);
    }

    public persist(person: PersonDto): Observable<any> {
        person.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        return super.persist(person);
    }
    
    public endPoint(): string {
        return SUPPLIER_ENDPOINT;
    }
} 