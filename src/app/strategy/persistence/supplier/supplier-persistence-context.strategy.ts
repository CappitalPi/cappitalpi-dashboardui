import { Injectable } from '@angular/core';
import { PersonDto } from '../../../dto/person/person.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateSupplierPersistenceStrategy } from './create-supplier-persistence.strategy';
import { UpdateSupplierPersistenceStrategy } from './update-supplier-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class SupplierPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<PersonDto>>;

  constructor(createSupplierPersistence: CreateSupplierPersistenceStrategy,
              updateSupplierPersistence: UpdateSupplierPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<PersonDto>>();
    this.strategies.set(createSupplierPersistence.mode(), createSupplierPersistence);
    this.strategies.set(updateSupplierPersistence.mode(), updateSupplierPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
