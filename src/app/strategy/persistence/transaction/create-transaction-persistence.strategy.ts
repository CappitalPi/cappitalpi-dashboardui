import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TransactionDto } from "../../../dto/transaction/transaction.dto";
import { SessionService } from "../../../service/auth/session-service";
import { TransactionService } from "../../../service/transaction/transaction.service";
import { CreateTransactionValidator } from "../../../validator/transaction/create-transaction.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateTransactionPersistenceStrategy implements PersistenceStrategy<TransactionDto> {

    constructor(private validator: CreateTransactionValidator, private transactionService: TransactionService, private sessionService: SessionService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(transaction: TransactionDto): Observable<any> {
        transaction.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        transaction.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        const error = this.validator.validate(transaction);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.transactionService.create(transaction).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 