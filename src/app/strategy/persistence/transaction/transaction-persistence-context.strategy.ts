import { Injectable } from '@angular/core';
import { TransactionDto } from '../../../dto/transaction/transaction.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateTransactionPersistenceStrategy } from './create-transaction-persistence.strategy';
import { UpdateTransactionPersistenceStrategy } from './update-transaction-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class TransactionPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<TransactionDto>>;

  constructor(createTransactionPersistence: CreateTransactionPersistenceStrategy,
              updateTransactionPersistence: UpdateTransactionPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<TransactionDto>>();
    this.strategies.set(createTransactionPersistence.mode(), createTransactionPersistence);
    this.strategies.set(updateTransactionPersistence.mode(), updateTransactionPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
