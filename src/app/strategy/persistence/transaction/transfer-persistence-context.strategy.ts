import { Injectable } from '@angular/core';
import { TransferRequestDto } from '../../../dto/transaction/transfer-request.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { TransferPersistenceStrategy } from './transfer-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class TransferPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<TransferRequestDto>>;

  constructor(transferPersistence: TransferPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<TransferRequestDto>>();
    this.strategies.set(transferPersistence.mode(), transferPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
