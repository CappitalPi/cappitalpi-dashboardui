import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TransferRequestDto } from "../../../dto/transaction/transfer-request.dto";
import { SessionService } from "../../../service/auth/session-service";
import { TransactionService } from "../../../service/transaction/transaction.service";
import { TransferValidator } from "../../../validator/transaction/transfer.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class TransferPersistenceStrategy implements PersistenceStrategy<TransferRequestDto> {

    constructor(private validator: TransferValidator, private transactionService: TransactionService, private sessionService: SessionService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(transfer: TransferRequestDto): Observable<any> {
        transfer.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        transfer.appAccountUid = this.sessionService.getUser() ? this.sessionService.getUser().ownerUid : null;
        const error = this.validator.validate(transfer);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.transactionService.transfer(transfer).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 