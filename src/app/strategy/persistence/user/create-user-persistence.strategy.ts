import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserDto } from "../../../dto/user/user.dto";
import { SessionService } from "../../../service/auth/session-service";
import { UserService } from "../../../service/user/user.service";
import { CreateUserValidator } from "../../../validator/user/create-user.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class CreateUserPersistenceStrategy implements PersistenceStrategy<UserDto> {

    constructor(private validator: CreateUserValidator, private userService: UserService, private sessionService: SessionService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.CREATE;
    }

    public persist(user: UserDto): Observable<any> {
        user.registerUid = this.sessionService.getUser() ? this.sessionService.getUser().registerUid : null;
        const error = this.validator.validate(user);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.userService.create(user).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 