import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserDto } from "../../../dto/user/user.dto";
import { UserService } from "../../../service/user/user.service";
import { CreateUserValidator } from "../../../validator/user/create-user.validator";
import { UpdateUserValidator } from "../../../validator/user/update-user.validator";
import { PersistenceMode } from "../persistence-mode.enum";
import { PersistenceStrategy } from "../persistence.strategy";

@Injectable({
    providedIn: 'root',
})
export class UpdateUserPersistenceStrategy implements PersistenceStrategy<UserDto> {

    constructor(private validator: UpdateUserValidator, private userService: UserService) {
    }

    public mode(): PersistenceMode {
        return PersistenceMode.UPDATE;
    }

    public persist(user: UserDto): Observable<any> {
        const error = this.validator.validate(user);
        if (error) {
            return new Observable(observer => observer.error(error));
        }
        return new Observable(observer => {
            this.userService.update(user).subscribe(
                data => {
                    return observer.next(data);
                },
                error => {
                    return observer.error(error);
                }
            );
        });
    }

} 