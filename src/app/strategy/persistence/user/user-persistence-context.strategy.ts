import { Injectable } from '@angular/core';
import { UserDto } from '../../../dto/user/user.dto';
import { PersistenceMode } from '../persistence-mode.enum';
import { PersistenceStrategy } from '../persistence.strategy';
import { CreateUserPersistenceStrategy } from './create-user-persistence.strategy';
import { UpdateUserPersistenceStrategy } from './update-user-persistence.strategy';

@Injectable({
  providedIn: 'root',
})
export class UserPersistenceContext {

  private strategies: Map<PersistenceMode, PersistenceStrategy<UserDto>>;

  constructor(createUserPersistence: CreateUserPersistenceStrategy,
              updateUserPersistence: UpdateUserPersistenceStrategy) {
    this.strategies = new Map<PersistenceMode, PersistenceStrategy<UserDto>>();
    this.strategies.set(createUserPersistence.mode(), createUserPersistence);
    this.strategies.set(updateUserPersistence.mode(), updateUserPersistence);
  }

  public context(mode: PersistenceMode) {
    return this.strategies.get(mode);
  }

}
