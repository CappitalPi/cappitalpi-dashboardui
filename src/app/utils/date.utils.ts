import * as moment from "moment";

export const YYYY_MM_DD = 'YYYY-MM-DD';
export const DD_MMM = 'DD MMM';
export const ZULU_MOMENT = 'YYYY-MM-DDTHH:mm:ss.SSS[Z]';
export const OFFSET_MOMENT = "YYYY-MM-DD HH:mm:ssZZ";
export const OFFSET_MOMENT2 = "YYYY-MM-DDTHH:mm:ssZ";
export const OFFSET_MOMENT3 = "YYYY-MM-DDTHH:mmZ";

//2019-09-16T00:00-03:00[America/Sao_Paulo]

export class DateUtils {

    public static now(timeToZero : boolean = false) : Date {
        const now : Date = new Date();
        if (timeToZero) {
            return this.setTimeToZero(now);
        }
        return moment.utc(now).toDate();
    }

    public static strToDate(date: string): Date {
        if (date != null) {
            return moment.utc(date).toDate();
        }
        return null;
    }
    
    public static dateToJsonUtc(date : Date, timeToZero : boolean = false) : string {
        if (date && date instanceof Date) {
            if (timeToZero) {
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
                date.setMilliseconds(0);
            }
            return moment.utc(date).toJSON();
        }
        return null;
    }

    public static formatToShortDate(date: Date): string {
        if (date != null) {
            return moment.utc(date).format(YYYY_MM_DD);
        }
        return null;
    }

    public static formatToDayMonth(date: Date): string {
        if (date != null) {
            return moment.utc(date).format(DD_MMM);
        }
        return null;
    }
    
    public static isDateValid(date) : boolean {
        if (date) {
            return moment.utc(date).isValid();
        }
        return false;
    }

    public static setTimeToZero(date : Date) : Date {
        if (date && date instanceof Date) {
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            date.setMilliseconds(0);
            return date;
        }
        return null;
    }

    public static addDays(date: Date, days: number): Date {
        date.setDate(date.getDate() + days);
        return date;
    }

    public static substractDays(date: Date, days: number): Date {
        date.setDate(date.getDate() - days);
        return date;
    }

    public static isSameOrAfter(date1: Date, date2: Date) {
        return moment.utc(date1).isSameOrAfter(date2);
    }

    public static calcDates(date1: Date, date2: Date) {
        if (date1 != null && date2 != null) {
            var momentDate1 = moment.utc(date1);
            var momentDate2 = moment.utc(date2);
            return Math.round(moment.duration(momentDate1.diff(momentDate2)).asDays());
        }
        return null;
    }

}
