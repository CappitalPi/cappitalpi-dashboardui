import { EnumValues } from 'enum-values';

const EMPTY_ITEM_VALUE : Number = -1;

export class EnumUtils {

  public static getEmptyValue() {
    return EMPTY_ITEM_VALUE;
  }

  public static getValueFromName(enumType, name) {
    for (let entry of EnumValues.getNamesAndValues(enumType)) {
      if (entry.name == name) {
        return entry.value;
      }
    }
    return null;
  }

  public static getEnumTypeFromName(enumType, name) {
    for (let entry of EnumValues.getNamesAndValues(enumType)) {
      if (entry.name == name) {
        return entry;
      }
    }
    return null;
  }

  public static getEnumTypeFromValue(enumType, value) {
    for (let entry of EnumValues.getNamesAndValues(enumType)) {
      if (entry.value == value) {
        return entry;
      }
    }
    return null;
  }

  public static getNameFromValue(enumType, value) {
    for (let entry of EnumValues.getNamesAndValues(enumType)) {
      if (entry.value == value) {
        return entry.name;
      }
    }
    return null;
  }

   public static getList(enumType : any, hasEmptyValue : boolean) {
    var select = [];
    if (hasEmptyValue) {
      select.push({value: this.getEmptyValue(), name:'NONE'}); //TODO
    }
    for (let entry of EnumValues.getNamesAndValues(enumType)) {
      let selectItem = {value: entry.value, name: entry.name};
      select.push(selectItem);
    }
    return select;
  }

}
