import { FormControl, NgForm } from "@angular/forms";

export class FormValidationUtils {

    public static isFieldInvalid(field: FormControl): boolean {
        return field && field.invalid && (field.dirty || field.touched);
    }

    public static markFieldsAsTouched(form: NgForm) {
        const controls = form.controls;
        Object.keys(controls).forEach(controlName => controls[controlName].markAsTouched())
    }

    public static keyPressNumbers(event) {
      var charCode = (event.which) ? event.which : event.keyCode;
      // Only Numbers 0-9
      if ((charCode < 48 || charCode > 57)) {
        event.preventDefault();
        return false;
      }
      return true;
    }

}