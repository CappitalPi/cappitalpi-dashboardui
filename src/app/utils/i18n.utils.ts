const PT_BR: string = "pt_BR";
const EN_US: string = "en_CA";

export class I18NUtils {

    public static getBackEndLanguage(defaultLang: string) {
        switch(defaultLang) {
            case "pt": return PT_BR;
            case "en": return EN_US;
            default: return EN_US;
        }
    }

}
