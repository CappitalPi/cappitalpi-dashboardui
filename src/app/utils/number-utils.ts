import { CurrencyPipe } from "@angular/common";

const CURRENCY_SYMBOL = ' ';
export class NumberUtils {

    public static strToCurrency(amount: string): number {
        if (amount?.length > 0) {
            return Number(NumberUtils.unformat(amount));
        }
        return null;
    }

    public static currencyToStr(amount: number, pipe: CurrencyPipe): string {
        if (amount != null) {
            return pipe.transform(amount, CURRENCY_SYMBOL);
        }
        return null;
    }

    private static unformat(value: string): string {
        return value?.replace(/[^0-9.]+/g, '');
    }

    // Number(this.transaction.amount).toFixed(2)


}
