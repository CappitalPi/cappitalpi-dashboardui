import { max } from "rxjs/operators";

export class StringUtils {
    static readonly EMPTY = "";

    public static normalizeSpace(value: string) {
        if (value) {
            return value.replace(/\s+/g, ' ').trim();
        }
        return value;
    }

    public static ignoreAccents(text: string) {
        if (text?.length > 0) {
            return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        }
        return "";
    }

    public static formatLongText(text: string){
        return this.formatLongTextMaxLength(text, 255);
    }

    public static formatLongTextMaxLength(text: string, maxLength: number){
        if (text?.length > 0 && maxLength > 0) {
            return text.substring(0,maxLength).concat('...');
        }
        return this.EMPTY;
    }
}
