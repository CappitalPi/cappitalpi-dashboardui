import { AutoAccountPayRecRequestDto } from "../../../dto/account-pay-rec/auto-account-pay-rec-request.dto";
import { FrequencyType } from "../../../dto/account-pay-rec/frequency-type.enum";
import { RepeatType } from "../../../dto/account-pay-rec/repeat-type.enum";

export class AutoAccountValidatorHelper {

  public static isFrequencyByDayValid(accountReceivable: AutoAccountPayRecRequestDto): boolean {
    if (FrequencyType.DAY == accountReceivable.frequencyType && RepeatType.BY_DUE_DATE == accountReceivable.repeatType) {
      return accountReceivable.sunday || accountReceivable.monday || accountReceivable.tuesday || accountReceivable.wednesday || accountReceivable.thursday || accountReceivable.friday || accountReceivable.saturday;
    }
    if (RepeatType.BY_AMOUNT_OF_DAYS == accountReceivable.repeatType) {
        return accountReceivable.frequencyAmount > 0;
    }
    return true;
  }

}