import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { AccountPayRecPaymentDto } from '../../dto/account-pay-rec/account-pay-rec-payment.dto';

@Injectable({
  providedIn: 'root'
})
export class PaymentAccountPayRecValidator implements ObjectValidator<AccountPayRecPaymentDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(accountPayment: AccountPayRecPaymentDto): string {
      if (!accountPayment?.accountPayRecUid) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      if (accountPayment.cashAccountUid == null) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.CASH_ACCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (accountPayment.paymentDate == null) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.PAYMENT_DATE');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (accountPayment.paymentMethod == null) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.PAYMENT_METHOD');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (accountPayment.discount == null || accountPayment.discount < 0) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.DISCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (accountPayment.interest == null || accountPayment.interest < 0) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.INTEREST');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (accountPayment.amount == null || accountPayment.amount <= 0) {
        const fieldName = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.AMOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      return null;
  }

}
