import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { AccountPayRecRequestDto } from '../../dto/account-pay-rec/account-pay-rec-request.dto';

@Injectable({
  providedIn: 'root'
})
export class CreateAccountPayableValidator implements ObjectValidator<AccountPayRecRequestDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(accountPayable: AccountPayRecRequestDto): string {
    if (!accountPayable) {
        return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
    }
    if (accountPayable.personUid == null) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.SUPPLIER');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.chartOfAccountUid == null) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.CHART_OF_ACCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.issueDate == null) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.ISSUE_DATE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.discount == null || accountPayable.discount < 0) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.DISCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.interest == null || accountPayable.interest < 0) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.INTEREST');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.amount == null || accountPayable.amount <= 0) {
      const fieldName = this.translateService.instant('ACCOUNT_PAYABLE.FIELD.AMOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    return null;
  }

}
