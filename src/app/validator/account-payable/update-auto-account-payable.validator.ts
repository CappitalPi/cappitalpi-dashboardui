import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { AutoAccountPayRecRequestDto } from '../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { AutoAccountValidatorHelper } from '../account-pay-rec/helper/auto-account-validator.helper';

@Injectable({
  providedIn: 'root'
})
export class UpdateAutoAccountPayableValidator implements ObjectValidator<AutoAccountPayRecRequestDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(accountPayable: AutoAccountPayRecRequestDto): string {
    if (!accountPayable || !accountPayable.uid) {
        return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
    }
    if (accountPayable.personUid == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.CUSTOMER');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.chartOfAccountUid == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.CHART_OF_ACCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.frequencyStartDate == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.FREQUENCY_START_DATE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.frequencyType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.FREQUENCY_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.repeatType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.REPEAT_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (!AutoAccountValidatorHelper.isFrequencyByDayValid(accountPayable)) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.FREQUENCY_AMOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});  
    }
    if (accountPayable.costType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.COST_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.discount == null || accountPayable.discount < 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.DISCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.interest == null || accountPayable.interest < 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.INTEREST');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountPayable.amount == null || accountPayable.amount <= 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_PAYABLE.FIELD.AMOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    return null;  
  }

}
