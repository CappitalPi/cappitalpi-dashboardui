import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { AutoAccountPayRecRequestDto } from '../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { RepeatType } from '../../dto/account-pay-rec/repeat-type.enum';
import { FrequencyType } from '../../dto/account-pay-rec/frequency-type.enum';
import { AutoAccountValidatorHelper } from '../account-pay-rec/helper/auto-account-validator.helper';

@Injectable({
  providedIn: 'root'
})
export class CreateAutoAccountReceivableValidator implements ObjectValidator<AutoAccountPayRecRequestDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(accountReceivable: AutoAccountPayRecRequestDto): string {
    if (!accountReceivable) {
        return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
    }
    if (accountReceivable.personUid == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.CUSTOMER');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.chartOfAccountUid == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.CHART_OF_ACCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.frequencyStartDate == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.FREQUENCY_START_DATE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.frequencyType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.FREQUENCY_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.repeatType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.REPEAT_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (!AutoAccountValidatorHelper.isFrequencyByDayValid(accountReceivable)) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.FREQUENCY_AMOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});  
    }
    if (accountReceivable.costType == null) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.COST_TYPE');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.discount == null || accountReceivable.discount < 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.DISCOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.interest == null || accountReceivable.interest < 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.INTEREST');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    if (accountReceivable.amount == null || accountReceivable.amount <= 0) {
      const fieldName = this.translateService.instant('AUTO_ACCOUNT_RECEIVABLE.FIELD.AMOUNT');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    return null;
  }

}
