import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { AccountDto } from '../../dto/account/account.dto';

@Injectable({
  providedIn: 'root'
})
export class UpdateAccountValidator implements ObjectValidator<AccountDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(account: AccountDto): string {
      if (!account || !account.uid) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      return null;
  }

}
