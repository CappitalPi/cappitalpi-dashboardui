import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { ChartOfAccountDto } from '../../dto/chart-of-account/chart-of-account.dto';

@Injectable({
  providedIn: 'root'
})
export class UpdateChartOfAccountValidator implements ObjectValidator<ChartOfAccountDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(chartOfAccount: ChartOfAccountDto): string {
      if (!chartOfAccount || !chartOfAccount.uid) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      return null;
  }

}
