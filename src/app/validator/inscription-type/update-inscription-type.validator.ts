import { Injectable } from '@angular/core';
import { InscriptionTypeDto } from '../../dto/inscription/inscription-type.dto';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UpdateInscriptionTypeValidator implements ObjectValidator<InscriptionTypeDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(inscriptionType: InscriptionTypeDto): string {
    if (!inscriptionType) {
      return this.translateService.instant('INSCRIPTION_TYPE.APP.VALIDATION.NULL_OBJECT');
    }
    if (inscriptionType.name?.length <= 0) {
      const fieldName = this.translateService.instant('INSCRIPTION_TYPE.FIELD.NAME');
      return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
    }
    return null;
  }

}
