export interface ObjectValidator<T> {

    validate(t: T): string

 }