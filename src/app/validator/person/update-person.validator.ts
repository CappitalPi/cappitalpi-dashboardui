import { Injectable } from '@angular/core';
import { PersonDto } from '../../dto/person/person.dto';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UpdatePersonValidator implements ObjectValidator<PersonDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(customer: PersonDto): string {
      if (!customer || !customer.uid) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      return null;
  }

}
