import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { TransactionDto } from '../../dto/transaction/transaction.dto';

@Injectable({
  providedIn: 'root'
})
export class CreateTransactionValidator implements ObjectValidator<TransactionDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(transaction: TransactionDto): string {
      if (!transaction) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      if (!transaction.cashAccount || !transaction.cashAccount.uid) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.ACCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (!transaction.chartOfAccount || !transaction.chartOfAccount.uid) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.CHART_OF_ACCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (transaction.amount == null || transaction.amount <= 0) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.AMOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      return null;
  }

}
