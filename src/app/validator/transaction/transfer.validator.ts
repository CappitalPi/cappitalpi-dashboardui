import { Injectable } from '@angular/core';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';
import { TransferDto } from '../../dto/transaction/transfer.dto';
import { TransferRequestDto } from '../../dto/transaction/transfer-request.dto';

@Injectable({
  providedIn: 'root'
})
export class TransferValidator implements ObjectValidator<TransferRequestDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(transfer: TransferRequestDto): string {
      if (!transfer) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      }
      if (!transfer.creditCashAccount?.uid || !transfer.debitCashAccount?.uid) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.ACCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (!transfer.creditChartOfAccount?.uid || !transfer.debitChartOfAccount?.uid) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.CHART_OF_ACCOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      if (transfer.amount == null || transfer.amount <= 0) {
        const fieldName = this.translateService.instant('TRANSACTION.FIELD.AMOUNT');
        return this.translateService.instant('APP.VALIDATION.FIELD_IS_REQUIRED', {'fieldname':fieldName});
      }
      return null;
  }

}
