import { Injectable } from '@angular/core';
import { UserDto } from '../../dto/user/user.dto';
import { ObjectValidator } from '../object-validator';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class CreateUserValidator implements ObjectValidator<UserDto> {

  constructor(private translateService: TranslateService) {
  }
  
  public validate(user: UserDto): string {
      if (!user) {
          return this.translateService.instant('USER.APP.VALIDATION.NULL_OBJECT');
      } else {
        if (user.password != user.confirmPassword) {
          return this.translateService.instant('USER.REGISTER.VALIDATION.INVALID_PASSWORD_CONFIRMATION');
        }
      }
      return null;
  }

}
