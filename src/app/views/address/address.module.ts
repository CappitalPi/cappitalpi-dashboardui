import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { AddressListComponent } from './list/address-list.component';
import { AddressRegisterComponent } from './register/address-register.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [
    AddressListComponent,
    AddressRegisterComponent
  ],
  exports: [
    AddressListComponent,
    AddressRegisterComponent
  ]
})
export class AddressModule { }
