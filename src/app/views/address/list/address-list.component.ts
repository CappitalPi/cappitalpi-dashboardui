import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { AddressViewModelHelper } from '../view-model/address-view-model.helper';
import { AddressRegisterComponent } from '../register/address-register.component';
import { AddressViewModel } from '../view-model/address-view-model';

@Component({
  selector: 'address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})
export class AddressListComponent implements OnInit {

  @Input()
  public addressViewModelList : AddressViewModel[];
  @Output()
  paramsEmiter = new EventEmitter<{addressViewModelList: AddressViewModel[]}>();

  constructor(private modalService: BsModalService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }
  
  public openRegisterScreen(address: AddressViewModel) {
    const initialState = { existingAddress: address };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(AddressRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          AddressViewModelHelper.addToModelList(registerModalRef.content.addressViewModel, this.addressViewModelList);
          this.refresh();
        }
      });
  }

  public openRemoveDialog(model: AddressViewModel) {
    const initialState = { 
      title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code': model.code}), 
      message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info': model.addressLine}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.addressViewModelList.splice(model.index, 1);
          AddressViewModelHelper.formatAddresses(this.addressViewModelList);
          this.refresh();
        }
      });
  }

  public refresh(): void {
    this.paramsEmiter.emit(
      {
        addressViewModelList: this.addressViewModelList
      }
    );
  }

  public async close() {
    this.modalService.hide();
  }

  public hasAddress(): boolean {
    return this.addressViewModelList?.length > 0;
  }

}
