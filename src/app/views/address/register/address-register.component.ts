import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AddressTypeDto } from '../../../dto/address/address-type.dto';
import { AddressDto } from '../../../dto/address/address.dto';
import { CityDto } from '../../../dto/address/city.dto';
import { CountryDto } from '../../../dto/address/country.dto';
import { StateDto } from '../../../dto/address/state.dto';
import { AddressViewModelHelper } from '../view-model/address-view-model.helper';
import { AddressService } from '../../../service/address/address.service';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { AddressViewModel } from '../view-model/address-view-model';

@Component({
  selector: 'app-address-register',
  templateUrl: './address-register.component.html',
  styleUrls: ['./address-register.component.scss']
})
export class AddressRegisterComponent implements OnInit {

  @ViewChild('addressForm')
  private form : NgForm;
  @Input()
  public existingAddress: AddressViewModel;
  public addressViewModel: AddressViewModel;
  public address: AddressDto;

  public addressTypes: AddressTypeDto[];
  public cities: CityDto[];
  public states: StateDto[];
  public countries: CountryDto[];
  public selectedCity: CityDto;
  public selectedState: StateDto;
  public selectedCountry: CountryDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private addressService: AddressService) {
    this.address = new AddressDto();
    this.addressViewModel = new AddressViewModel();
    this.addressViewModel.address = new AddressDto();
  }
  
  ngOnInit(): void {
    this.addressTypes = [];
    this.cities = [];
    this.selectedCountry = new CountryDto();
    this.selectedState = new StateDto();
    this.selectedCity = new CityDto();
    this.load();
    this.loadList();
  }
  
  private load(): void {
    if (this.existingAddress) {
      this.addressViewModel = this.existingAddress;
      this.selectedCity = this.addressViewModel.address.city;
      this.selectedState = this.addressViewModel.address.state;
      this.selectedCountry = this.addressViewModel.address.country;
    }
    this.address = this.addressViewModel.address;
  }
  
  private loadList(): void {
    this.findAddressTypes();
    this.findCountries();    
    this.findStates(this.selectedCountry.code);
    this.findCities(this.selectedState.code);
  }
  
  public isNew(): boolean {
    return !this.addressViewModel?.code;
  }

  public findAddressTypes() {
    this.addressService.findAllTypes().subscribe(
      (list: AddressTypeDto[]) => {
        this.addressTypes = list;
        list.forEach(element => {
          if (this.address && this.address.type && element.code == this.address.type.code) {
            this.address.type = element;
          }
        });
      }, error => {
        this.alertMessage = error;
      }
    )
  }

  public findCities(stateCode: number) {
    this.cities = [];
    if (stateCode) {
      this.addressService.findCitiesByState(stateCode).subscribe(
        (data: CityDto[]) => {
          this.cities = data;
        }, error => {
          this.alertMessage = error;
        }
      )
    }
  }

  public findStates(countryCode: number) {
    this.states = [];
    if (countryCode) {
      this.addressService.findStatesByCountry(countryCode).subscribe(
        (data: StateDto[]) => {
          this.states = data;
        }, error => {
          this.alertMessage = error;
        }
      )
    }
  }

  public findCountries() {
    this.countries = [];
    this.addressService.findAllCountries().subscribe(
      (data: CountryDto[]) => {
        this.countries = data;
      }, error => {
        this.alertMessage = error;
      }
    )
  }

  public onChangeCity($event) {
    this.selectedCity = new CityDto();
    this.selectedCity.name = $event.target.value;
    const selectedItem = AddressViewModelHelper.getItemByName(this.cities, $event.target.value);
    if (selectedItem) {
      this.selectedCity = selectedItem;
    }
  }

  public onChangeCountry($event) {
    this.selectedCountry = new CountryDto();
    this.selectedCountry.name = $event.target.value;
    const selectedItem = AddressViewModelHelper.getItemByName(this.countries, $event.target.value);
    if (selectedItem) {
      this.selectedCountry = selectedItem;
      this.findStates(selectedItem.code);
    }
  }

  public onChangeState($event) {
    this.selectedState = new StateDto();
    this.selectedState.name = $event.target.value;
    const selectedItem = AddressViewModelHelper.getItemByName(this.states, $event.target.value);
    if (selectedItem) {
      this.selectedState = selectedItem;
      this.findCities(selectedItem.code);
    }
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public add(): void {
    this.submitted = false;
    this.disableSaveButton = true;
    this.address.city = this.selectedCity;
    this.address.state = this.selectedState;
    this.address.country = this.selectedCountry;
    this.addressViewModel.address = this.address;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.submitted = true;
    this.close();
  }
    
  public close(): void {
    this.modalRef.hide();
  }
}
  