import { AddressDto } from "../../../dto/address/address.dto";
import { AddressViewModel } from "./address-view-model";
import { AppHelper } from "../../../helper/app.helper";

const SEPARATOR = "#";

export class AddressViewModelHelper {


  public static getItemByName(list: any[], targetName: string) {
    if (list) {
      return list.find(x => x?.name.toUpperCase() === targetName.toUpperCase());
    }
    return null;
  }

  public static getAddresses(models: AddressViewModel[]) {
    const addresses: AddressDto[] = [];
    models?.forEach(model => {
      addresses.push(model.address);
    });
    return addresses;
  }

  public static findItemByValue(addressModelList: AddressViewModel[], itemValue: string) {
    if (addressModelList && itemValue?.length > 0) {
      const itemSplitted : string[] = itemValue.split(SEPARATOR);
      if (itemSplitted?.length > 0) {
        const code = itemSplitted[0];
        return addressModelList.find(x => x?.code === code);
      }
    }
    return null;
  }

  public static loadModelList(addressList: AddressDto[]): AddressViewModel[] {
    const addressModelViewList: AddressViewModel[] = [];
    addressList?.forEach(address => {
      const model: AddressViewModel = new AddressViewModel();
      model.address = address;
      addressModelViewList.push(model);
    });
    this.formatAddresses(addressModelViewList);
    return addressModelViewList;
  }

  public static findItemByCode(list: AddressViewModel[], code: number) {
    if (list && code) {
      const itemByCode = list.find(x => x?.address?.code === code);
      if (itemByCode) {
        return itemByCode;
      }
    }
    return null;
  }

  public static addToModelList(addressViewModel: AddressViewModel, addressList: AddressViewModel[]): void {
    if (addressList && addressViewModel) {
      if (addressViewModel.index >= 0) {
        addressList[addressViewModel.index] = addressViewModel;
      } else {
        addressList.push(addressViewModel);
      }
      this.formatAddresses(addressList);
    }
  }

  public static formatAddresses(addresses: AddressViewModel[]): void {
    if (addresses != null) {
      let index: number = 0;
      for (let addressViewModel of addresses) {
        addressViewModel.index = index++;
        this.formatAddress(addressViewModel);
      }
    }
  }

  public static formatAddress(model: AddressViewModel): void {
    if (model == null || model.address == null) {
      return null;
    }
    let addressLine = model.address.addressLine1;
    if (model.address.addressLine2) {
      addressLine += `, ${model.address.addressLine2}`;
    }
    if(model.address.city) {
      addressLine += `, ${model.address.city.name}`;
    }
    if(model.address.state) {
      if (model.address.state.stateCode) {
        addressLine += `, ${model.address.state.stateCode}`;
      } else {
        addressLine += `, ${model.address.state.name}`;
      }
    }
    if(model.address.country) {
      addressLine += `, ${model.address.country.name}`;
    }
    if (model.address.postalCode) {
      addressLine += `, ${model.address.postalCode}`;
    }
    model.addressLine = addressLine;
    model.code = AppHelper.generateUniqueCode(model.index, model.code);
    model.itemLabel = AppHelper.generateItemLabel(model.code, model.addressLine);
  }

}