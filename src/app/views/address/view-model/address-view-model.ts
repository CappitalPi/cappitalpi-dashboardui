import { AddressDto } from "../../../dto/address/address.dto";

/**
 * ViewModel is a Model for a View. It’s like a DTO, however specialized for a presentation, which can be the result of an API call or the page in a Web Application.
 * It generally accepts a DTO as input and adds information and methods specifically targeted to the View in question.
 * In the case of a page in a Web Application, it may contain logic to show or hide a field, formatting functions and other things directed
 * to the final presentation of the information, which in turn helps avoid Spaghetti Code in Views.
 */
export class AddressViewModel {
    code: string;
    index: number;
    address: AddressDto;
    addressLine: string;
    itemLabel: string;
}

