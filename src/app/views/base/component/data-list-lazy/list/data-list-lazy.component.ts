import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { StringUtils } from '../../../../../utils/string-utils';
import { DataListLazyDtoModel } from '../view-model/data-list-lazy-dto-model';
import { DataListLazyViewModel } from '../view-model/data-list-lazy-view-model';

@Component({
  selector: 'data-list-lazy',
  templateUrl: './data-list-lazy.component.html',
  styleUrls: ['./data-list-lazy.component.scss']
})
export class DataListLazyComponent implements OnInit {

  @Input()
  public dataList : DataListLazyViewModel[];
  @Input()
  public showCodeColumn : boolean;
  @Input()
  public isDisabled : boolean;
  @Input()
  public disableClick : boolean;
  @Output()
  public paramsEmitter = new EventEmitter<{dataList: DataListLazyViewModel[]}>();
  @Output()
  public selectedItemEmitter = new EventEmitter<{selectedItem: DataListLazyViewModel}>();
  @Output()
  public searchTermEmitter = new EventEmitter<{searchTerm: string}>();

  private selectedItem: DataListLazyViewModel;
  public searchField : string;
  public submitted: boolean;

  constructor() {
    this.selectedItem = new DataListLazyViewModel();
    this.searchField = StringUtils.EMPTY;
    this.submitted = false;
  }

  ngOnInit(): void {
    this.disableClick = this.isDisabled;
  }

  @Input() set setDataList(dataListDto : DataListLazyDtoModel) {
    this.dataList = [];
    if (dataListDto?.list?.length > 0) {
      this.dataList = dataListDto.list;
      this.selectItemByUid(dataListDto);
    }
  }

  private selectItemByUid(dataListDto : DataListLazyDtoModel): void {
    if (dataListDto.selectedUid) {
      let selectedIndex : number = null;
      dataListDto.list.forEach(item => {
        if (item?.uid === dataListDto.selectedUid) {
          selectedIndex = item.index;
        }
      });
      this.selectItem(selectedIndex);
    }
  }

  public selectItem(index: number): void {
    if (index >= 0) {
      this.selectedItem = this.dataList[index],
      this.selectedItemEmitter.emit(
        {
          selectedItem: this.selectedItem
        }
     );
    }
  }

  public search(): void {
    this.selectedItem = new DataListLazyViewModel();
    this.searchTermEmitter.emit( { searchTerm: this.searchField } );
    this.submitted = true;
  }

  public clear(): void {
    this.searchField = StringUtils.EMPTY;
    this.submitted = false;
  }

  public disableButtons() {
    return this.searchField?.length <= 0;
  }
  
  public hasData(): boolean {
    return this.dataList?.length > 0;
  }

  public isSelected(item: DataListLazyViewModel): boolean {
    return !item.unselectable && this.selectedItem?.index == item.index;
  }

  public showCode() {
    return this.showCodeColumn;
  }

}
