import { DataListLazyViewModel } from "./data-list-lazy-view-model";

export class DataListLazyDtoModel {
    list: DataListLazyViewModel[];
    selectedUid: string;
}

