import { DataListLazyViewModel } from "./data-list-lazy-view-model";

export interface DataListLazyViewModelHelper {

    load(list: any[]): DataListLazyViewModel[];

}