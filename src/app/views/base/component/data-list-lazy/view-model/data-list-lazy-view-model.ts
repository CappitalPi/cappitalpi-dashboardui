export class DataListLazyViewModel {
    index: number;
    object: any;
    uid: string;
    code: number;
    description: string;
    unselectable: boolean;
}

