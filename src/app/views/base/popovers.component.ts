import {Component, SecurityContext} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  templateUrl: 'popovers.component.html'
})
export class PopoversComponent {

  constructor(sanitizer: DomSanitizer) {
    this.html = sanitizer.sanitize(SecurityContext.HTML, this.html);
  }

  title: string = 'Welcome word';
  content: string = 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.';
  html: string = `<span class="btn btn-warning">Never trust not sanitized <code>HTML</code>!!!</span>`;
  // html: string = `<span class="btn btn-warning" style="white-space: pre-line">Lorem ipsum dolor sit amet.\r\nNulla sit amet felis vitae justo bibendum vehicula.\r\nPraesent eu scelerisque nibh, quis fermentum nibh.\r\n- Nulla mi eros, rutrum rutrum aliquet a, aliquet in risus.\r\n- Nulla sit amet magna ipsum.<code>HTML</code>!!!</span>`;

}
