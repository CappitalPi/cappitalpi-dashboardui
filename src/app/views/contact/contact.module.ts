import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ContactListComponent } from './list/contact-list.component';
import { ContactRegisterComponent } from './register/contact-register.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [
    ContactListComponent,
    ContactRegisterComponent,
  ],
  exports: [
    ContactListComponent,
    ContactRegisterComponent
  ]
})
export class ContactModule { }
