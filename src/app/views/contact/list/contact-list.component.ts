import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { ContactViewModelHelper } from '../view-model/contact-view-model.helper';
import { ContactRegisterComponent } from '../register/contact-register.component';
import { ContactViewModel } from '../view-model/contact-view-model';
import { PersonType } from '../../../dto/person/person-type.enum';

@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  @Input()
  public contactViewModelList : ContactViewModel[];
  @Input()
  public personType : string;
  @Output()
  paramsEmiter = new EventEmitter<{contactViewModelList: ContactViewModel[]}>();

  constructor(private modalService: BsModalService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }
  
  public openRegisterScreen(contact: ContactViewModel) {
    const initialState = { existingContact: contact, personType: this.personType };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(ContactRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          ContactViewModelHelper.addToModelList(registerModalRef.content.contactViewModel, this.contactViewModelList);
          this.refresh();
        }
      });
  }

  public openRemoveDialog(model: ContactViewModel) {
    const initialState = { 
      title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code': model.code}), 
      message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info': model.contactDescription}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.contactViewModelList.splice(model.index, 1);
          ContactViewModelHelper.formatContacts(this.contactViewModelList);
          this.refresh();
        }
      });
  }

  public refresh(): void {
    this.paramsEmiter.emit(
      {
        contactViewModelList: this.contactViewModelList
      }
    );
  }

  public async close() {
    this.modalService.hide();
  }

  public hasContacts(): boolean {
    return this.contactViewModelList?.length > 0;
  }

  public isLegalPerson(): boolean {
    return this.personType === PersonType.LEGAL_PERSON;
  }

}
