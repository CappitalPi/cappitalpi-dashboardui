import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ContactDto } from '../../../dto/contact/contact.dto';
import { PersonType } from '../../../dto/person/person-type.enum';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { ContactViewModel } from '../view-model/contact-view-model';

@Component({
  selector: 'app-contact-register',
  templateUrl: './contact-register.component.html',
  styleUrls: ['./contact-register.component.scss']
})
export class ContactRegisterComponent implements OnInit {

  @ViewChild('contactForm') form : NgForm;
  @Input() public existingContact: ContactViewModel;
  @Input() public personType: string;
  public contactViewModel: ContactViewModel;
  public contact: ContactDto;

  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef) {
    this.contact = new ContactDto();
    this.contactViewModel = new ContactViewModel();
    this.contactViewModel.contact = new ContactDto();
  }
  
  ngOnInit(): void {
    this.load();
  }
  
  private load(): void {
    if (this.existingContact) {
      this.contactViewModel = this.existingContact;
    }
    this.contact = this.contactViewModel.contact;
  }
  
  public isNew(): boolean {//TODO: Verificar título das páginas. Quando um novo registro é adicionado (não persistido), o código ainda não existe.
    return !this.contactViewModel?.code;
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public add(): void {
    this.submitted = false;
    this.disableSaveButton = true;
    this.contactViewModel.contact = this.contact;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.submitted = true;
    this.close();
  }

  public isLegalPerson(): boolean {
    return this.personType === PersonType.LEGAL_PERSON;
  }
    
  public close(): void {
    this.modalRef.hide();
  }
}
  