import { ContactDto } from "../../../dto/contact/contact.dto";
import { StringUtils } from "../../../utils/string-utils";
import { ContactViewModel } from "./contact-view-model";
import { AppHelper } from "../../../helper/app.helper";

const SEPARATOR = "#";
export class ContactViewModelHelper {


  public static getContacts(models: ContactViewModel[]) {
    const contacts: ContactDto[] = [];
    models?.forEach(model => {
      contacts.push(model.contact);
    });
    return contacts;
  }

  public static loadModelList(contactList: ContactDto[]): ContactViewModel[] {
    const contactModelViewList: ContactViewModel[] = [];
    contactList?.forEach(contact => {
      const model: ContactViewModel = new ContactViewModel();
      model.contact = contact;
      contactModelViewList.push(model);
    });
    this.formatContacts(contactModelViewList);
    return contactModelViewList;
  }

  public static addToModelList(contactViewModel: ContactViewModel, contactList: ContactViewModel[]): void {
    if (contactList && contactViewModel) {
      if (contactViewModel.index >= 0) {
        contactList[contactViewModel.index] = contactViewModel;
      } else {
        contactList.push(contactViewModel);
      }
      this.formatContacts(contactList);
    }
  }

  public static formatContacts(contacts: ContactViewModel[]): void {
    if (contacts != null) {
      let index: number = 0;
      for (let contactViewModel of contacts) {
        contactViewModel.index = index++;
        this.formatContact(contactViewModel);
      }
    }
  }

  public static formatContact(model: ContactViewModel): void {
    if (model == null || model.contact == null) {
      return null;
    }
    let contactDescription = model.contact.name;;
    if (model.contact.role?.length > 0) {
      contactDescription += ` (${model.contact.role})`;
    }
    model.code = AppHelper.generateUniqueCode(model.index, model.code);
    model.contactDescription = contactDescription;
    model.itemLabel = AppHelper.generateItemLabel(model.code, model.contact.name);
  }

  public static findItemValue(contactModelList: ContactViewModel[], itemValue: string, role: string) {
    if (contactModelList && itemValue?.length > 0) {
      const itemSplitted : string[] = itemValue.split(SEPARATOR);
      if (itemSplitted?.length > 0) {
        const code = itemSplitted[0];
        if (role?.length > 0) {
          return contactModelList.find(
            x => x?.code === StringUtils.normalizeSpace(code) && 
            StringUtils.normalizeSpace(x?.contact?.role).toUpperCase() === StringUtils.normalizeSpace(role).toUpperCase()
          );
        }
        return contactModelList.find(x => x?.code === StringUtils.normalizeSpace(code));
      }
    }
    return null;
  }

  public static findByNameAndRole(list: ContactViewModel[], name: string, role: string) {
    if (list && name && role) {
      const itemByName = list.find(x => 
        StringUtils.normalizeSpace(x?.contact.name.toUpperCase()) === StringUtils.normalizeSpace(name.toUpperCase()) &&
        StringUtils.normalizeSpace(x.contact.role.toUpperCase()) === StringUtils.normalizeSpace(role.toUpperCase()));
      if (itemByName) {
        return itemByName;
      }
    }
    return null;
  }

  public static findItemByCode(list: ContactViewModel[], code: number) {
    if (list && code) {
      const itemByCode = list.find(x => x?.contact?.code === code);
      if (itemByCode) {
        return itemByCode;
      }
    }
    return null;
  }

}