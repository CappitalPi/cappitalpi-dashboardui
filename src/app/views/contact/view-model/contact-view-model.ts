import { ContactDto } from "../../../dto/contact/contact.dto";

export class ContactViewModel {
    code: string;
    index: number;
    contact: ContactDto;
    contactDescription: string;
    itemLabel: string;
}