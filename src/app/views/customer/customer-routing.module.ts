import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CustomerSearchComponent } from './customer-search/customer-search.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerSearchComponent,
    data: {
      title: 'APP.MENU.PERSON.CUSTOMERS'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule {

  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
