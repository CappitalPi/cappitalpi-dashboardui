import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormValidationUtils } from '../../../utils/form-validation.utils';

@Component({
  selector: 'app-customer-search-bar',
  templateUrl: './customer-search-bar.component.html',
  styleUrls: ['./customer-search.component.scss']
})
export class CustomerSearchBarComponent implements OnInit {

  public codeFilter: number;
  public customerFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, customerFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.customerFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        customerFilter: this.customerFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
