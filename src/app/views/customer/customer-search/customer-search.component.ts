import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../dto/page-result.dto';
import { PersonSearchDto } from '../../../dto/person/person-search.dto';
import { PersonDto } from '../../../dto/person/person.dto';
import { CustomerService } from '../../../service/customer/customer.service';
import { CustomerRegisterComponent } from '../customer-register/customer-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-customer-search',
  templateUrl: './customer-search.component.html',
  styleUrls: ['./customer-search.component.scss']
})
export class CustomerSearchComponent implements OnInit {

  public isEditButtonDisabled = false;
  public alertMessage : string;
  public warningMessage : string;
  public itemsPerPage: number = 20;
  public customerPageResult : PageResultDto<PersonSearchDto>;
  public codeFilter: number;
  public customernameFilter: string;

  constructor(private modalService: BsModalService, 
              private customerService: CustomerService,
              private translateService: TranslateService) {
    this.customerPageResult = new PageResultDto<PersonSearchDto>();
  }

  ngOnInit(): void {
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.customernameFilter = $event.customernameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.customerService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.customernameFilter).subscribe(
      (data: PageResultDto<PersonSearchDto>) => {
        this.alertMessage = null;
        this.customerPageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.customerPageResult && this.customerPageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(customer: PersonDto) {
    const initialState = { existingCustomer: customer };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(CustomerRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public edit(customer: PersonSearchDto) {
    try {
      this.isEditButtonDisabled = true;
      if (customer) {
        this.customerService.findByCode(customer.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data);
            this.isEditButtonDisabled = false;
            },
          error => {
            this.alertMessage = error; 
            this.isEditButtonDisabled = false;
          }
        );
      }
    } catch {
      this.isEditButtonDisabled = false;
    }
  }

  public openRemoveDialog(customer: PersonSearchDto) {
    if (customer) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_TITLE', {'code':customer.code}), 
        message: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_MESSAGE', {'info':customer.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.disable(customer);
          }
        });
    }
  }

  public disableOrEnable(customer: PersonSearchDto): void {
    if (customer.disabled) {
      this.enable(customer)
    } else {
      this.openRemoveDialog(customer);
    }
  }

  private disable(customer: PersonSearchDto) {
    this.customerService.disable(customer.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  private enable(customer: PersonSearchDto) {
    this.customerService.enable(customer.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

}
