import { Component, OnInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { TransactionReportService } from '../../service/transaction/transaction-report.service';
import { TransactionTotalByDateDto } from '../../dto/transaction/transaction-total-by-date.dto';
import { DateUtils } from '../../utils/date.utils';
import { AccountPayRecService } from '../../service/account-pay-rec/account-pay-rec.service';
import { AccountPayRecCountDto } from '../../dto/account-pay-rec/account-pay-rec-count.dto';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  radioModel: string = 'Month';

  // lineChart1
  public lineChart1Data: Array<any> = [
    {
      data: [65, 59, 84, 84, 51, 55, 40],
      label: 'Series A'
    }
  ];
  public lineChart1Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 40 - 5,
          max: 84 + 5,
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart1Colours: Array<any> = [
    {
      backgroundColor: getStyle('--primary'),
      borderColor: 'rgba(255,255,255,.55)'
    }
  ];
  public lineChart1Legend = false;
  public lineChart1Type = 'line';

  // lineChart2
  public lineChart2Data: Array<any> = [
    {
      data: [80, 18, 9, 17, 34, 22, 11],
      label: 'Series A'
    }
  ];
  public lineChart2Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart2Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 1 - 5,
          max: 34 + 5,
        }
      }],
    },
    elements: {
      line: {
        tension: 0.00001,
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart2Colours: Array<any> = [
    { // grey
      backgroundColor: getStyle('--info'),
      borderColor: 'rgba(255,255,255,.55)'
    }
  ];
  public lineChart2Legend = false;
  public lineChart2Type = 'line';


  // lineChart3
  public lineChart3Data: Array<any> = [
    {
      data: [78, 81, 80, 45, 34, 12, 40],
      label: 'Series A'
    }
  ];
  public lineChart3Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChart3Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false
      }],
      yAxes: [{
        display: false
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart3Colours: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,.2)',
      borderColor: 'rgba(255,255,255,.55)',
    }
  ];
  public lineChart3Legend = false;
  public lineChart3Type = 'line';


  // barChart1
  public barChart1Data: Array<any> = [
    {
      data: [78, 81, 80, 45, 34, 12, 40, 78, 81, 80, 45, 34, 12, 40, 12, 40],
      label: 'Series A',
      barPercentage: 0.6,
    }
  ];
  public barChart1Labels: Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'];
  public barChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
      }],
      yAxes: [{
        display: false
      }]
    },
    legend: {
      display: false
    }
  };
  public barChart1Colours: Array<any> = [
    {
      backgroundColor: 'rgba(255,255,255,.3)',
      borderWidth: 0
    }
  ];
  public barChart1Legend = false;
  public barChart1Type = 'bar';

  // mainChart
  public title: string;
  public subTitle: string;
  public mainChartElements = 27;
  public mainChartCreditData: Array<number> = [];
  public mainChartDebitData: Array<number> = [];
  public mainChartBalanceData: Array<number> = [];
  public mainChartLabels: Array<any> = [];
  public mainChartData: Array<any>;
  public mainChartOptions: any;
  public mainChartColours: Array<any>;
  public mainChartLegend = false;
  public mainChartType = 'line';
  public alertMessage : string;
  public accountPayRecCount: AccountPayRecCountDto;
  private transactions: TransactionTotalByDateDto[];

  constructor(private transactionService: TransactionReportService, private accountPayRecService: AccountPayRecService, private translateService: TranslateService) {
    this.configureMainChartOptions();
    this.mainChartData = [];
    this.mainChartColours = [];
    this.mainChartLabels = [];
    this.mainChartCreditData = [];
    this.mainChartDebitData = [];
    this.mainChartBalanceData = [];

    this.title = this.translateService.instant('DASHBOARD.TITLE');
    const now = new Date();
    const initialDate = DateUtils.addDays(new Date(), -15);
    const initialDay = initialDate.getDate();
    const initialMonth = initialDate.toLocaleString(this.translateService.getDefaultLang(), { month: 'long' });
    const initialYear = initialDate.getFullYear();
    const finalDay = now.getDate();
    const finalMonth = now.toLocaleString(this.translateService.getDefaultLang(), { month: 'long' });
    const finalYear = now.getFullYear();
    this.subTitle = this.translateService.instant('DASHBOARD.SUB_TITLE', 
        {
          'initialDay':initialDay,
          'initialMonth':initialMonth,
          'initialYear':initialYear,
          'finalDay':finalDay,
          'finalMonth':finalMonth,
          'finalYear':finalYear
        }
    );
  }
  
  
  ngOnInit(): void {
    if (this.mainChartCreditData?.length <= 0 || this.mainChartDebitData?.length <= 0 || this.mainChartBalanceData?.length <= 0) {
      this.findTotalByDate();
    }
    if (this.accountPayRecCount == null) {
      this.findAccountPayRecCountOverdue();
    }
  }

  public refresh(): void {
    this.findTotalByDate();
    this.findAccountPayRecCountOverdue();
  }

  public findAccountPayRecCountOverdue(): void {
    this.accountPayRecCount = new AccountPayRecCountDto();
    this.accountPayRecService.findAccountPayRecCountOverdue(new Date()).subscribe(
      (data: AccountPayRecCountDto) => {
        this.accountPayRecCount.accountReceivableOverdue = data.accountReceivableOverdue;
        this.accountPayRecCount.accountPayableOverdue = data.accountPayableOverdue;
        this.accountPayRecCount.accountReceivableNext7Days = data.accountReceivableNext7Days;
        this.accountPayRecCount.accountPayableNext7Days = data.accountPayableNext7Days;
        this.alertMessage = null;
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  public findTotalByDate(): void {
    this.transactionService.findTotalByDate(new Date()).subscribe(
      (data: TransactionTotalByDateDto[]) => {
        this.alertMessage = null;
        this.transactions  = data;
        this.prepareChart();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private prepareChart(): void {
    this.mainChartLabels = [];
    this.mainChartCreditData = [];
    this.mainChartDebitData = [];
    this.mainChartBalanceData = [];
    if (this.transactions?.length > 0) {
      this.transactions.forEach(element => {
        this.mainChartLabels.push(DateUtils.formatToDayMonth(element.date));
        this.mainChartCreditData.push(element.totalCredit);
        this.mainChartDebitData.push(element.totalDebit);
        this.mainChartBalanceData.push(element.balance);  
      });
    }
    this.prepareData();
    this.configureMainChartColors();
  }

  private prepareData() {
    this.mainChartData = [
      {
        data: this.mainChartCreditData,
        label: 'Receitas'
      },
      {
        data: this.mainChartDebitData,
        label: 'Despesas'
      },
      {
        data: this.mainChartBalanceData,
        label: 'Saldo'
      }
    ];  
  }

  private configureMainChartColors(): void {
    this.mainChartColours = [
      {
        backgroundColor: hexToRgba(getStyle('--info'), 10),
        borderColor: getStyle('--info'),
        pointHoverBackgroundColor: '#fff'
      },
      {
        backgroundColor: 'transparent',
        borderColor: getStyle('--danger'),
        pointHoverBackgroundColor: '#fff'
      },
      {
        backgroundColor: 'transparent',
        borderColor: getStyle('--gray'),
        pointHoverBackgroundColor: '#fff',
        borderWidth: 1,
        borderDash: [8, 5]
      }
    ];
  }

  private configureMainChartOptions(): void {
    this.mainChartOptions = {
      tooltips: {
        enabled: false,
        custom: CustomTooltips,
        intersect: true,
        mode: 'index',
        position: 'nearest',
        callbacks: {
          labelColor: function(tooltipItem, chart) {
            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
          }
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false,
          },
          // ticks: {
          //   callback: function(value: any) {
          //     return value.charAt(0);
          //   }
          // }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            // stepSize: Math.ceil(250 / 5),
            // max: 250
          }
        }]
      },
      elements: {
        line: {
          borderWidth: 2
        },
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        }
      },
      legend: {
        display: false
      }
    };
  }
  
}
