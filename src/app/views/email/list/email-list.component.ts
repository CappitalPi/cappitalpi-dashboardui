import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { EmailViewModelHelper } from '../view-model/email-view-model.helper';
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { EmailRegisterComponent } from '../register/email-register.component';
import { EmailViewModel } from '../view-model/email-view-model';

@Component({
  selector: 'email-list',
  templateUrl: './email-list.component.html',
  styleUrls: ['./email-list.component.scss']
})
export class EmailListComponent implements OnInit {

  @Input()
  public emailViewModelList : EmailViewModel[];
  @Input()
  public personType : string;
  @Input()
  public contactViewModelList: ContactViewModel[];
  @Output()
  paramsEmiter = new EventEmitter<{emailViewModelList: EmailViewModel[]}>();

  constructor(private modalService: BsModalService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }
  
  public openRegisterScreen(email: EmailViewModel) {
    const initialState = { existingEmail: email, contactViewModelList: this.contactViewModelList, personType: this.personType };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(EmailRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          EmailViewModelHelper.addToModelList(registerModalRef.content.emailViewModel, this.emailViewModelList);
          this.refresh();
        }
      });
  }

  public openRemoveDialog(model: EmailViewModel) {
    const initialState = { 
      title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code': model.code}), 
      message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info': model.emailAddress}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.emailViewModelList.splice(model.index, 1);
          EmailViewModelHelper.formatEmails(this.emailViewModelList);
          this.refresh();
        }
      });
  }

  public refresh(): void {
    this.paramsEmiter.emit(
      {
        emailViewModelList: this.emailViewModelList
      }
    );
  }

  public isEmptyList() {
    return this.emailViewModelList?.length <= 0;
  }

  public async close() {
    this.modalService.hide();
  }

}
