import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ContactDto } from '../../../dto/contact/contact.dto';
import { EmailDto } from '../../../dto/email/email.dto';
import { ContactViewModelHelper } from '../../contact/view-model/contact-view-model.helper';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { EmailViewModel } from '../view-model/email-view-model';
import { PersonType } from '../../../dto/person/person-type.enum';

@Component({
  selector: 'app-email-register',
  templateUrl: './email-register.component.html',
  styleUrls: ['./email-register.component.scss']
})
export class EmailRegisterComponent implements OnInit {

  @ViewChild('emailForm')
  private form : NgForm;
  @Input()
  public existingEmail: EmailViewModel;
  @Input()
  public contactViewModelList: ContactViewModel[];
  @Input()
  public personType: string;
  public emailViewModel: EmailViewModel;

  public selectedContactItem: string;
  public selectedContactName: string;
  public selectedContactRole: string;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public isContactPrepared: boolean = false;

  constructor(public modalRef: BsModalRef) {
    this.emailViewModel = new EmailViewModel();
    this.emailViewModel.email = new EmailDto();
  }
  
  ngOnInit(): void {
    this.load();
  }
  
  private load(): void {
    if (this.existingEmail) {
      this.emailViewModel = this.existingEmail;
      if (this.existingEmail.contactModel) {
        this.selectContact(this.existingEmail.contactModel.itemLabel);
      }
    }
    if (!this.contactViewModelList) {
      this.contactViewModelList = [];
    }
  }
  
  public onChangeContact($event) {
    this.selectContact($event.target.value);
  }

  private selectContact(itemValue: string): void {
    this.selectedContactItem = null;
    const selectedItem : ContactViewModel= this.getContact(itemValue);
    if (selectedItem && selectedItem.contact) {
      this.selectedContactItem = itemValue;
      this.selectedContactName = selectedItem.contact.name;
      this.selectedContactRole = selectedItem.contact.role;
    } else {
      if (!this.isContactPrepared) {
        this.clearSelectedContact();
      }
    }
  }

  private getContact(itemValue: string): ContactViewModel {
    if (this.isContactPrepared) {
      return ContactViewModelHelper.findItemValue(this.contactViewModelList, itemValue, this.selectedContactRole);
    }
    return ContactViewModelHelper.findItemValue(this.contactViewModelList, itemValue, null);
  }

  public addContact() {
    //TODO: Add question.
    this.clearSelectedContact();
    this.isContactPrepared = this.isContactPrepared ? false : true;
  }

  private clearSelectedContact() {
    this.emailViewModel.contactModel = null;
    this.selectedContactName = null;
    this.selectedContactRole = null;
    this.selectedContactItem = null;
  }
  
  public isNew(): boolean {
    return !this.emailViewModel?.code;
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public isLegalPerson(): boolean {
    return this.personType === PersonType.LEGAL_PERSON;
  }

  private prepare(): void {
    this.emailViewModel.contactModel = null;
    const selectedItem : ContactViewModel= this.getContact(this.selectedContactItem);
    if (selectedItem) {
      this.emailViewModel.contactModel = selectedItem;
    } else {
      if (this.selectedContactName?.length > 0) {
        this.emailViewModel.contactModel = new ContactViewModel();
        this.emailViewModel.contactModel.contact = new ContactDto();
        this.emailViewModel.contactModel.contact.name = this.selectedContactName;
        this.emailViewModel.contactModel.contact.role = this.selectedContactRole;
      }
    }
  }

  //TODO: Role é obrigatório?
  public save(): void {
    this.submitted = false;
    this.disableSaveButton = true;
    this.prepare();
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.submitted = true;
    this.close();
  }
    
  public close(): void {
    this.modalRef.hide();
  }
}
  