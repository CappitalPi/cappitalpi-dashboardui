import { EmailDto } from "../../../dto/email/email.dto";
import { ContactViewModel } from "../../contact/view-model/contact-view-model";
import { EmailViewModel } from "./email-view-model";
import { AppHelper } from "../../../helper/app.helper";
import { ContactViewModelHelper } from "../../contact/view-model/contact-view-model.helper";

export class EmailViewModelHelper {


  public static getEmails(models: EmailViewModel[]) {
    const emails: EmailDto[] = [];
    models?.forEach(model => {
      if (model.contactModel?.contact) {
        model.email.contact = model.contactModel.contact;
      } else {
        model.email.contact = null;
      }
      emails.push(model.email);
    });
    return emails;
  }

  public static refreshList(emailViewModel: EmailViewModel[], contactViewModelList: ContactViewModel[]): void {
    emailViewModel?.forEach(email => {
      if (email.contactModel) {
        const contactModel: ContactViewModel = ContactViewModelHelper.findItemValue(contactViewModelList, email.contactModel.itemLabel, null);
        email.contactModel = contactModel;
      }
    });
    this.formatEmails(emailViewModel);
  }

  public static loadModelList(emailList: EmailDto[], contactViewModelList: ContactViewModel[]): EmailViewModel[] {
    const emailModelViewList: EmailViewModel[] = [];
    emailList?.forEach(email => {
      const model: EmailViewModel = new EmailViewModel();
      model.email = email;
      if (email.contact) {
        const contactModel: ContactViewModel = ContactViewModelHelper.findItemByCode(contactViewModelList, email.contact.code);
        model.contactModel = contactModel;
      }
      emailModelViewList.push(model);
    });
    this.formatEmails(emailModelViewList);
    return emailModelViewList;
  }

  public static addToModelList(emailViewModel: EmailViewModel, emailList: EmailViewModel[]): void {
    if (emailList && emailViewModel) {
      if (emailViewModel.index >= 0) {
        emailList[emailViewModel.index] = emailViewModel;
      } else {
        emailList.push(emailViewModel);
      }
      this.formatEmails(emailList);
    }
  }

  public static formatEmails(emails: EmailViewModel[]): void {
    if (emails != null) {
      let index: number = 0;
      for (let emailViewModel of emails) {
        emailViewModel.index = index++;
        this.formatEmail(emailViewModel);
      }
    }
  }

  public static formatEmail(model: EmailViewModel): void {
    if (model == null || model.email == null) {
      return null;
    }
    model.emailAddress = model.email.emailAddress;
    if (model.contactModel?.contact) {
      model.emailAddress += ` - ${model.contactModel.contact.name} (${model.contactModel.contact.role})`;
    }
    model.code = AppHelper.generateUniqueCode(model.index, model.code);
  }

}