import { EmailDto } from "../../../dto/email/email.dto";
import { ContactViewModel } from "../../contact/view-model/contact-view-model";

export class EmailViewModel {
    code: string;
    index: number;
    email: EmailDto;
    contactModel: ContactViewModel;
    emailAddress: string;
}

