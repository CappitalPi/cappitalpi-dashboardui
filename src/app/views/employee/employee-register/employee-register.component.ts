import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LegalPersonType } from '../../../dto/person/legal-person-type.enum';
import { LegalPersonDto } from '../../../dto/person/legal-person.dto';
import { MaritalStatus } from '../../../dto/person/marital-status.enum';
import { PersonType } from '../../../dto/person/person-type.enum';
import { PersonDto } from '../../../dto/person/person.dto';
import { PhysicalPersonDto } from '../../../dto/person/physical-person.dto';
import { Sex } from '../../../dto/person/sex.enum';
import { AddressViewModelHelper } from '../../address/view-model/address-view-model.helper';
import { ContactViewModelHelper } from '../../contact/view-model/contact-view-model.helper';
import { EmailViewModelHelper } from '../../email/view-model/email-view-model.helper';
import { InscriptionViewModelHelper } from '../../inscription/view-model/inscription-view-model.helper';
import { PersonHelper } from '../../../helper/person/person.helper';
import { PhoneViewModelHelper } from '../../phone/view-model/phone-view-model.helper';
import { EmployeePersistenceContext } from '../../../strategy/persistence/employee/employee-persistence-context.strategy';
import { PersistenceMode } from '../../../strategy/persistence/persistence-mode.enum';
import { EnumUtils } from '../../../utils/enum-utils';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { AddressViewModel } from '../../address/view-model/address-view-model';
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { EmailViewModel } from '../../email/view-model/email-view-model';
import { InscriptionViewModel } from '../../inscription/view-model/inscription-view-model';
import { PhoneViewModel } from '../../phone/view-model/phone-view-model';

@Component({
  selector: 'app-employee-register',
  templateUrl: './employee-register.component.html',
  styleUrls: ['./employee-register.component.scss']
})
export class EmployeeRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingEmployee: PersonDto;

  public employee: PersonDto;
  public legalPerson: LegalPersonDto;
  public physicalPerson: PhysicalPersonDto;
  public legalPersonTypes: LegalPersonType[];
  public sexs: Sex[];
  public maritalStatuses: MaritalStatus[];
  public addressViewModelList: AddressViewModel[];
  public phoneViewModelList: PhoneViewModel[]; 
  public emailViewModelList: EmailViewModel[];
  public inscriptionViewModelList: InscriptionViewModel[];
  public contactViewModelList: ContactViewModel[];

  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private employeePersistenceContext: EmployeePersistenceContext) {
    this.employee = new PersonDto();
    this.employee.type = PersonType.PHYSICAL_PERSON;
    this.legalPerson = new LegalPersonDto();
    this.physicalPerson = new PhysicalPersonDto();
    this.addressViewModelList = [];
    this.phoneViewModelList = [];
    this.emailViewModelList = [];
    this.inscriptionViewModelList = [];
    this.contactViewModelList = [];
  }
  
  ngOnInit(): void {
    if (this.existingEmployee) {
      this.employee = this.existingEmployee;
      if (PersonType.LEGAL_PERSON == this.employee.type) {
        this.legalPerson = this.employee.detail as LegalPersonDto;
      }
      if (PersonType.PHYSICAL_PERSON == this.employee.type) {
        this.physicalPerson = this.employee.detail as PhysicalPersonDto;
      }
      this.contactViewModelList = ContactViewModelHelper.loadModelList(this.employee.contacts);
      this.addressViewModelList = AddressViewModelHelper.loadModelList(this.employee.addresses);
      this.inscriptionViewModelList = InscriptionViewModelHelper.loadModelList(this.employee.inscriptions);
      this.emailViewModelList = EmailViewModelHelper.loadModelList(this.employee.emails, this.contactViewModelList);
      this.phoneViewModelList = PhoneViewModelHelper.loadModelList(this.employee.phones, this.contactViewModelList, this.addressViewModelList);
    }
    this.findLegalPersonTypes();
    this.findSexs();
    this.findMaritalStatus();
  }

  public findLegalPersonTypes(): void {
    if (!this.legalPersonTypes) {
      this.legalPersonTypes = EnumUtils.getList(LegalPersonType, false);
    }
  }

  public findSexs(): void {
    if (!this.sexs) {
      this.sexs = EnumUtils.getList(Sex, false);
    }
  }

  public findMaritalStatus(): void {
    if (!this.maritalStatuses) {
      this.maritalStatuses = EnumUtils.getList(MaritalStatus, false);
    }
  }
  
  ngAfterViewInit(): void {
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public isNew(): boolean {
    return this.employee && !this.employee.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public refreshAddress($event): void {
    this.addressViewModelList = $event.addressViewModelList;
  }

  public refreshPhones($event): void {
    this.phoneViewModelList = $event.phoneViewModelList;
    if (this.phoneViewModelList?.length > 0) {
      this.phoneViewModelList.forEach(element => {
        if (element) {
          this.updateContactViewModelList(element.contactModel);
        }
      });
    }
  }

  public refreshEmails($event): void {
    this.emailViewModelList = $event.emailViewModelList;
    if (this.emailViewModelList?.length > 0) {
      this.emailViewModelList.forEach(element => {
        if (element) {
          this.updateContactViewModelList(element.contactModel);
        }
      });
    }
  }

  public refreshInscriptions($event): void {
    this.inscriptionViewModelList = $event.inscriptionViewModelList;
  }

  public refreshContacts($event): void {
    this.contactViewModelList = $event.contactViewModelList;
    EmailViewModelHelper.refreshList(this.emailViewModelList, this.contactViewModelList);
    PhoneViewModelHelper.refreshList(this.phoneViewModelList, this.contactViewModelList, this.addressViewModelList);
  }

  private updateContactViewModelList(model: ContactViewModel): void {
    if (model) {
      const newContact = ContactViewModelHelper.findItemValue(this.contactViewModelList, model.code, null);
      if (!newContact) {
        if (!this.contactViewModelList) {
          this.contactViewModelList = [];
        }
        ContactViewModelHelper.addToModelList(model, this.contactViewModelList);
      }
    }
  }

  //TODO: Erro ao atualizar o tipo de pessoa (Pessoa fisica para pessoa juridica)
  // Rever campo tipo empresa: Uma empresa com vários endereços terá o mesmo tipo? 
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    this.employee.emails = EmailViewModelHelper.getEmails(this.emailViewModelList);
    this.employee.phones = PhoneViewModelHelper.getPhones(this.phoneViewModelList);
    this.employee.contacts = ContactViewModelHelper.getContacts(this.contactViewModelList);
    this.employee.addresses = AddressViewModelHelper.getAddresses(this.addressViewModelList);
    this.employee.inscriptions = InscriptionViewModelHelper.getInscriptions(this.inscriptionViewModelList);
    this.employee.type = PersonType.PHYSICAL_PERSON;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    const mode = this.employee.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.employee = PersonHelper.personByType(this.employee, this.legalPerson, this.physicalPerson);
    this.employeePersistenceContext.context(mode).persist(this.employee).subscribe(
      data => {
        console.debug(`Cliente criado com sucesso: ${data}`);
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
        console.error(`Erro na criação de cliente: ${error}`)
      }
      );
      this.submitted = true;
  }
    
  public close(): void {
    this.modalRef.content.submitted = this.submitted;
    this.modalRef.hide();
  }
}
  
  