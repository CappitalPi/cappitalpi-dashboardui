import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EmployeeSearchComponent } from './employee-search/employee-search.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeSearchComponent,
    data: {
      title: 'APP.MENU.PERSON.EMPLOYEES'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {
  
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
