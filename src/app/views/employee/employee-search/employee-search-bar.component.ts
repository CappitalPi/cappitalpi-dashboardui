import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormValidationUtils } from '../../../utils/form-validation.utils';

@Component({
  selector: 'app-employee-search-bar',
  templateUrl: './employee-search-bar.component.html',
  styleUrls: ['./employee-search.component.scss']
})
export class EmployeeSearchBarComponent implements OnInit {

  public codeFilter: number;
  public employeeFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, employeeFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.employeeFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        employeeFilter: this.employeeFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
