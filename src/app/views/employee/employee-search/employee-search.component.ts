import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../dto/page-result.dto';
import { PersonSearchDto } from '../../../dto/person/person-search.dto';
import { PersonDto } from '../../../dto/person/person.dto';
import { EmployeeService } from '../../../service/employee/employee.service';
import { EmployeeRegisterComponent } from '../employee-register/employee-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.scss']
})
export class EmployeeSearchComponent implements OnInit {

  public itemsPerPage: number = 20;
  public isEditButtonDisabled = false;
  public alertMessage : string;
  public warningMessage : string;
  public employeePageResult : PageResultDto<PersonSearchDto>;
  public codeFilter: number;
  public employeeNameFilter: string;

  constructor(private modalService: BsModalService, 
              private employeeService: EmployeeService,
              private translateService: TranslateService) {
    this.employeePageResult = new PageResultDto<PersonSearchDto>();
  }

  ngOnInit(): void {
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.employeeNameFilter = $event.employeenameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.employeeService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.employeeNameFilter).subscribe(
      (data: PageResultDto<PersonSearchDto>) => {
        this.alertMessage = null;
        this.employeePageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.employeePageResult && this.employeePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(employee: PersonDto) {
    const initialState = { existingEmployee: employee };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(EmployeeRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public edit(employee: PersonSearchDto) {
    try {
      this.isEditButtonDisabled = true;
      if (employee) {
        this.employeeService.findByCode(employee.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data);
            this.isEditButtonDisabled = false;
            },
          error => {
            this.alertMessage = error; 
            this.isEditButtonDisabled = false;
          }
        );
      }
    } catch {
      this.isEditButtonDisabled = false;
    }
  }

  public openRemoveDialog(employee: PersonSearchDto) {
    if (employee) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_TITLE', {'code':employee.code}), 
        message: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_MESSAGE', {'info':employee.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.disable(employee);
          }
        });
    }
  }

  public disableOrEnable(employee: PersonSearchDto): void {
    if (employee.disabled) {
      this.enable(employee)
    } else {
      this.openRemoveDialog(employee);
    }
  }

  private disable(employee: PersonSearchDto) {
    this.employeeService.disable(employee.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  private enable(employee: PersonSearchDto) {
    this.employeeService.enable(employee.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }  

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

}
