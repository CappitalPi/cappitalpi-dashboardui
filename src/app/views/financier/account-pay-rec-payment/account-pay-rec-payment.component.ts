import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CurrencyPipe } from '@angular/common';
import { AccountPayRecPaymentDto } from '../../../dto/account-pay-rec/account-pay-rec-payment.dto';
import { DataListDtoModel } from '../component/data-list/view-model/data-list-dto-model';
import { NumberUtils } from '../../../utils/number-utils';
import { FormValidationUtils } from '../../../utils/form-validation.utils';
import { DateUtils } from '../../../utils/date.utils';
import { AccountPaymentMode } from '../../../strategy/persistence/account-pay-rec-payment/account-payment-mode.enum';
import { AccountPaymentPersistenceContext } from '../../../strategy/persistence/account-pay-rec-payment/account-payment-persistence-context.strategy';
import { DataListViewModel } from '../component/data-list/view-model/data-list-view-model';
import { AccountService } from '../../../service/account/account.service';
import { AccountDataListViewHelper } from '../account/view-model/account-data-list-view.helper';
import { AccountSearchDto } from '../../../dto/account/account-search.dto';
import { PaymentMethod } from '../../../enum/payment-method.enum';
import { EnumUtils } from '../../../utils/enum-utils';
import { StringUtils } from '../../../utils/string-utils';
import { AccountPayRecDetailDto } from '../../../dto/account-pay-rec/account-pay-rec-detail.dto';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { AccountDto } from '../../../dto/account/account.dto';
import { AccountType } from '../../../enum/account-type.enum';

@Component({
  selector: 'app-account-pay-rec-payment',
  templateUrl: './account-pay-rec-payment.component.html',
  styleUrls: ['./account-pay-rec-payment.component.scss']
})
export class AccountPayRecPaymentComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public accountPayRec: AccountPayRecDetailDto;
  @Input() public paymentMode: AccountPaymentMode;

  public accountPayment: AccountPayRecPaymentDto;
  public success : boolean = false;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public amount : string;
  public discount : string;
  public interest : string;
  public balanceToPay : string;
  public paymentDate : string;
  public amountPaid : string;
  public amountToPay : string;
  public chartOfAccountDescription: string;
  public paymentMethods: PaymentMethod[];
  public selectedAccount: DataListViewModel;
  public accountDataList: DataListDtoModel;

  constructor(public modalRef: BsModalRef,
              private currencyPipe: CurrencyPipe,
              private modalService: BsModalService,
              private translateService: TranslateService,
              private accountService: AccountService,
              private accountDataListViewHelper: AccountDataListViewHelper,
              private accountPaymentPersistence: AccountPaymentPersistenceContext) {
    this.paymentDate = DateUtils.formatToShortDate(new Date());
    this.accountPayment = new AccountPayRecPaymentDto();
    this.accountPayment.amount = 0;
    this.accountPayment.discount = 0;
    this.accountPayment.interest = 0;
    this.selectedAccount = new DataListViewModel();
  }
  
  ngOnInit(): void {
    if (!this.accountPayRec) {
      this.alertMessage = this.translateService.instant('APP.WARNING.DATA_NOT_AVAILABLE');
      throw new Error(this.alertMessage);
    }
    if (!this.accountPayment) {
      this.accountPayment = new AccountPayRecPaymentDto();
    }
    this.accountPayment.accountPayRecUid = this.accountPayRec.uid;
    this.accountPayment.registerUid = this.accountPayRec.registerUid;
    if (this.accountPayRec?.chartOfAccount){
      this.chartOfAccountDescription = this.accountPayRec.chartOfAccount.accountNumber+". "+this.accountPayRec.chartOfAccount.name;
    }
    this.initAmounts();
    this.findAccounts();
  }

  private initAmounts() {
    this.amount = NumberUtils.currencyToStr(this.accountPayment.amount, this.currencyPipe);
    this.discount = NumberUtils.currencyToStr(this.accountPayment.discount, this.currencyPipe);
    this.interest = NumberUtils.currencyToStr(this.accountPayment.interest, this.currencyPipe);
    this.amountPaid = NumberUtils.currencyToStr(this.accountPayRec.amountPaid, this.currencyPipe);
    this.calcAmount();
  }

  public findAccounts(): void{
    this.accountService.findAll().subscribe(
      (data: AccountSearchDto[]) => {
        this.accountDataList = new DataListDtoModel();
        this.accountDataList.list = this.accountDataListViewHelper.load(data);
        this.accountDataList.selectedUid = this.selectedAccount.uid;
      },
      error => {
        console.error(error);
        this.alertMessage = this.translateService.instant('APP.ERROR.SOMETHING_WENT_WRONG');
      }
    );
  }

  public getCurrencyValue(amount: number): string {
    if (!amount) {
      amount = 0;
    }
    return NumberUtils.currencyToStr(amount, this.currencyPipe);
  }

  public getDateValue(date: Date): string {
    if (date) {
      return DateUtils.formatToShortDate(date);
    }
    return StringUtils.EMPTY;
  }

  public accountSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.selectedAccount = $event.selectedItems[0];
      this.findPaymentMethods();
    }
  }

  private findPaymentMethods(): void {
    this.paymentMethods = [];
    this.accountPayment.paymentMethod = null;
    if (this.selectedAccount?.object) {
      const account: AccountDto = this.selectedAccount.object;      
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.CREDIT_CARD)) {
        this.paymentMethods.push(PaymentMethod.CREDIT_CARD);
        this.accountPayment.paymentMethod = PaymentMethod.CREDIT_CARD;
      }
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.WALLET)) {
        this.paymentMethods.push(PaymentMethod.CASH);
        this.paymentMethods.push(PaymentMethod.CHECK);
      }
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.BANK) || account.type === EnumUtils.getNameFromValue(AccountType, AccountType.INVESTMENT)) {
        this.paymentMethods.push(PaymentMethod.BANK_SLIP);
        this.paymentMethods.push(PaymentMethod.BANK_TRANSFER);
        this.paymentMethods.push(PaymentMethod.CHECK);
        this.paymentMethods.push(PaymentMethod.DEBIT_CARD);
        this.paymentMethods.push(PaymentMethod.DEPOSIT);
        this.paymentMethods.push(PaymentMethod.PAYPAL);
      }
    }
  }
  
  public onChangeAmount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.amount = $event.target.value;
    this.calcAmount();
  }

  public onChangeDiscount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.discount = $event.target.value;
    this.calcAmount();
  }

  public onChangeInterest($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.interest = $event.target.value;
    this.calcAmount();
  }

  public isCheckNumberDisabled() {
    if (this.accountPayment?.paymentMethod == PaymentMethod.CHECK || this.accountPayment?.paymentMethod == PaymentMethod.DEPOSIT) {
      return false;
    }
    this.accountPayment.checkNumber = null;
    return true;
  }

  public calcAmount(): void {
    const amountToPay : number = NumberUtils.strToCurrency(this.amount) - NumberUtils.strToCurrency(this.discount) + NumberUtils.strToCurrency(this.interest);
    this.amountToPay = NumberUtils.currencyToStr(amountToPay, this.currencyPipe);

    const balanceToPay : number = this.accountPayRec.balance - NumberUtils.strToCurrency(this.amount) - NumberUtils.strToCurrency(this.amountPaid);
    this.balanceToPay = NumberUtils.currencyToStr(balanceToPay, this.currencyPipe);
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public confirmPayment() {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    const balanceToPayAmount: number = NumberUtils.strToCurrency(this.balanceToPay);
    var dialogMessage: string = balanceToPayAmount <= 0 ? 'ACCOUNT_PAY_REC_PAYMENT.DIALOG.MESSAGE_TOTAL_PAYMENT' : 'ACCOUNT_PAY_REC_PAYMENT.DIALOG.MESSAGE_PARTIAL_PAYMENT';
    const initialState = { 
      title: this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.DIALOG.MESSAGE_TOTAL_PAYMENT_SUBJECT', {'code' : this.accountPayRec.code}),
      message: this.translateService.instant(dialogMessage, {'amount' : this.amountToPay, 'amountToPay' : this.balanceToPay}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.persist();
        }
      });
  }

  public persist(): void {
    this.success = false;
    try {
      this.accountPayment.amount = NumberUtils.strToCurrency(this.amountToPay);
      this.accountPayment.discount = NumberUtils.strToCurrency(this.discount);
      this.accountPayment.interest = NumberUtils.strToCurrency(this.interest);
      this.accountPayment.paymentDate = DateUtils.strToDate(this.paymentDate);
      this.accountPayment.cashAccountUid = this.selectedAccount.object.uid;
      this.accountPaymentPersistence.context(this.paymentMode).pay(this.accountPayment).subscribe(
        data => {
          this.success = true;
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
        }
        );
        this.submitted = true;  
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }  
  }
  
  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }
}
  
  