import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { AccountPayRecPaymentComponent } from './account-pay-rec-payment.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataListModule } from '../component/data-list/data-list.module';
import { DirectivesModule } from '../../../directive/directives.module';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DataListModule,
    DirectivesModule,
    ConfirmationDialogModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    // PopoverModule.forRoot(),
    // AlertModule.forRoot(),
    // TooltipModule.forRoot()
  ],
  declarations: [
    AccountPayRecPaymentComponent
  ],
  exports: [
    AccountPayRecPaymentComponent
  ]
})
export class AccountPayRecPaymentModule { }
