import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountPayableRegisterComponent } from './account-payable-register.component';

describe('TransactionRegisterComponent', () => {
  let component: AccountPayableRegisterComponent;
  let fixture: ComponentFixture<AccountPayableRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountPayableRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPayableRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
