import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountPayableSearchComponent } from './account-payable-search/account-payable-search.component';

const routes: Routes = [
  {
    path: '',
    component: AccountPayableSearchComponent,
    data: {
      title: 'APP.MENU.FINANCIER.ACCOUNTS-PAYABLE'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPayableRoutingModule {
  
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
