import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { AccountService } from '../../../../service/account/account.service';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { FormValidationUtils } from '../../../../utils/form-validation.utils';
import { ChartOfAccountViewModelHelper } from '../../chart-of-account/view-model/chart-of-account-view-model.helper';
import { ChartOfAccountViewModel } from '../../chart-of-account/view-model/chart-of-account-view-model';
import { DateType } from '../../../../enum/date-type.enum';
import { DateUtils } from '../../../../utils/date.utils';
import { StringUtils } from '../../../../utils/string-utils';
import { AccountPayRecStatus } from '../../../../enum/account-pay-rec-status.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { TransactionType } from '../../../../enum/transaction-type.enum';

@Component({
  selector: 'app-account-payable-search-bar',
  templateUrl: './account-payable-search-bar.component.html',
  styleUrls: ['./account-payable-search.component.scss']
})
export class AccountPayableSearchBarComponent implements OnInit {

  public codeFilter: number;
  public startDateFilter: string;
  public endDateFilter: string;
  public dateTypeFilter: DateType;
  public personSearchFilter: string;
  public documentNumberFilter: string
  public statusFilter: string
  public selectedChartOfAccount: string;
  public chartOfAccounts: ChartOfAccountViewModel[];
  public statuses: AccountPayRecStatus[];
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<
  { 
    codeFilter: number;
    startDateFilter: string;
    endDateFilter: string;
    dateTypeFilter: DateType;
    personSearchFilter: string;
    chartOfAccountUidFilter: string;
    documentNumberFilter: string;
    statusFilter: string;
  }>();

  constructor(public accountService: AccountService, public chartOfAccountService: ChartOfAccountService) {
    this.dateTypeFilter = DateType.ISSUE;
    this.startDateFilter = DateUtils.formatToShortDate(DateUtils.addDays(new Date(), -90));
    this.endDateFilter = DateUtils.formatToShortDate(new Date());
    this.statusFilter = AccountPayRecStatus.OPEN;
  }

  ngOnInit(): void {
    this.chartOfAccounts = [];
    this.findChartOfAccounts();
    this.findStatuses();
    this.search();
  }

  private findStatuses(): void {
    if (!this.statuses) {
      this.statuses = EnumUtils.getList(AccountPayRecStatus, false);
    }
  }

  private findChartOfAccounts(): void {
    this.chartOfAccountService.findByType(TransactionType.DEBIT).subscribe(
      (list: ChartOfAccountDto[]) => {
        this.chartOfAccounts = ChartOfAccountViewModelHelper.loadModelList(list);
      }
    )
  }

  public clearChartOfAccount(): void {
    this.selectedChartOfAccount = StringUtils.EMPTY;;
  }

  public clearStatus(): void {
    this.statusFilter = StringUtils.EMPTY;;
  }

  public onChangeChartOfAccount($event): void {
    $event.target.value = $event?.target?.value?.trim();
    this.selectedChartOfAccount = $event.target.value;
  }

  public clear(): void {
    this.codeFilter = null;
    this.selectedChartOfAccount = StringUtils.EMPTY;
    this.personSearchFilter = StringUtils.EMPTY;
    this.documentNumberFilter = StringUtils.EMPTY;
    this.statusFilter = StringUtils.EMPTY;
    this.search();
  }

  public search(): void {
    const selectedChartOfAccount: ChartOfAccountViewModel = ChartOfAccountViewModelHelper.getItemByName(this.chartOfAccounts, this.selectedChartOfAccount);
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        startDateFilter: this.startDateFilter,
        endDateFilter: this.endDateFilter,
        dateTypeFilter: this.dateTypeFilter,
        personSearchFilter: this.personSearchFilter,
        chartOfAccountUidFilter: selectedChartOfAccount?.chartOfAccount?.uid,
        documentNumberFilter: this.documentNumberFilter,
        statusFilter: this.statusFilter
      }
    );
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

}
