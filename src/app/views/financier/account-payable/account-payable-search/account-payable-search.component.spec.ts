import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountPayableSearchComponent } from './account-payable-search.component';


describe('AccountPayableSearchComponent', () => {
  let component: AccountPayableSearchComponent;
  let fixture: ComponentFixture<AccountPayableSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountPayableSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPayableSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
