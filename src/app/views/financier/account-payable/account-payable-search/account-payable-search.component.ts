import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { AccountPayRecDetailDto } from '../../../../dto/account-pay-rec/account-pay-rec-detail.dto';
import { AccountPayRecRepairDto } from '../../../../dto/account-pay-rec/account-pay-rec-repair.dto';
import { AccountPayRecDto } from '../../../../dto/account-pay-rec/account-pay-rec.dto';
import { AccountPayRecSearchDto } from '../../../../dto/account-pay-rec/account-pay-rec-search.dto';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { AccountPayRecBalance } from '../../../../enum/account-pay-rec-balance.enum';
import { AccountPayRecStatus } from '../../../../enum/account-pay-rec-status.enum';
import { DateType } from '../../../../enum/date-type.enum';
import { AccountPayableService } from '../../../../service/account-payable/account-payable.service';
import { AccountPaymentMode } from '../../../../strategy/persistence/account-pay-rec-payment/account-payment-mode.enum';
import { DateUtils } from '../../../../utils/date.utils';
import { NumberUtils } from '../../../../utils/number-utils';
import { StringUtils } from '../../../../utils/string-utils';
import { AccountPayRecPaymentComponent } from '../../account-pay-rec-payment/account-pay-rec-payment.component';
import { AccountPayableRegisterComponent } from '../account-payable-register/account-payable-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-account-payable-search',
  templateUrl: './account-payable-search.component.html',
  styleUrls: ['./account-payable-search.component.scss']
})
export class AccountPayableSearchComponent implements OnInit {

  public isEditButtonDisabled : boolean = false;
  public showBalance : boolean = false;
  public alertMessage : string;
  public warningMessage : string;
  public successMessage : string;
  public totalPaid: number;
  public totalToPay: number;
  public total: number;
  public accountPayablePageResult : PageResultDto<AccountPayRecSearchDto>;
  public codeFilter: number;
  public startDateFilter: string;
  public endDateFilter: string;
  public dateTypeFilter: DateType;
  public personSearchFilter: string;
  public chartOfAccountUidFilter: string;
  public documentNumberFilter: string
  public statusFilter: string
  public itemsPerPage: number = 20;
  public selectedAccount: AccountPayRecSearchDto;
  public tooltipAmountDetail: string;
  public tooltipSupplierDetail: string;

  constructor(private modalService: BsModalService,
              private currencyPipe: CurrencyPipe,
              private accountPayableService: AccountPayableService,
              private translateService: TranslateService,
              private sanitizer: DomSanitizer) {
    this.accountPayablePageResult = new PageResultDto<AccountPayRecSearchDto>();
    this.accountPayablePageResult.totalElements = 0;
    this.totalPaid = NumberUtils.strToCurrency("0");
    this.totalToPay = NumberUtils.strToCurrency("0");
    this.total = NumberUtils.strToCurrency("0");
    this.selectedAccount = new AccountPayRecSearchDto();
  }

  ngOnInit(): void {
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.codeFilter = $event.codeFilter;
    this.startDateFilter = $event.startDateFilter;
    this.endDateFilter = $event.endDateFilter;
    this.dateTypeFilter = $event.dateTypeFilter;
    this.personSearchFilter = $event.personSearchFilter;
    this.chartOfAccountUidFilter = $event.chartOfAccountUidFilter;
    this.documentNumberFilter = $event.documentNumberFilter;
    this.statusFilter = $event.statusFilter;
    this.findList(FIRST_PAGE);
  }

  public findList(pageNumber: number, message: string = ""): void {
    this.accountPayableService.filter(pageNumber,
                                      this.itemsPerPage,
                                      this.codeFilter,
                                      DateUtils.strToDate(this.startDateFilter),
                                      DateUtils.strToDate(this.endDateFilter),
                                      this.dateTypeFilter,
                                      this.personSearchFilter,
                                      this.chartOfAccountUidFilter,
                                      this.documentNumberFilter,
                                      this.statusFilter).subscribe(
      (data: PageResultDto<AccountPayRecSearchDto>) => {
        this.alertMessage = null;
        this.accountPayablePageResult  = data;
        this.totalPaid = data?.balances?.[AccountPayRecBalance.TOTAL_PAID];
        this.totalToPay = data?.balances?.[AccountPayRecBalance.TOTAL_TO_PAY];
        this.total = data?.balances?.[AccountPayRecBalance.TOTAL];
        this.afterFilter(message);
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter(successMessage: string = "") {
    this.warningMessage = StringUtils.EMPTY;
    this.successMessage = successMessage;
    if (this.accountPayablePageResult && this.accountPayablePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
    if (this.warningMessage?.length <= 0 && this.hasPendingAccountPayable()) {
      this.warningMessage = this.translateService.instant("ACCOUNT_PAYABLE.MESSAGE.PENDING_ACCOUNT_PAYABLE");
    }
    const dueAccounts = this.getOverdueAccounts();
    if (dueAccounts > 0) {
      this.alertMessage = this.translateService.instant('ACCOUNT_PAYABLE.MESSAGE.DUE_ACCOUNT_PAYABLE', {'total':dueAccounts});
    }
  }

  public dblclickOnTable(accountPayable: AccountPayRecSearchDto): void {
    if (this.isOpen(accountPayable)) {
      this.edit(accountPayable);
    } else {
      this.view(accountPayable);
    }
  }

  public view(accountPayable: AccountPayRecSearchDto): void {
    this.findAndOpenRegisterScreen(accountPayable, true);
  }

  public edit(accountPayable: AccountPayRecSearchDto): void {
    try {
      this.isEditButtonDisabled = true;
      this.findAndOpenRegisterScreen(accountPayable, false);
    } finally {
      this.isEditButtonDisabled = false;
    }
  }

  public findAndOpenRegisterScreen(accountPayable: AccountPayRecSearchDto, isViewMode: boolean): void {
    try {
      if (accountPayable) {
        this.accountPayableService.findByCodeWithPayments(accountPayable.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data, accountPayable, isViewMode);
          },
          error => {
            this.alertMessage = this.translateService.instant("ACCOUNT_PAYABLE.MESSAGE.COULD_NOT_OPEN") + ` (${error})`; 
          }
        );
      }
    } catch {
      throw new Error(this.translateService.instant("ACCOUNT_PAYABLE.MESSAGE.COULD_NOT_OPEN"));
    }
  }

  public openRegisterScreen(accountPayable: AccountPayRecDto, accountPayableSearch: AccountPayRecSearchDto, isViewMode: boolean) {
    const initialState = { existingAccountPayable: accountPayable, accountPayableSearch: accountPayableSearch, isViewMode: isViewMode };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AccountPayableRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public payment() {
    try {
      if (!this.selectedAccount || !this.selectedAccount.uid) {
        this.warningMessage = this.translateService.instant("ACCOUNT_PAYMENT.MESSAGE.SELECT_VALID_ACCOUNT");
      } else {
        this.accountPayableService.findByUidDetailed(this.selectedAccount.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openPaymentScreen(data);
          },
          error => {
            this.alertMessage = error; 
          }
          );
      }
    } catch {
      //TODO:
    }
  }

  public repair(): void {
    if (!this.selectedAccount || !this.selectedAccount.uid) {
      this.warningMessage = this.translateService.instant("ACCOUNT_PAYMENT.MESSAGE.SELECT_VALID_ACCOUNT");
    } else {
      this.alertMessage = null;

      const repair: AccountPayRecRepairDto = new AccountPayRecRepairDto();
      repair.registerUid = this.selectedAccount.registerUid;
      repair.uid = this.selectedAccount.uid;
      this.accountPayableService.repair(repair).subscribe(
        data => {
          const message = this.translateService.instant('ACCOUNT_PAYABLE.MESSAGE.REPAIRED_SUCCESS', {'code':this.selectedAccount.code});
          this.findList(this.accountPayablePageResult.currentPage, message);
        },
        error => {
          this.alertMessage = error;
        }
      );
    }
  }


  public openPaymentScreen(accountPayable: AccountPayRecDetailDto) {
    const initialState = { accountPayRec: accountPayable, paymentMode: AccountPaymentMode.ACCOUNT_PAYABLE };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AccountPayRecPaymentComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.success) {
          const message = this.translateService.instant('ACCOUNT_PAYABLE.MESSAGE.PAYMENT_SUCCESS', {'code':accountPayable.code});
          this.findList(FIRST_PAGE, message);
        }
      });
  }

  public openRemoveDialog(accountPayable: AccountPayRecSearchDto) {
    if (accountPayable) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':accountPayable.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':accountPayable.person.name + " ($"+NumberUtils.currencyToStr(accountPayable.amount, this.currencyPipe)+")"}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(accountPayable);
          }
        });
    }
  }

  private remove(accountPayable: AccountPayRecSearchDto) {
    this.accountPayableService.remove(accountPayable.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public formatDescription(description: string): string{
    return StringUtils.formatLongText(description);
  }
  
  private hasPendingAccountPayable(): boolean {
    if (this.accountPayablePageResult?.totalElements > 0) {
      for (var item of this.accountPayablePageResult.content) {
        if (this.isPending(item)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public pageChanged(event: any): void {
    this.findList(event.page);
  }
  
  public showBalanceClick(): boolean {
    return this.showBalance = !this.showBalance;
  }
  
  public isSelected(account: AccountPayRecSearchDto): boolean {
    return this.selectedAccount?.code == account.code;
  }

  public isPending(accountPayable: AccountPayRecSearchDto): boolean {
    return accountPayable?.status == AccountPayRecStatus.PENDING;
  }

  public isOpen(accountPayable: AccountPayRecSearchDto): boolean {
    return accountPayable?.status === AccountPayRecStatus.OPEN;
  }

  public isPaid(accountPayable: AccountPayRecSearchDto): boolean {
    return accountPayable?.status === AccountPayRecStatus.DONE;
  }

  public isDue(accountPayable: AccountPayRecSearchDto): boolean {
    if (accountPayable?.dueDate) {
      return !this.isPaid(accountPayable) && DateUtils.isSameOrAfter(new Date(), accountPayable.dueDate);
    }
    return false;
  }

  private getOverdueAccounts(): number {
    var dueAccounts = 0;
    if (this.accountPayablePageResult?.content?.length > 0) {
      this.accountPayablePageResult.content.forEach(account => {
        if (this.isDue(account)) {
          dueAccounts++;
        }
      })
    }
    return dueAccounts;
  }

  public selectItem(account: AccountPayRecSearchDto): void {
    this.selectedAccount = new AccountPayRecSearchDto();
    if (account) {
      this.selectedAccount = account;
    }
  }

  public getTooltipSupplier(accountPayable: AccountPayRecSearchDto): string {
    const codeLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.CODE');
    const businessNameLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.BUSINESS_NAME');
    const phoneLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.PHONE');
    let html = codeLabel+ ": " +accountPayable.person.code.toString();
    if (accountPayable.person.businessName) {
      html += "<br/>";
      html += businessNameLabel+ ": " +accountPayable.person.businessName;
    }
    if (accountPayable.person?.phones) {
      html += "<br/>";
      html += phoneLabel+ ": ";
      accountPayable.person.phones.forEach(element => {
        html += element.areaCode+"-"+element.phoneNumber + "  ";  
      });
      html = html.trim();
    }
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipAmount(accountPayable: AccountPayRecSearchDto): string {
    const amountLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.AMOUNT');
    const discountLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.DISCOUNT');
    const interestLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.INTEREST');
    let html = amountLabel+ ": " +NumberUtils.currencyToStr(accountPayable.amount, this.currencyPipe);
    html += "<br/>";
    html += discountLabel+ ": " +NumberUtils.currencyToStr(accountPayable.discount, this.currencyPipe);
    html += "<br/>";
    html += interestLabel+ ": " +NumberUtils.currencyToStr(accountPayable.interest, this.currencyPipe);
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipDueDate(accountPayable: AccountPayRecSearchDto): string {
    var html: string = null;
    if (this.isPaid(accountPayable)) {
      html = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.PAYMENT_DATE', {'date': DateUtils.formatToShortDate(accountPayable.paymentDate) });
    } else {
      if (accountPayable?.dueDate) {
        let totalOfDays = 0;
        if (this.isDue(accountPayable)) {
          totalOfDays = DateUtils.calcDates(new Date(), accountPayable.dueDate);
          html = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.DAYS_OVERDUE', {'total':totalOfDays});
        } else {
          totalOfDays = DateUtils.calcDates(accountPayable.dueDate, new Date());
          html = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.DAYS_LEFT', {'total':totalOfDays});
        }
      }
    }
    if (html) {
      html = html.trim();
      return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
    }
    return null;
  }

}
