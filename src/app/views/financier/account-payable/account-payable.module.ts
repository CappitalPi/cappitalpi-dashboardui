import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';
import { AccountPayableRegisterComponent } from './account-payable-register/account-payable-register.component';
import { AccountPayableRoutingModule } from './account-payable-routing.module';
import { AccountPayableSearchBarComponent } from './account-payable-search/account-payable-search-bar.component';
import { AccountPayableSearchComponent } from './account-payable-search/account-payable-search.component';
import { DataListLazyComponent } from '../../base/component/data-list-lazy/list/data-list-lazy.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { DataListModule } from '../component/data-list/data-list.module';
import { AccountPayRecPaymentModule } from '../account-pay-rec-payment/account-pay-rec-payment.module';
import { DataListLazyModule } from '../../base/component/data-list-lazy/data-list-lazy.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    AccountPayableRoutingModule,
    ConfirmationDialogModule,
    DataListModule,
    AccountPayRecPaymentModule,
    DataListLazyModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    AccountPayableRegisterComponent,
    AccountPayableSearchComponent,
    AccountPayableSearchBarComponent
  ]
})
export class AccountPayableModule { }
