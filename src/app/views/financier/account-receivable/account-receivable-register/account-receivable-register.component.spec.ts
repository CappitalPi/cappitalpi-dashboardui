import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountReceivableRegisterComponent } from './account-receivable-register.component';

describe('TransactionRegisterComponent', () => {
  let component: AccountReceivableRegisterComponent;
  let fixture: ComponentFixture<AccountReceivableRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountReceivableRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountReceivableRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
