import { Component, Input, OnInit, SecurityContext, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { DataListViewModel } from '../../component/data-list/view-model/data-list-view-model';
import { DateUtils } from '../../../../utils/date.utils';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { CurrencyPipe } from '@angular/common';
import { NumberUtils } from '../../../../utils/number-utils';
import { AccountPayRecRequestDto } from '../../../../dto/account-pay-rec/account-pay-rec-request.dto';
import { DataListLazyDtoModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-dto-model';
import { PersonService } from '../../../../service/person/person.service';
import { PersonSearchDetailDto } from '../../../../dto/person/person-search-detail.dto';
import { DataListLazyViewModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-view-model';
import { PersonDataListViewHelper } from '../../../person/view-model/person-data-list-view.helper';
import { ChartOfAccountDataListViewHelper } from '../../chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { AccountReceivablePersistenceContext } from '../../../../strategy/persistence/account-receivable/account-receivable-persistence-context.strategy';
import { AccountPayRecSearchDto } from '../../../../dto/account-pay-rec/account-pay-rec-search.dto';
import { AccountPayRecPaymentDto } from '../../../../dto/account-pay-rec/account-pay-rec-payment.dto';
import { AccountPayRecDto } from '../../../../dto/account-pay-rec/account-pay-rec.dto';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { StringUtils } from '../../../../utils/string-utils';

@Component({
  selector: 'app-account-receivable-register',
  templateUrl: './account-receivable-register.component.html',
  styleUrls: ['./account-receivable-register.component.scss']
})
export class AccountReceivableRegisterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public isViewMode: boolean;
  @Input() public existingAccountReceivable: AccountPayRecDto;
  @Input() public accountReceivableSearch: AccountPayRecSearchDto;

  public accountReceivable: AccountPayRecRequestDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public issueDate : string;
  public dueDate : string;
  public amount : string;
  public discount : string;
  public interest : string;
  public total : string;
  public amountPaid : string;
  public amountToPay : string;
  public payments : AccountPayRecPaymentDto[];
  public selectedChartOfAccount: DataListViewModel;
  public selectedPerson: DataListLazyViewModel;
  public accountDataList: DataListDtoModel;
  public chartOfAccountDataList: DataListDtoModel;
  public personDataList: DataListLazyDtoModel;

  constructor(public modalRef: BsModalRef,
              private modalService: BsModalService,
              private currencyPipe: CurrencyPipe,
              private accountReceivablePersistenceContext: AccountReceivablePersistenceContext,
              private personService: PersonService,
              private chartOfAccountService: ChartOfAccountService,
              private personDataListLazyViewHelper: PersonDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper,
              private translateService: TranslateService,
              private sanitizer: DomSanitizer) {
    this.issueDate = DateUtils.formatToShortDate(new Date());
    this.accountReceivable = new AccountPayRecRequestDto();
    this.accountReceivable.amount = 0;
    this.accountReceivable.discount = 0;
    this.accountReceivable.interest = 0;
    this.accountReceivable.amountPaid = 0;
    this.accountReceivable.amountToPay = 0;
    this.accountReceivable.balance = 0;
    this.initAmounts(this.accountReceivable);
    this.selectedChartOfAccount = new DataListViewModel();
    this.selectedPerson = new DataListLazyViewModel();
  }
  
  ngOnInit(): void {
    if (this.existingAccountReceivable) {
      this.accountReceivable = new AccountPayRecRequestDto();
      this.accountReceivable.uid = this.existingAccountReceivable.uid;
      this.accountReceivable.code = this.existingAccountReceivable.code;
      this.accountReceivable.registerUid = this.existingAccountReceivable.registerUid;
      this.accountReceivable.appAccountUid = this.existingAccountReceivable.appAccountUid;
      this.accountReceivable.personUid = this.existingAccountReceivable.personUid;
      this.accountReceivable.chartOfAccountUid = this.existingAccountReceivable.chartOfAccountUid;
      this.accountReceivable.description = this.existingAccountReceivable.description;
      this.accountReceivable.documentNumber = this.existingAccountReceivable.documentNumber;
      this.accountReceivable.barCode = this.existingAccountReceivable.barCode;
      this.accountReceivable.issueDate = this.existingAccountReceivable.issueDate;
      this.accountReceivable.dueDate = this.existingAccountReceivable.dueDate;
      this.accountReceivable.amount = this.existingAccountReceivable.amount;
      this.accountReceivable.discount = this.existingAccountReceivable.discount;
      this.accountReceivable.interest = this.existingAccountReceivable.interest;
      this.accountReceivable.balance = this.existingAccountReceivable.balance;
      this.accountReceivable.amountPaid = this.existingAccountReceivable.amountPaid;
      this.accountReceivable.amountToPay = this.existingAccountReceivable.amountToPay;
      this.accountReceivable.updatedDate = this.existingAccountReceivable.updatedDate;
      this.selectedPerson.uid = this.existingAccountReceivable.personUid;
      this.selectedChartOfAccount.uid = this.existingAccountReceivable.chartOfAccountUid;
      this.payments = this.existingAccountReceivable.payments;
      this.issueDate = DateUtils.formatToShortDate(this.existingAccountReceivable.issueDate);
      this.dueDate = DateUtils.formatToShortDate(this.existingAccountReceivable.dueDate);
      this.initAmounts(this.existingAccountReceivable);
      const selectedPerson : PersonSearchDetailDto[] = [this.accountReceivableSearch.person]
      this.loadPerson(selectedPerson);
    }
    this.findChartOfAccounts();
  }

  private initAmounts(accountReceivable : AccountPayRecRequestDto) {
    this.amount = NumberUtils.currencyToStr(accountReceivable.amount, this.currencyPipe);
    this.discount = NumberUtils.currencyToStr(accountReceivable.discount, this.currencyPipe);
    this.interest = NumberUtils.currencyToStr(accountReceivable.interest, this.currencyPipe);
    this.amountPaid = NumberUtils.currencyToStr(accountReceivable.amountPaid, this.currencyPipe);
    this.calcAmount();
  }
  
  public findChartOfAccounts(): void{
    this.chartOfAccountService.findByType(TransactionType.CREDIT).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountDataList = new DataListDtoModel();
        this.chartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.chartOfAccountDataList.selectedUid = this.selectedChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  public chartOfAccountSelected($event): void {
    this.selectedChartOfAccount = new DataListViewModel();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.selectedChartOfAccount = $event.selectedItems[0];
      }
    }
  }

  public personSelected($event): void {
    this.selectedPerson = $event.selectedItem;
  }

  public filterPerson($event) {
    this.selectedPerson = new DataListLazyViewModel();
    this.personService.filter($event.searchTerm).subscribe(
      (data: PersonSearchDetailDto[]) => {
        this.loadPerson(data);
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  private loadPerson(data: PersonSearchDetailDto[]) {
    this.personDataList = new DataListLazyDtoModel();
    this.personDataList.list = this.personDataListLazyViewHelper.load(data);
    this.personDataList.selectedUid = this.selectedPerson.uid;
  }

  public formatNotes(notes: string): string{
    return StringUtils.formatLongText(notes);
  }

  /**
   * Método necessário pois existe um bug na diretiva ao incluir aos trilhões.
   * Por algum motivo, a aplicação está aceitando um dígito a mais.
   * TODO: Tentar corrigir a diretiva ou criar outra.
   * @param $event 
   */
  public onChangeAmount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.amount = $event.target.value;
    this.calcAmount();
  }

  public onChangeDiscount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.discount = $event.target.value;
    this.calcAmount();
  }

  public onChangeInterest($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.interest = $event.target.value;
    this.calcAmount();
  }

  public calcAmount(): void {
    const total : number = NumberUtils.strToCurrency(this.amount) - NumberUtils.strToCurrency(this.discount) + NumberUtils.strToCurrency(this.interest);
    this.total = NumberUtils.currencyToStr(total, this.currencyPipe);

    const amountToPay : number = NumberUtils.strToCurrency(this.total) - NumberUtils.strToCurrency(this.amountPaid);
    this.amountToPay = NumberUtils.currencyToStr(amountToPay, this.currencyPipe);
  }

  public isNew(): boolean {
    return this.accountReceivable && !this.accountReceivable.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public persist(): void {
    try {
      this.disableSaveButton = true;
      this.submitted = false;
      this.alertMessage = null;
      if (!this.form.valid) {
        FormValidationUtils.markFieldsAsTouched(this.form);
        this.disableSaveButton = false;
        return;
      }
      this.accountReceivable.personUid = this.selectedPerson.uid;
      this.accountReceivable.chartOfAccountUid = this.selectedChartOfAccount.uid;
      this.accountReceivable.amount = NumberUtils.strToCurrency(this.amount);
      this.accountReceivable.discount = NumberUtils.strToCurrency(this.discount);
      this.accountReceivable.interest = NumberUtils.strToCurrency(this.interest);
      this.accountReceivable.amountToPay = NumberUtils.strToCurrency(this.amountToPay);
      this.accountReceivable.issueDate = DateUtils.strToDate(this.issueDate);
      this.accountReceivable.dueDate = DateUtils.strToDate(this.dueDate);
      const mode = this.accountReceivable.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
      this.accountReceivablePersistenceContext.context(mode).persist(this.accountReceivable).subscribe(
        data => {
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
        }
      );
        this.submitted = true;
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }  
  }

  public hasPayments(): boolean {
    return this.payments?.length > 0;
  }
  
  public isUpdate(): boolean {
    return this.accountReceivable?.uid?.length > 0
  }

  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }

  public getTooltipAmount(payment: AccountPayRecPaymentDto): string {
    const discountLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.DISCOUNT');
    const interestLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.INTEREST');
    let html = discountLabel+ ": " +NumberUtils.currencyToStr(payment.discount, this.currencyPipe);
    html += "<br/>";
    html += interestLabel+ ": " +NumberUtils.currencyToStr(payment.interest, this.currencyPipe);
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipCode(payment: AccountPayRecPaymentDto): string {
    const documentNumberLabel: string = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.DOCUMENT_NUMBER');
    const checkNumberLabel: string = this.translateService.instant('ACCOUNT_PAY_REC_PAYMENT.FIELD.CHECK_NUMBER');
    let html = documentNumberLabel+ ": " + (payment.documentNumber != null ? payment.documentNumber : "");
    html += "<br/>";
    html += checkNumberLabel+ ": " + (payment.checkNumber != null ? payment.checkNumber : "");
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public showPaymentNotes(content): void {
    this.modalService.show(content);
  }
}
  
  