import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountReceivableSearchComponent } from './account-receivable-search/account-receivable-search.component';

const routes: Routes = [
  {
    path: '',
    component: AccountReceivableSearchComponent,
    data: {
      title: 'APP.MENU.FINANCIER.ACCOUNTS-RECEIVABLE'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountReceivableRoutingModule {

  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
