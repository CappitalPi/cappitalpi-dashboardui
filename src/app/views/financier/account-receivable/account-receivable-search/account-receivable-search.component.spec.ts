import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountReceivableSearchComponent } from './account-receivable-search.component';


describe('AccountReceivableSearchComponent', () => {
  let component: AccountReceivableSearchComponent;
  let fixture: ComponentFixture<AccountReceivableSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountReceivableSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountReceivableSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
