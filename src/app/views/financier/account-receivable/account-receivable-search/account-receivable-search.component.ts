import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { AccountPayRecDetailDto } from '../../../../dto/account-pay-rec/account-pay-rec-detail.dto';
import { AccountPayRecRepairDto } from '../../../../dto/account-pay-rec/account-pay-rec-repair.dto';
import { AccountPayRecDto } from '../../../../dto/account-pay-rec/account-pay-rec.dto';
import { AccountPayRecSearchDto } from '../../../../dto/account-pay-rec/account-pay-rec-search.dto';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { AccountPayRecBalance } from '../../../../enum/account-pay-rec-balance.enum';
import { AccountPayRecStatus } from '../../../../enum/account-pay-rec-status.enum';
import { DateType } from '../../../../enum/date-type.enum';
import { AccountReceivableService } from '../../../../service/account-receivable/account-receivable.service';
import { AccountPaymentMode } from '../../../../strategy/persistence/account-pay-rec-payment/account-payment-mode.enum';
import { DateUtils } from '../../../../utils/date.utils';
import { NumberUtils } from '../../../../utils/number-utils';
import { StringUtils } from '../../../../utils/string-utils';
import { AccountPayRecPaymentComponent } from '../../account-pay-rec-payment/account-pay-rec-payment.component';
import { AccountReceivableRegisterComponent } from '../account-receivable-register/account-receivable-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-account-receivable-search',
  templateUrl: './account-receivable-search.component.html',
  styleUrls: ['./account-receivable-search.component.scss']
})
export class AccountReceivableSearchComponent implements OnInit {

  public isEditButtonDisabled : boolean = false;
  public showBalance : boolean = false;
  public alertMessage : string;
  public warningMessage : string;
  public successMessage : string;
  public totalPaid: number;
  public totalToPay: number;
  public total: number;
  public accountReceivablePageResult : PageResultDto<AccountPayRecSearchDto>;
  public codeFilter: number;
  public startDateFilter: string;
  public endDateFilter: string;
  public dateTypeFilter: DateType;
  public personSearchFilter: string;
  public chartOfAccountUidFilter: string;
  public documentNumberFilter: string
  public statusFilter: string
  public itemsPerPage: number = 20;
  public selectedAccount: AccountPayRecSearchDto;
  public tooltipAmountDetail: string;
  public tooltipSupplierDetail: string;

  constructor(private modalService: BsModalService,
              private currencyPipe: CurrencyPipe,
              private accountReceivableService: AccountReceivableService,
              private translateService: TranslateService,
              private sanitizer: DomSanitizer) {
    this.accountReceivablePageResult = new PageResultDto<AccountPayRecSearchDto>();
    this.accountReceivablePageResult.totalElements = 0;
    this.totalPaid = NumberUtils.strToCurrency("0");
    this.totalToPay = NumberUtils.strToCurrency("0");
    this.total = NumberUtils.strToCurrency("0");
    this.selectedAccount = new AccountPayRecSearchDto();
  }

  ngOnInit(): void {
  }

  
  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.codeFilter = $event.codeFilter;
    this.startDateFilter = $event.startDateFilter;
    this.endDateFilter = $event.endDateFilter;
    this.dateTypeFilter = $event.dateTypeFilter;
    this.personSearchFilter = $event.personSearchFilter;
    this.chartOfAccountUidFilter = $event.chartOfAccountUidFilter;
    this.documentNumberFilter = $event.documentNumberFilter;
    this.statusFilter = $event.statusFilter;
    this.findList(FIRST_PAGE);
  }
  
  public findList(pageNumber: number, message: string = ""): void {
    this.accountReceivableService.filter(pageNumber,
      this.itemsPerPage,
      this.codeFilter,
      DateUtils.strToDate(this.startDateFilter),
      DateUtils.strToDate(this.endDateFilter),
      this.dateTypeFilter,
      this.personSearchFilter,
      this.chartOfAccountUidFilter,
      this.documentNumberFilter,
      this.statusFilter).subscribe(
        (data: PageResultDto<AccountPayRecSearchDto>) => {
          this.alertMessage = null;
          this.accountReceivablePageResult  = data;
          this.totalPaid = data?.balances?.[AccountPayRecBalance.TOTAL_PAID];
          this.totalToPay = data?.balances?.[AccountPayRecBalance.TOTAL_TO_PAY];
          this.total = data?.balances?.[AccountPayRecBalance.TOTAL];
          this.afterFilter(message);
        },
        error => {
          this.alertMessage = error;
        }
        );
      }
      
  private afterFilter(successMessage: string = "") {
    this.warningMessage = StringUtils.EMPTY;
    this.successMessage = successMessage;
    if (this.accountReceivablePageResult && this.accountReceivablePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
    if (this.warningMessage?.length <= 0 && this.hasPendingAccountReceivable()) {
      this.warningMessage = this.translateService.instant("ACCOUNT_RECEIVABLE.MESSAGE.PENDING_ACCOUNT_RECEIVABLE");
    }
    const dueAccounts = this.getOverdueAccounts();
    if (dueAccounts > 0) {
      this.alertMessage = this.translateService.instant('ACCOUNT_RECEIVABLE.MESSAGE.DUE_ACCOUNT_RECEIVABLE', {'total':dueAccounts});
    }
  }

  public dblclickOnTable(accountReceivable: AccountPayRecSearchDto): void {
    if (this.isOpen(accountReceivable)) {
      this.edit(accountReceivable);
    } else {
      this.view(accountReceivable);
    }
  }

  public view(accountReceivable: AccountPayRecSearchDto): void {
    this.findAndOpenRegisterScreen(accountReceivable, true);
  }

  public edit(accountReceivable: AccountPayRecSearchDto): void {
    try {
      this.isEditButtonDisabled = true;
      this.findAndOpenRegisterScreen(accountReceivable, false);
    } finally {
      this.isEditButtonDisabled = false;
    }
  }

  public findAndOpenRegisterScreen(accountReceivable: AccountPayRecSearchDto, isViewMode: boolean): void {
    try {
      if (accountReceivable) {
        this.accountReceivableService.findByCodeWithPayments(accountReceivable.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data, accountReceivable, isViewMode);
          },
          error => {
            this.alertMessage = this.translateService.instant("ACCOUNT_RECEIVABLE.MESSAGE.COULD_NOT_OPEN") + ` (${error})`; 
          }
        );
      }
    } catch {
      throw new Error(this.translateService.instant("ACCOUNT_RECEIVABLE.MESSAGE.COULD_NOT_OPEN"));
    }
  }

  public openRegisterScreen(accountReceivable: AccountPayRecDto, accountReceivableSearch: AccountPayRecSearchDto, isViewMode: boolean) {
    const initialState = { existingAccountReceivable: accountReceivable, accountReceivableSearch: accountReceivableSearch, isViewMode: isViewMode };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AccountReceivableRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public payment() {
    try {
      if (!this.selectedAccount || !this.selectedAccount.uid) {
        this.warningMessage = this.translateService.instant("ACCOUNT_PAYMENT.MESSAGE.SELECT_VALID_ACCOUNT");
      } else {
        this.accountReceivableService.findByUidDetailed(this.selectedAccount.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openPaymentScreen(data);
          },
          error => {
            this.alertMessage = error; 
          }
          );
      }
    } catch {
      //TODO:
    }
  }

  public repair(): void {
    if (!this.selectedAccount || !this.selectedAccount.uid) {
      this.warningMessage = this.translateService.instant("ACCOUNT_PAYMENT.MESSAGE.SELECT_VALID_ACCOUNT");
    } else {
      this.alertMessage = null;

      const repair: AccountPayRecRepairDto = new AccountPayRecRepairDto();
      repair.registerUid = this.selectedAccount.registerUid;
      repair.uid = this.selectedAccount.uid;
      this.accountReceivableService.repair(repair).subscribe(
        data => {
          const message = this.translateService.instant('ACCOUNT_RECEIVABLE.MESSAGE.REPAIRED_SUCCESS', {'code':this.selectedAccount.code});
          this.findList(this.accountReceivablePageResult.currentPage, message);
        },
        error => {
          this.alertMessage = error;
        }
      );
    }
  }


  public openPaymentScreen(accountReceivable: AccountPayRecDetailDto) {
    const initialState = { accountPayRec: accountReceivable, paymentMode: AccountPaymentMode.ACCOUNT_RECEIVABLE };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AccountPayRecPaymentComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          const message = this.translateService.instant('ACCOUNT_RECEIVABLE.MESSAGE.PAYMENT_SUCCESS', {'code':accountReceivable.code});
          this.findList(FIRST_PAGE, message);
        }
      });
  }

  public openRemoveDialog(accountReceivable: AccountPayRecSearchDto) {
    if (accountReceivable) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':accountReceivable.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':accountReceivable.person.name + " ($"+NumberUtils.currencyToStr(accountReceivable.amount, this.currencyPipe)+")"}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(accountReceivable);
          }
        });
    }
  }

  private remove(accountReceivable: AccountPayRecSearchDto) {
    this.accountReceivableService.remove(accountReceivable.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public formatDescription(description: string): string{
    return StringUtils.formatLongText(description);
  }
  
  private hasPendingAccountReceivable(): boolean {
    if (this.accountReceivablePageResult?.totalElements > 0) {
      for (var item of this.accountReceivablePageResult.content) {
        if (this.isPending(item)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public pageChanged(event: any): void {
    this.findList(event.page);
  }
  
  public showBalanceClick(): boolean {
    return this.showBalance = !this.showBalance;
  }
  
  public isSelected(account: AccountPayRecSearchDto): boolean {
    return this.selectedAccount?.code == account.code;
  }

  public isPending(accountReceivable: AccountPayRecSearchDto): boolean {
    return accountReceivable?.status == AccountPayRecStatus.PENDING;
  }

  public isOpen(accountReceivable: AccountPayRecSearchDto): boolean {
    return accountReceivable?.status === AccountPayRecStatus.OPEN;
  }

  public isPaid(accountReceivable: AccountPayRecSearchDto): boolean {
    return accountReceivable?.status === AccountPayRecStatus.DONE;
  }

  public isDue(accountReceivable: AccountPayRecSearchDto): boolean {
    if (accountReceivable?.dueDate) {
      return !this.isPaid(accountReceivable) && DateUtils.isSameOrAfter(new Date(), accountReceivable.dueDate);
    }
    return false;
  }

  private getOverdueAccounts(): number {
    var dueAccounts = 0;
    if (this.accountReceivablePageResult?.content?.length > 0) {
      this.accountReceivablePageResult.content.forEach(account => {
        if (this.isDue(account)) {
          dueAccounts++;
        }
      })
    }
    return dueAccounts;
  }

  public selectItem(account: AccountPayRecSearchDto): void {
    this.selectedAccount = new AccountPayRecSearchDto();
    if (account) {
      this.selectedAccount = account;
    }
  }

  public getTooltipSupplier(accountReceivable: AccountPayRecSearchDto): string {
    const codeLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.CODE');
    const businessNameLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.BUSINESS_NAME');
    const phoneLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.PHONE');
    let html = codeLabel+ ": " +accountReceivable.person.code.toString();
    if (accountReceivable.person.businessName) {
      html += "<br/>";
      html += businessNameLabel+ ": " +accountReceivable.person.businessName;
    }
    if (accountReceivable.person?.phones) {
      html += "<br/>";
      html += phoneLabel+ ": ";
      accountReceivable.person.phones.forEach(element => {
        html += element.areaCode+"-"+element.phoneNumber + "  ";  
      });
      html = html.trim();
    }
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipAmount(accountReceivable: AccountPayRecSearchDto): string {
    const amountLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.AMOUNT');
    const discountLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.DISCOUNT');
    const interestLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.INTEREST');
    let html = amountLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.amount, this.currencyPipe);
    html += "<br/>";
    html += discountLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.discount, this.currencyPipe);
    html += "<br/>";
    html += interestLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.interest, this.currencyPipe);
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipDueDate(accountReceivable: AccountPayRecSearchDto): string {
    var html: string = null;
    if (this.isPaid(accountReceivable)) {
      html = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.PAYMENT_DATE', {'date': DateUtils.formatToShortDate(accountReceivable.paymentDate) });
    } else {
      if (accountReceivable?.dueDate) {
        let totalOfDays = 0;
        if (this.isDue(accountReceivable)) {
          totalOfDays = DateUtils.calcDates(new Date(), accountReceivable.dueDate);
          html = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.DAYS_OVERDUE', {'total':totalOfDays});
        } else {
          totalOfDays = DateUtils.calcDates(accountReceivable.dueDate, new Date());
          html = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.DAYS_LEFT', {'total':totalOfDays});
        }
      }
    }
    if (html) {
      html = html.trim();
      return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
    }
    return null;
  }

}
