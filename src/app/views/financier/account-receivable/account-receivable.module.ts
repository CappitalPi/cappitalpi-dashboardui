import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';
import { AccountReceivableRegisterComponent } from './account-receivable-register/account-receivable-register.component';
import { AccountReceivableRoutingModule } from './account-receivable-routing.module';
import { AccountReceivableSearchBarComponent } from './account-receivable-search/account-receivable-search-bar.component';
import { AccountReceivableSearchComponent } from './account-receivable-search/account-receivable-search.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { DataListModule } from '../component/data-list/data-list.module';
import { AccountPayRecPaymentModule } from '../account-pay-rec-payment/account-pay-rec-payment.module';
import { DataListLazyModule } from '../../base/component/data-list-lazy/data-list-lazy.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    AccountReceivableRoutingModule,
    ConfirmationDialogModule,
    DataListModule,
    AccountPayRecPaymentModule,
    DataListLazyModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    AccountReceivableRegisterComponent,
    AccountReceivableSearchComponent,
    AccountReceivableSearchBarComponent
  ]
})
export class AccountReceivableModule { }
