import { CurrencyPipe } from '@angular/common';
import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { AccountDto } from '../../../../dto/account/account.dto';
import { AccountType } from '../../../../enum/account-type.enum';
import { AccountPersistenceContext } from '../../../../strategy/persistence/account/account-persistence-context.strategy';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { NumberUtils } from '../../../../utils/number-utils';

@Component({
  selector: 'app-account-register',
  templateUrl: './account-register.component.html',
  styleUrls: ['./account-register.component.scss']
})
export class AccountRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingAccount: AccountDto;

  public account: AccountDto;
  public startBalance : string;
  public currentBalance : string;
  public creditLimit : string;
  public submitted : boolean = false;
  public types: AccountType[];
  public alertMessage : string;
  public warningMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef,
              private accountPersistenceContext: AccountPersistenceContext,
              private currencyPipe: CurrencyPipe,
              private modalService: BsModalService,
              private translateService: TranslateService) {
    this.startBalance = NumberUtils.currencyToStr(0, currencyPipe);
    this.currentBalance = NumberUtils.currencyToStr(0, currencyPipe);
    this.creditLimit = NumberUtils.currencyToStr(0, currencyPipe);
    this.account = new AccountDto();
    this.account.startBalance = 0;
    this.account.balance = 0;
    this.account.creditLimit = 0;
  }
  
  ngOnInit(): void {
    if (this.existingAccount) {
      this.account = this.existingAccount;
      this.startBalance = NumberUtils.currencyToStr(this.existingAccount.startBalance, this.currencyPipe);
      this.currentBalance = NumberUtils.currencyToStr(this.existingAccount.balance, this.currencyPipe);
      this.creditLimit = NumberUtils.currencyToStr(this.existingAccount.creditLimit, this.currencyPipe);
    }
    this.findTypes();
  }

  public findTypes(): void{
    if (!this.types) {
      this.types = EnumUtils.getList(AccountType, false);
    }
  }
  
  ngAfterViewInit(): void {
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public showAccountNumberSection() {
    return this.account.type !== EnumUtils.getNameFromValue(AccountType, AccountType.WALLET);
  }

  public isCreditCard() {
    return this.account.type === EnumUtils.getNameFromValue(AccountType, AccountType.CREDIT_CARD);
  }

  public onChangeStartBalance($event) {
    this.startBalance = $event.target.value;
    if (this.isNew()) {
      this.currentBalance = NumberUtils.currencyToStr(Number(this.startBalance), this.currencyPipe);;
    }
    if (this.startBalance?.length <= 0) {
      this.startBalance = NumberUtils.currencyToStr(0, this.currencyPipe);
    }
  }

  public onChangeCreditLimit($event) {
    this.creditLimit = $event.target.value;
    if (this.creditLimit?.length <= 0) {
      this.creditLimit = NumberUtils.currencyToStr(0, this.currencyPipe);
    }
  }

  public onChangeUpdateBalance($event) {
    this.warningMessage = null;
    if ($event?.target?.checked) {
      const initialState = { 
        title: this.translateService.instant('ACCOUNT.DIALOG.TITLE_UPDATE_BALANCE'),
        message: this.translateService.instant('ACCOUNT.DIALOG.MESSAGE_UPDATE_BALANCE'), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.warningMessage = this.translateService.instant('ACCOUNT.WARNING.UPDATE_BALANCE');
            this.startBalance = NumberUtils.currencyToStr(0, this.currencyPipe);
            this.currentBalance = NumberUtils.currencyToStr(Number(this.startBalance), this.currencyPipe);
            this.account.updateBalanceDisabled = true;                
          } else {
            this.account.updateBalanceDisabled = false;
          }
        });
    }
  }

  public isNew(): boolean {
    return this.account && !this.account.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public disableStartBalance(): boolean {
    return !this.isNew() || this.account.updateBalanceDisabled;
  }
  
  /**
   * Erro ao cadastrar um cartão de crédito:
   * "Ops! accountNumber cannot be bigger than 10"
   * O campo "Account Number" não suporta 16 caracteres.
   * @returns 
   */
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.account.startBalance = NumberUtils.strToCurrency(this.startBalance);
    this.account.balance = NumberUtils.strToCurrency(this.currentBalance);
    this.account.creditLimit = NumberUtils.strToCurrency(this.creditLimit);
    const mode = this.account.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.accountPersistenceContext.context(mode).persist(this.account).subscribe(
      data => {
        console.debug(`Conta criada com sucesso: ${data}`);
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
        console.debug(`Erro na criação de conta: ${error}`)
      }
      );
      this.submitted = true;
    }
    
    public close(): void {
      this.modalRef.content.submitted = this.submitted;
      this.modalRef.hide();
    }
  }
  