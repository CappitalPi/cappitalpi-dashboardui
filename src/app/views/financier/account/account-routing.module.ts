import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountSearchComponent } from './account-search/account-search.component';

const routes: Routes = [
  {
    path: '',
    component: AccountSearchComponent,
    data: {
      title: 'APP.MENU.FINANCIER.CASH-ACCOUNTS'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {

  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }
  
}
