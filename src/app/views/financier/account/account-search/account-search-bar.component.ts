import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormValidationUtils } from '../../../../utils/form-validation.utils';

@Component({
  selector: 'app-account-search-bar',
  templateUrl: './account-search-bar.component.html',
  styleUrls: ['./account-search.component.scss']
})
export class AccountSearchBarComponent implements OnInit {

  public codeFilter: number;
  public accountnameFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, accountnameFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.accountnameFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        accountnameFilter: this.accountnameFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
