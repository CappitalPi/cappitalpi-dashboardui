import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { AccountSearchDto } from '../../../../dto/account/account-search.dto';
import { AccountDto } from '../../../../dto/account/account.dto';
import { AccountService } from '../../../../service/account/account.service';
import { AccountRegisterComponent } from '../account-register/account-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-account-search',
  templateUrl: './account-search.component.html',
  styleUrls: ['./account-search.component.scss']
})
export class AccountSearchComponent implements OnInit {

  public alertMessage : string;
  public warningMessage : string;
  public itemsPerPage: number = 20;
  public pageResult : PageResultDto<AccountSearchDto>;
  public codeFilter: number;
  public accountNameFilter: string;

  constructor(private modalService: BsModalService, 
              private accountService: AccountService,
              private translateService: TranslateService) {
    this.pageResult = new PageResultDto<AccountSearchDto>();
    this.pageResult.totalElements = 0;
  }

  ngOnInit(): void {
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.accountNameFilter = $event.accountnameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.accountService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.accountNameFilter).subscribe(
      (data: PageResultDto<AccountSearchDto>) => {
        this.alertMessage = null;
        this.pageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.pageResult && this.pageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(account: AccountDto) {
    const initialState = { existingAccount: account };
    const modalConfig: {} = { class: 'modal-lg modal-dialog-centered', backdrop: 'static' };
    const registerModalRef = this.modalService.show(AccountRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
    }

  public edit(account: AccountSearchDto) {
    if (account) {
      this.accountService.findByCode(account.code).subscribe(
        data => {
          this.alertMessage = null;
          this.openRegisterScreen(data);
          },
        error => {
          this.alertMessage = error; 
        }
      );
    }
  }

  public openRemoveDialog(account: AccountSearchDto) {
    if (account) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':account.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':account.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger modal-dialog-centered' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(account);
          }
        });
    }
  }

  private remove(account: AccountSearchDto) {
    this.accountService.remove(account.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

  public label(account: AccountDto) {
    let label = account.accountNumber;
    if (account && account.branchNumber && account.accountNumber) {
      label = account.branchNumber+"-"+ account.accountNumber;
    }
    return label;
  }

}
