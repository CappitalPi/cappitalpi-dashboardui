import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountRoutingModule } from './account-routing.module';
import { AccountSearchComponent } from './account-search/account-search.component';
import { AccountRegisterComponent } from './account-register/account-register.component';
import { AccountSearchBarComponent } from './account-search/account-search-bar.component';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TabsModule,
    TranslateModule,
    DirectivesModule,
    AccountRoutingModule,
    ConfirmationDialogModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [ 
    AccountSearchComponent,
    AccountRegisterComponent,
    AccountSearchBarComponent
  ]
})
export class AccountModule { }
