import { Injectable } from "@angular/core";
import { AccountSearchDto } from "../../../../dto/account/account-search.dto";
import { StringUtils } from "../../../../utils/string-utils";
import { DataListViewModel } from "../../component/data-list/view-model/data-list-view-model";
import { DataListViewModelHelper } from "../../component/data-list/view-model/data-list-view-model.helper";

@Injectable({
    providedIn: 'root'
  })
export class AccountDataListViewHelper implements DataListViewModelHelper {
    
    load(list: AccountSearchDto[]): DataListViewModel[] {
        const result: DataListViewModel[] = [];
        if (list?.length > 0) {
            let index: number = 0;
            list.forEach(account => {
                const data: DataListViewModel = new DataListViewModel();
                data.index = index++;
                data.object = account;
                data.uid = account.uid;
                data.code = account.code;
                data.description = this.getAccountDescription(account);
                data.unselectable = false;
                result.push(data);
            });
        } 
        return result;
    }

    private getAccountDescription(account: AccountSearchDto): string {
        let description = StringUtils.EMPTY;
        if (account) {
            if (account.branchNumber) {
                description = account.branchNumber;
            }
            if (account.accountNumber) {
                description += " " + account.accountNumber
            }
            description += " " + account.name;
        }
        return description.trim();
    }
    
}