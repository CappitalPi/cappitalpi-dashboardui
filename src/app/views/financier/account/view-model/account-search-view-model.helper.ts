import { AccountSearchDto } from "../../../../dto/account/account-search.dto";
import { AccountSearchViewModel } from "./account-search-view-model";

export class AccountSearchViewModelHelper {


  public static getItemByName(list: any[], targetName: string) {
    if (list) {
      return list.find(x => x?.label?.toUpperCase().trim() === targetName?.toUpperCase().trim());
    }
    return null;
  }

  public static loadModelList(accountList: AccountSearchDto[]): AccountSearchViewModel[] {
    const accountModelViewList: AccountSearchViewModel[] = [];
    accountList?.forEach(account => {
      const model: AccountSearchViewModel = new AccountSearchViewModel();
      model.account = account;
      model.code = account.code;
      accountModelViewList.push(model);
    });
    this.formatList(accountModelViewList);
    return accountModelViewList;
  }

  public static formatList(accounts: AccountSearchViewModel[]): void {
    if (accounts != null) {
      let index: number = 0;
      for (let accountViewModel of accounts) {
        accountViewModel.index = index++;
        this.format(accountViewModel);
      }
    }
  }

  public static format(model: AccountSearchViewModel): void {
    if (model == null || model.account == null) {
      return null;
    }
    var branchNumber = model.account.branchNumber?.length > 0 ? model.account.branchNumber.trim() : ""
    var accountNumber = model.account.accountNumber?.length > 0 ? model.account.accountNumber.trim() : ""
    model.label = `${branchNumber} ${accountNumber} ${model.account.name}`;
  }
  
}