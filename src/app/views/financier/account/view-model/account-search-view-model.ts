import { AccountSearchDto } from "../../../../dto/account/account-search.dto";

export class AccountSearchViewModel {
    code: number;
    index: number;
    account: AccountSearchDto;
    label: string;
}

