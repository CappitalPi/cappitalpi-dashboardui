import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartOfAccountRegisterComponent } from './chart-of-account-register.component';

describe('ChartOfAccountRegisterComponent', () => {
  let component: ChartOfAccountRegisterComponent;
  let fixture: ComponentFixture<ChartOfAccountRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartOfAccountRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartOfAccountRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
