import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { EnumValues } from 'enum-values';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { ChartOfAccountCategory } from '../../../../enum/chart-of-account-category.enum';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { ChartOfAccountPersistenceContext } from '../../../../strategy/persistence/chart-of-account/chart-of-account-persistence-context.strategy';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { ChartOfAccountViewModelHelper } from '../view-model/chart-of-account-view-model.helper';

@Component({
  selector: 'app-chart-of-account-register',
  templateUrl: './chart-of-account-register.component.html',
  styleUrls: ['./chart-of-account-register.component.scss']
})
export class ChartOfAccountRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingChartOfAccount: ChartOfAccountDto;
  @Input() public masterAccount: ChartOfAccountDto;

  public chartOfAccount: ChartOfAccountDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public selectedType: string;
  public selectedCategory: string;
  public selectedChartOfAccount: ChartOfAccountDto;
  public selectedChartOfAccountLabel: string;
  public categories: ChartOfAccountCategory[];
  public types: TransactionType[];

  constructor(public modalRef: BsModalRef, private chartOfAccountPersistenceContext: ChartOfAccountPersistenceContext) {
    this.chartOfAccount = new ChartOfAccountDto();
  }
  
  ngOnInit(): void {
    if (this.existingChartOfAccount) {
      this.loadExistingData();
    } else {
      this.prepareData();
    }
    this.findCategories();
    this.findTypes();
  }

  private prepareData() {
    if (!this.masterAccount) {
      this.selectedCategory = EnumValues.getNameFromValue(ChartOfAccountCategory, ChartOfAccountCategory.SYNTHETIC);
    } else {
      this.selectedChartOfAccount = this.masterAccount;
      this.selectedChartOfAccountLabel = ChartOfAccountViewModelHelper.formatLabel(this.masterAccount);
      this.selectedType = EnumValues.getNameFromValue(TransactionType, this.masterAccount?.type);
      if (this.masterAccount.lastLevel) {
        this.selectedCategory = EnumValues.getNameFromValue(ChartOfAccountCategory, ChartOfAccountCategory.ANALYTIC);
      }
    }
  }

  private loadExistingData() {
    this.chartOfAccount = this.existingChartOfAccount;
    this.selectedChartOfAccount = this.existingChartOfAccount.parent;
    this.selectedChartOfAccountLabel = ChartOfAccountViewModelHelper.formatLabel(this.existingChartOfAccount.parent);
    this.selectedType = EnumValues.getNameFromValue(TransactionType, this.existingChartOfAccount.type);
    this.selectedCategory = EnumValues.getNameFromValue(ChartOfAccountCategory, this.existingChartOfAccount.category);
  }

  public findTypes(): void{
    if (!this.types) {
      this.types = EnumUtils.getList(TransactionType, false);
    }
  }
  
  public findCategories(): void{
    if (!this.categories) {
      this.categories = EnumUtils.getList(ChartOfAccountCategory, false);
    }
  }

  ngAfterViewInit(): void {
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public isNew(): boolean {
    return this.chartOfAccount && !this.chartOfAccount.uid;
  }

  public hasMasterAccount(): boolean {
    return this.selectedChartOfAccount?.uid?.length > 0;
  }

  public disabledCategoryField() {
    return this.chartOfAccount?.uid?.length > 0 || !this.hasMasterAccount() || this.masterAccount?.lastLevel;
  }

  public disabledTypeField() {
    return this.chartOfAccount?.uid?.length > 0 || this.hasMasterAccount();
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public isCredit(): Boolean {
    if (this.selectedType?.length > 0) {
      return EnumValues.getNameFromValue(TransactionType, TransactionType.CREDIT) == this.selectedType;
    }
    return null;
  }
  
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.chartOfAccount.category = EnumUtils.getValueFromName(ChartOfAccountCategory, this.selectedCategory) as string;
    this.chartOfAccount.type = EnumUtils.getValueFromName(TransactionType, this.selectedType) as string;
    this.chartOfAccount.parent = this.selectedChartOfAccount;
    const mode = this.chartOfAccount.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.chartOfAccountPersistenceContext.context(mode).persist(this.chartOfAccount).subscribe(
      data => {
        console.debug(`Chart of Account created successfully. ${JSON.stringify(data)}`);
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
      }
      );
      this.submitted = true;
    }
    
    public close(): void {
      this.modalRef.content.submitted = this.submitted;
      this.modalRef.hide();
    }
  }
  