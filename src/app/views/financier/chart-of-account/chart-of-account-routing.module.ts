import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ChartOfAccountSearchComponent } from './chart-of-account-search/chart-of-account-search.component';

const routes: Routes = [
  {
    path: '',
    component: ChartOfAccountSearchComponent,
    data: {
      title: 'APP.MENU.FINANCIER.CHART-OF-ACCOUNTS'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartOfAccountRoutingModule {
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }
}
