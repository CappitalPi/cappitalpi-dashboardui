import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormValidationUtils } from '../../../../utils/form-validation.utils';

@Component({
  selector: 'app-chart-of-account-search-bar',
  templateUrl: './chart-of-account-search-bar.component.html',
  styleUrls: ['./chart-of-account-search.component.scss']
})
export class ChartOfAccountSearchBarComponent implements OnInit {

  public codeFilter: number;
  public chartOfAccountNameFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, chartOfAccountNameFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.chartOfAccountNameFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        chartOfAccountNameFilter: this.chartOfAccountNameFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
