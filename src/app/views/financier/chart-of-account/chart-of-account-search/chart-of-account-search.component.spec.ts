import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChartOfAccountSearchComponent } from './chart-of-account-search.component';


describe('ChartOfAccountSearchComponent', () => {
  let component: ChartOfAccountSearchComponent;
  let fixture: ComponentFixture<ChartOfAccountSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartOfAccountSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartOfAccountSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
