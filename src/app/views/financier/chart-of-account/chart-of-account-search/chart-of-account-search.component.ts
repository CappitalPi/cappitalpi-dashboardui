import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { ChartOfAccountRegisterComponent } from '../chart-of-account-register/chart-of-account-register.component';
import { ChartOfAccountViewModelHelper } from '../view-model/chart-of-account-view-model.helper';

@Component({
  selector: 'app-chart-of-account-search',
  templateUrl: './chart-of-account-search.component.html',
  styleUrls: ['./chart-of-account-search.component.scss']
})
export class ChartOfAccountSearchComponent implements OnInit {

  public alertMessage : string;
  public warningMessage : string;
  public pageResult : PageResultDto<ChartOfAccountDto>;
  public codeFilter: number;
  public chartOfAccountNameFilter: string;

  constructor(private modalService: BsModalService, 
              private chartOfAccountService: ChartOfAccountService,
              private translateService: TranslateService) {
    this.pageResult = new PageResultDto<ChartOfAccountDto>();
    this.pageResult.totalElements = 0;
  }

  ngOnInit(): void {
    this.findList();
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.chartOfAccountNameFilter = $event.chartOfAccountNameFilter;
    this.findList();
  }

  private findList(): void {
    this.chartOfAccountService.filter(this.codeFilter, this.chartOfAccountNameFilter).subscribe(
      (data: PageResultDto<ChartOfAccountDto>) => {
        this.alertMessage = null;
        this.pageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.pageResult && this.pageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public addMasterAccount() {
    this.openRegisterScreen(null, null);
  }

  public add(masterAccount: ChartOfAccountDto) {
    this.openRegisterScreen(null, masterAccount);
  }

  public openRegisterScreen(chartOfAccount: ChartOfAccountDto, masterAccount: ChartOfAccountDto) {
    const initialState = { existingChartOfAccount: chartOfAccount, masterAccount: masterAccount };
    const modalConfig: {} = { class: 'modal-lg modal-dialog-centered', backdrop: 'static' };
    const registerModalRef = this.modalService.show(ChartOfAccountRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList();
        }
      });
  }

  public edit(chartOfAccount: ChartOfAccountDto) {
    if (chartOfAccount) {
      this.chartOfAccountService.findByCode(chartOfAccount.code).subscribe(
        data => {
          this.alertMessage = null;
          this.openRegisterScreen(data, null);
          },
        error => {
          this.alertMessage = error; 
        }
      );
    }
  }

  public openRemoveDialog(chartOfAccount: ChartOfAccountDto) {
    if (chartOfAccount) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':chartOfAccount.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':chartOfAccount.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-dialog-centered modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(chartOfAccount);
          }
        });
    }
  }

  private remove(chartOfAccount: ChartOfAccountDto) {
    this.chartOfAccountService.remove(chartOfAccount.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList();
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public formatDescription(chartOfAccount: ChartOfAccountDto) : string {
    return ChartOfAccountViewModelHelper.getTab(chartOfAccount.accountNumber) +""+ chartOfAccount.accountNumber+" - "+chartOfAccount.name;
  }

}
