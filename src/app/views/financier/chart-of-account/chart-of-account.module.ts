import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartOfAccountRoutingModule } from './chart-of-account-routing.module';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ChartOfAccountSearchComponent } from './chart-of-account-search/chart-of-account-search.component';
import { ChartOfAccountSearchBarComponent } from './chart-of-account-search/chart-of-account-search-bar.component';
import { ChartOfAccountRegisterComponent } from './chart-of-account-register/chart-of-account-register.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    ChartOfAccountRoutingModule,
    ConfirmationDialogModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [ 
    ChartOfAccountSearchComponent,
    ChartOfAccountRegisterComponent,
    ChartOfAccountSearchBarComponent
  ]
})
export class ChartOfAccountModule { }
