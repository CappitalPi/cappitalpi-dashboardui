import { Injectable } from "@angular/core";
import { ChartOfAccountDto } from "../../../../dto/chart-of-account/chart-of-account.dto";
import { DataListViewModel } from "../../component/data-list/view-model/data-list-view-model";
import { DataListViewModelHelper } from "../../component/data-list/view-model/data-list-view-model.helper";

@Injectable({
    providedIn: 'root'
  })
export class ChartOfAccountDataListViewHelper implements DataListViewModelHelper {

    public load(list: ChartOfAccountDto[]): DataListViewModel[] {
        const result: DataListViewModel[] = [];
        if (list?.length > 0) {
            let index: number = 0;
            list.forEach(chartOfAccount => {
                const data: DataListViewModel = new DataListViewModel();
                data.index = index++;
                data.object = chartOfAccount;
                data.uid = chartOfAccount.uid;
                data.code = chartOfAccount.code;
                data.description = this.getTab(chartOfAccount.accountNumber) +""+ chartOfAccount.accountNumber+". "+chartOfAccount.name;
                data.unselectable = chartOfAccount.category === "S";
                result.push(data);
            });
        } 
        return result;
    }

    private getTab(accountNumber: string) {
        if (accountNumber.length > 1) {
            let tabSpace = "";
            for (let index = 0; index < accountNumber.length; index++) {
                tabSpace = tabSpace + '\xa0\xa0';
            }
            return tabSpace;
        }
        return "";
    }

}