import { ChartOfAccountDto } from "../../../../dto/chart-of-account/chart-of-account.dto";

export class ChartOfAccountViewModel {
    code: number;
    index: number;
    chartOfAccount: ChartOfAccountDto;
    label: string;
}

