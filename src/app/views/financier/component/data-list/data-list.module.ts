import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { DataListComponent } from './list/data-list.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [
    DataListComponent
  ],
  exports: [
    DataListComponent
  ]
})
export class DataListModule { }
