import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { StringUtils } from '../../../../../utils/string-utils';
import { DataListDtoModel } from '../view-model/data-list-dto-model';
import { DataListViewModel } from '../view-model/data-list-view-model';

@Component({
  selector: 'list-data',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {

  @Input()
  public dataList : DataListViewModel[];
  @Input()
  public showCodeColumn : boolean;
  @Input()
  public isDisabled : boolean;
  @Input()
  public multiSelect : boolean;
  @Input()
  public disableClick : boolean;
  @Output()
  public paramsEmiter = new EventEmitter<{dataList: DataListViewModel[]}>();
  @Output()
  public selectedItemEmiter = new EventEmitter<{selectedItem: DataListViewModel}>();
  @Output()
  public selectedItemsEmiter = new EventEmitter<{selectedItems: DataListViewModel[]}>();
  private selectedItems: DataListViewModel[];
  public allDataList : DataListViewModel[];

  constructor() {
    this.allDataList = [];
    this.selectedItems = [];
    this.multiSelect = false;
  }

  ngOnInit(): void {
    this.disableClick = this.isDisabled;
  }

  @Input() set setDataList(dataListDto : DataListDtoModel) {
    this.dataList = [];
    this.allDataList = [];
    if (dataListDto?.list?.length > 0) {
      this.dataList = dataListDto.list;
      this.allDataList = dataListDto.list;
      this.selectItemByUid(dataListDto);
    }
  }

  private selectItemByUid(dataListDto : DataListDtoModel): void {
    if (dataListDto.selectedUid) {
      let selectedIndex : number = null;
      dataListDto.list.forEach(item => {
        if (item?.uid === dataListDto.selectedUid) {
          selectedIndex = item.index;
        }
      });
      this.selectItem(selectedIndex);
    }
  }

  public selectItem(index: number): void {
    if (index >= 0) {
     const selectedItem = this.allDataList[index];
     if (!this.multiSelect || !this.selectedItems) {
      this.selectedItems = [];
     }
     if (this.isSelected(selectedItem)) {
       this.selectedItems = this.selectedItems.filter(item => item.index != selectedItem.index);
    } else {
       this.selectedItems.push(selectedItem);
     }
      this.selectedItemsEmiter.emit({ selectedItems: this.selectedItems });
    }
  }

  public search(value: string): void {
    if (value?.length == 0) {
      this.dataList = this.allDataList;
    } else {
      this.dataList = this.allDataList.filter((val) => StringUtils.ignoreAccents(val.description.toLowerCase()).includes(StringUtils.ignoreAccents(value)));
    }
  }
  
  public hasData(): boolean {
    return this.dataList?.length > 0;
  }

  public isSelected(item: DataListViewModel): boolean {
    const selectedItem = this.selectedItems?.filter((obj) => obj.index === item.index);
    return !item.unselectable && selectedItem?.length > 0;
  }

  public showCode() {
    return this.showCodeColumn;
  }

}
