import { DataListViewModel } from "./data-list-view-model";

export interface DataListViewModelHelper {

    load(list: any[]): DataListViewModel[];

}