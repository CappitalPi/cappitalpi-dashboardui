import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TransactionPersistenceContext } from '../../../../strategy/persistence/transaction/transaction-persistence-context.strategy';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { TransactionDto } from '../../../../dto/transaction/transaction.dto';
import { AccountSearchDto } from '../../../../dto/account/account-search.dto';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { DataListViewModel } from '../../component/data-list/view-model/data-list-view-model';
import { AccountService } from '../../../../service/account/account.service';
import { AccountDataListViewHelper } from '../../account/view-model/account-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { ChartOfAccountDataListViewHelper } from '../../chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { DateUtils } from '../../../../utils/date.utils';
import { TranslateService } from '@ngx-translate/core';
import { EnumValueType } from 'enum-values/src/enumValues';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { CurrencyPipe } from '@angular/common';
import { NumberUtils } from '../../../../utils/number-utils';
import { TransactionCreationType } from '../../../../enum/transaction-creation-type.enum';
import { PaymentMethod } from '../../../../enum/payment-method.enum';
import { AccountDto } from '../../../../dto/account/account.dto';
import { AccountType } from '../../../../enum/account-type.enum';

@Component({
  selector: 'app-transaction-register',
  templateUrl: './transaction-register.component.html',
  styleUrls: ['./transaction-register.component.scss']
})
export class TransactionRegisterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public isViewMode: boolean;
  @Input() public existingTransaction: TransactionDto;
  @Input() public selectedType: { name: string, value: EnumValueType };

  public transaction: TransactionDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public transactionDate : string;
  public amount : string;
  public paymentMethods: PaymentMethod[];
  public selectedAccount: DataListViewModel;
  public selectedChartOfAccount: DataListViewModel;
  public accountDataList: DataListDtoModel;
  public chartOfAccountDataList: DataListDtoModel;

  constructor(public modalRef: BsModalRef,
              private transactionPersistenceContext: TransactionPersistenceContext,
              private accountService: AccountService,
              private translateService: TranslateService,
              private chartOfAccountService: ChartOfAccountService,
              private accountDataListViewHelper: AccountDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper,
              private currencyPipe: CurrencyPipe) {
    this.transactionDate = DateUtils.formatToShortDate(new Date());
    this.amount = NumberUtils.currencyToStr(0, currencyPipe);
    this.transaction = new TransactionDto();
    this.transaction.amount = 0;
    this.transaction.transactionDate =  DateUtils.now();
    this.transaction.cashAccount = new AccountSearchDto();
    this.transaction.chartOfAccount = new ChartOfAccountDto();
    this.selectedAccount = new DataListViewModel();
    this.selectedChartOfAccount = new DataListViewModel();
  }
  
  ngOnInit(): void {
    if (this.existingTransaction) {
      this.transaction = this.existingTransaction;
      this.selectedAccount.uid = this.existingTransaction.cashAccount.uid;
      this.selectedChartOfAccount.uid = this.existingTransaction.chartOfAccount.uid;
      this.amount = NumberUtils.currencyToStr(this.existingTransaction.amount, this.currencyPipe);
      this.transactionDate = DateUtils.formatToShortDate(this.transaction.transactionDate);
      this.selectedType = EnumUtils.getEnumTypeFromValue(TransactionType, this.existingTransaction.transactionType);
      this.findPaymentMethods();
    }
    this.findAccounts();
    this.findChartOfAccounts();
  }

  public findAccounts(): void{
    this.accountService.findAll().subscribe(
      (data: AccountSearchDto[]) => {
        this.accountDataList = new DataListDtoModel();
        this.accountDataList.list = this.accountDataListViewHelper.load(data);
        this.accountDataList.selectedUid = this.selectedAccount.uid;
      },
      error => {
        console.error(`Error ${error}`);
        this.alertMessage = error;//TODO
      }
    );
  }

  public findChartOfAccounts(): void{
    this.chartOfAccountService.findByType(this.selectedType.value.toString()).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountDataList = new DataListDtoModel();
        this.chartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.chartOfAccountDataList.selectedUid = this.selectedChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }
  
  public accountSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.selectedAccount = $event.selectedItems[0];
      this.findPaymentMethods();
    }
  }

  private findPaymentMethods(): void {
    this.paymentMethods = [];
    if (this.selectedAccount?.object) {
      const account: AccountDto = this.selectedAccount.object;      
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.CREDIT_CARD)) {
        this.paymentMethods.push(PaymentMethod.CREDIT_CARD);
      }
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.WALLET)) {
        this.paymentMethods.push(PaymentMethod.CASH);
        this.paymentMethods.push(PaymentMethod.CHECK);
      }
      if (account.type === EnumUtils.getNameFromValue(AccountType, AccountType.BANK) || account.type === EnumUtils.getNameFromValue(AccountType, AccountType.INVESTMENT)) {
        this.paymentMethods.push(PaymentMethod.BANK_SLIP);
        this.paymentMethods.push(PaymentMethod.BANK_TRANSFER);
        this.paymentMethods.push(PaymentMethod.CHECK);
        this.paymentMethods.push(PaymentMethod.DEBIT_CARD);
        this.paymentMethods.push(PaymentMethod.DEPOSIT);
        this.paymentMethods.push(PaymentMethod.PAYPAL);
      }
    }
    this.adjustPaymentMethodSelection();
  }

  private adjustPaymentMethodSelection() {
    if (this.transaction?.uid != null && this.transaction.paymentMethod) {
      const hasPaymentMethod = this.paymentMethods.find(paymentMethod => paymentMethod === this.transaction?.paymentMethod);
      if (!hasPaymentMethod) {
        this.paymentMethods.push(this.transaction.paymentMethod);
      }
    }
  }

  /**
   * Método necessário pois existe um bug na diretiva ao incluir aos trilhões.
   * Por algum motivo, a aplicação está aceitando um dígito a mais.
   * TODO: Tentar corrigir a diretiva ou criar outra.
   * @param $event 
   */
  public onChangeAmount($event) {
    this.amount = $event.target.value;
  }

  public chartOfAccountSelected($event): void {
    this.transaction.chartOfAccount = new ChartOfAccountDto();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.transaction.chartOfAccount = $event.selectedItems[0].object;
      }
    }
  }

  public isNew(): boolean {
    return this.transaction && !this.transaction.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public isCheckNumberDisabled() {
    if (this.transaction?.paymentMethod == PaymentMethod.CHECK || this.transaction?.paymentMethod == PaymentMethod.DEPOSIT) {
      return false;
    }
    this.transaction.checkNumber = '';
    return true;
  }

  public persist(): void {
    try {
      this.disableSaveButton = true;
      this.submitted = false;
      this.alertMessage = null;
      if (!this.form.valid) {
        FormValidationUtils.markFieldsAsTouched(this.form);
        this.disableSaveButton = false;
        return;
      }
      if (DateUtils.isDateValid(this.transactionDate)) {
        this.transaction.transactionDate = DateUtils.strToDate(this.transactionDate);
      } else {
        this.alertMessage = this.translateService.instant('APP.MESSAGE.INVALID_DATE');
        return;
      }
      this.transaction.amount = NumberUtils.strToCurrency(this.amount);
      this.transaction.transactionType = this.selectedType?.value?.toString();
      this.transaction.cashAccount = this.selectedAccount.object;
      this.transaction.creationType = TransactionCreationType.MANUAL.toString();
      const mode = this.transaction.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
      this.transactionPersistenceContext.context(mode).persist(this.transaction).subscribe(
        data => {
          console.debug(`Transação criada com sucesso: ${data}`);
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
          console.error(`Erro na criação da transação: ${error}`)
        }
        );
        this.submitted = true;
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }
  }
  
  public isUpdate(): boolean {
    return this.transaction?.uid?.length > 0
  }

  public isCredit(): boolean {
    return this.selectedType?.value == TransactionType.CREDIT;
  }
    
  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }
}
  
  