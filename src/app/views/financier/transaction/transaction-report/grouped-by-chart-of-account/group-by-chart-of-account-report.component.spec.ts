import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GroupByChartOfAccountReportComponent } from './group-by-chart-of-account-report.component';


describe('GroupByChartOfAccountReportComponent', () => {
  let component: GroupByChartOfAccountReportComponent;
  let fixture: ComponentFixture<GroupByChartOfAccountReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupByChartOfAccountReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupByChartOfAccountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
