import { Component, Input, OnInit } from '@angular/core';
import { ChartOfAccountTotalDto } from '../../../../../dto/transaction/chart-of-account-total.dto';
import { TransactionReportRequestDto } from '../../../../../dto/transaction/transaction-report-request.dto';
import { TransactionReportService } from '../../../../../service/transaction/transaction-report.service';
import { DateUtils } from '../../../../../utils/date.utils';
import { ChartOfAccountViewModelHelper } from '../../../chart-of-account/view-model/chart-of-account-view-model.helper';

@Component({
  selector: 'app-group-by-chart-of-account-report',
  templateUrl: './group-by-chart-of-account-report.component.html',
  styleUrls: ['./group-by-chart-of-account-report.component.scss']
})
export class GroupByChartOfAccountReportComponent implements OnInit {

  public warningMessage : string;
  public reportDataList: ChartOfAccountTotalDto[];
  public startDate: string;
  public endDate: string;

  constructor(private transactionService: TransactionReportService){
  }

  ngOnInit(): void {
  }

  @Input() set filter(filterRequest: TransactionReportRequestDto) {
    this.startDate = filterRequest.startDate;
    this.endDate = filterRequest.endDate;
    this.transactionService.findGroupedByChartOfAccounts(
      DateUtils.strToDate(filterRequest.startDate),
      DateUtils.strToDate(filterRequest.endDate),
      this.getAccounts(filterRequest),
      this.getChartOfAccounts(filterRequest)
    ).subscribe(
      (data: ChartOfAccountTotalDto[]) => {
        this.reportDataList = data;
      },
      error => {
        console.error(`Error ${error}`);
        this.warningMessage = error;
      }
    );

  }


  private getAccounts(filterRequest: TransactionReportRequestDto) {
    const accounts: string[] = [];
    if (filterRequest?.accountSelectedList?.length > 0) {
      filterRequest.accountSelectedList.forEach(element => {
        accounts.push(element.uid);
      });
    }
    return accounts;
  }

  private getChartOfAccounts(filterRequest: TransactionReportRequestDto) {
    const chartOfAccounts: string[] = [];
    if (filterRequest?.chartOfAccountCreditSelectedList?.length > 0) {
      filterRequest.chartOfAccountCreditSelectedList.forEach(element => {
        chartOfAccounts.push(element.uid);
      });
    }
    if (filterRequest?.chartOfAccountDebitSelectedList?.length > 0) {
      filterRequest.chartOfAccountDebitSelectedList.forEach(element => {
        chartOfAccounts.push(element.uid);
      });
    }
    return chartOfAccounts;
  }

  public formatDescription(chartOfAccount: ChartOfAccountTotalDto) : string {
    return ChartOfAccountViewModelHelper.getTab(chartOfAccount.accountNumber) +""+ chartOfAccount.accountNumber+" - "+chartOfAccount.name;
  }

  public hasData() :boolean {
    return this.reportDataList?.length > 0;
  }
}
