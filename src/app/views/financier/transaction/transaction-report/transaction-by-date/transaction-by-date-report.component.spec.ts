import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransactionByDateReportComponent } from './transaction-by-date-report.component';


describe('TransactionByDateReportComponent', () => {
  let component: TransactionByDateReportComponent;
  let fixture: ComponentFixture<TransactionByDateReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionByDateReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionByDateReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
