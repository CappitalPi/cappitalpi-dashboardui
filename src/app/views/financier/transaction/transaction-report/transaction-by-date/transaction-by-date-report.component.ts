import { Component, Input, OnInit } from '@angular/core';
import { TransactionReportRequestDto } from '../../../../../dto/transaction/transaction-report-request.dto';
import { TransactionTotalDto } from '../../../../../dto/transaction/transaction-total.dto';
import { TransactionDto } from '../../../../../dto/transaction/transaction.dto';
import { TransactionReportService } from '../../../../../service/transaction/transaction-report.service';
import { DateUtils } from '../../../../../utils/date.utils';
import { StringUtils } from '../../../../../utils/string-utils';

@Component({
  selector: 'app-transaction-by-date-report',
  templateUrl: './transaction-by-date-report.component.html',
  styleUrls: ['./transaction-by-date-report.component.scss']
})
export class TransactionByDateReportComponent implements OnInit {

  public warningMessage : string;
  public reportDataList: TransactionTotalDto;
  public startDate: string;
  public endDate: string;

  constructor(private transactionService: TransactionReportService){
  }

  ngOnInit(): void {
  }

  @Input() set filter(filterRequest: TransactionReportRequestDto) {
    this.startDate = filterRequest.startDate;
    this.endDate = filterRequest.endDate;
    this.transactionService.findByTransactionDate(
      DateUtils.strToDate(filterRequest.startDate),
      DateUtils.strToDate(filterRequest.endDate),
      this.getAccounts(filterRequest),
      this.getChartOfAccounts(filterRequest)
    ).subscribe(
      (data: TransactionTotalDto) => {
        this.reportDataList = data;
      },
      error => {
        console.error(`Error ${error}`);
        this.warningMessage = error;
      }
    );
  }

  private getAccounts(filterRequest: TransactionReportRequestDto) {
    const accounts: string[] = [];
    if (filterRequest?.accountSelectedList?.length > 0) {
      filterRequest.accountSelectedList.forEach(element => {
        accounts.push(element.uid);
      });
    }
    return accounts;
  }

  private getChartOfAccounts(filterRequest: TransactionReportRequestDto) {
    const chartOfAccounts: string[] = [];
    if (filterRequest?.chartOfAccountCreditSelectedList?.length > 0) {
      filterRequest.chartOfAccountCreditSelectedList.forEach(element => {
        chartOfAccounts.push(element.uid);
      });
    }
    if (filterRequest?.chartOfAccountDebitSelectedList?.length > 0) {
      filterRequest.chartOfAccountDebitSelectedList.forEach(element => {
        chartOfAccounts.push(element.uid);
      });
    }
    return chartOfAccounts;
  }

  public hasData() :boolean {
    return this.reportDataList?.transactions?.length > 0;
  }

  public isCredit(transaction: TransactionDto): boolean {
    return transaction?.transactionType == 'C';
  }

  public formatDescription(description: string): string{
    return StringUtils.formatLongText(description);
  }

  public deleteRow(selectedTransaction: TransactionDto): void{
    const index = this.reportDataList.transactions.indexOf(selectedTransaction);
    if (this.isCredit(selectedTransaction)) {
      this.reportDataList.totalCredit = this.reportDataList.totalCredit - selectedTransaction.amount;
    } else {
      this.reportDataList.totalDebit = this.reportDataList.totalDebit - selectedTransaction.amount;
    }
    this.reportDataList.total = this.reportDataList.totalCredit - this.reportDataList.totalDebit;
    this.reportDataList.transactions.splice(index, 1);
  }

}
