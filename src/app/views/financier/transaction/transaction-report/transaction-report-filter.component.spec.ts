import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransactionReportFilterComponent } from './transaction-report-filter.component';

describe('TransactionReportFilterComponent', () => {
  let component: TransactionReportFilterComponent;
  let fixture: ComponentFixture<TransactionReportFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionReportFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionReportFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
