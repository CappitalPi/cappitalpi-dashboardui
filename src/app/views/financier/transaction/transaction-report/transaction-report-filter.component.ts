import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { AccountSearchDto } from '../../../../dto/account/account-search.dto';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { AccountService } from '../../../../service/account/account.service';
import { AccountDataListViewHelper } from '../../account/view-model/account-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { ChartOfAccountDataListViewHelper } from '../../chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { DateUtils } from '../../../../utils/date.utils';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { EnumValueType } from 'enum-values/src/enumValues';

@Component({
  selector: 'app-transaction-report-filter',
  templateUrl: './transaction-report-filter.component.html'
})
export class TransactionReportFilterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public reportType: { name: string, value: EnumValueType };

  public submitted : boolean = false;
  public alertMessage : string;
  public startDate : string;
  public endDate : string;
  public accountDataList: DataListDtoModel;
  public accountSelectedList: AccountSearchDto[]; 
  public chartOfAccountCreditDataList: DataListDtoModel;
  public chartOfAccountCreditSelectedList: ChartOfAccountDto[]; 
  public chartOfAccountDebitDataList: DataListDtoModel;
  public chartOfAccountDebitSelectedList: ChartOfAccountDto[]; 

  constructor(public modalRef: BsModalRef,
              private accountService: AccountService,
              private chartOfAccountService: ChartOfAccountService,
              private accountDataListViewHelper: AccountDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper) {
    this.startDate = DateUtils.formatToShortDate(DateUtils.substractDays(new Date(), 30));
    this.endDate = DateUtils.formatToShortDate(new Date());
  }
  
  ngOnInit(): void {
    this.findAccounts();
    this.findChartOfAccountsCredit();
    this.findChartOfAccountsDebit();
  }

  public findAccounts(): void{
    this.accountService.findAll().subscribe(
      (data: AccountSearchDto[]) => {
        this.accountDataList = new DataListDtoModel();
        this.accountDataList.list = this.accountDataListViewHelper.load(data);
      },
      error => {
        console.error(`Error ${error}`);
        this.alertMessage = error;
      }
    );
  }

  public findChartOfAccountsCredit(): void{
    this.chartOfAccountService.findByType(TransactionType.CREDIT.toString()).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountCreditDataList = new DataListDtoModel();
        this.chartOfAccountCreditDataList.list = this.chartOfAccountDataListViewHelper.load(data);
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  public findChartOfAccountsDebit(): void{
    this.chartOfAccountService.findByType(TransactionType.DEBIT.toString()).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountDebitDataList = new DataListDtoModel();
        this.chartOfAccountDebitDataList.list = this.chartOfAccountDataListViewHelper.load(data);
      },
      error => {
        this.alertMessage = error;
      }
    );
  }
  
  public accountsSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.accountSelectedList = [];
      $event.selectedItems.forEach(element => {
        this.accountSelectedList.push(element.object);
      });
    }
  }

  public chartOfAccountsCreditSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.chartOfAccountCreditSelectedList = [];
      $event.selectedItems.forEach(element => {
        this.chartOfAccountCreditSelectedList.push(element.object);
      });
    }
  }
  
  public chartOfAccountsDebitSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.chartOfAccountDebitSelectedList = [];
      $event.selectedItems.forEach(element => {
        this.chartOfAccountDebitSelectedList.push(element.object);
      });
    }
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public filter(): void {
    this.submitted = false;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      return;
    }
    this.submitted = true;
    this.close();   
  }
  
  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }
}
  
  