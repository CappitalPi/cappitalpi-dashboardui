import { Component, OnInit } from '@angular/core';
import { EnumValueType } from 'enum-values/src/enumValues';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { TransactionReportRequestDto } from '../../../../dto/transaction/transaction-report-request.dto';
import { TransactionReportType } from '../../../../enum/transaction-report-type.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { TransactionReportFilterComponent } from './transaction-report-filter.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-transaction-report',
  templateUrl: './transaction-report.component.html',
  styleUrls: ['./transaction-report.component.scss']
})
export class TransactionReportComponent implements OnInit {

  public warningMessage : string;
  public filterRequest: TransactionReportRequestDto

  constructor(private modalService: BsModalService) {
  }

  ngOnInit(): void {
  }

  public openFilterScreen(reportName: string): void {
    const reportType: { name: string, value: EnumValueType } = EnumUtils.getEnumTypeFromName(TransactionReportType, reportName);
    const initialState = { reportType: reportType };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(TransactionReportFilterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.filterRequest = new TransactionReportRequestDto();
          this.filterRequest.startDate = registerModalRef.content.startDate;
          this.filterRequest.endDate = registerModalRef.content.endDate;
          this.filterRequest.selectedReport = registerModalRef.content.reportType.name;
          this.filterRequest.accountSelectedList = registerModalRef.content.accountSelectedList;
          this.filterRequest.chartOfAccountCreditSelectedList = registerModalRef.content.chartOfAccountCreditSelectedList;
          this.filterRequest.chartOfAccountDebitSelectedList = registerModalRef.content.chartOfAccountDebitSelectedList;
        } else {
          this.back();
        }
      });
  }

  public back(): void {
    this.filterRequest = null;
  }

  public showReport(): boolean {
    return this.filterRequest?.selectedReport?.length > 0;
  }

}
