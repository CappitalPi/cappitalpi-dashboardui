import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { TransactionSearchByDayComponent } from './transaction-search-by-day/transaction-search-by-day.component';
import { TransactionSearchComponent } from './transaction-search/transaction-search.component';

const routes: Routes = [
  {
    path: 'today',
    component: TransactionSearchByDayComponent,
    data: {
      title: 'APP.MENU.FINANCIER.TRANSACTIONS'
    }
  },
  {
    path: 'history',
    component: TransactionSearchComponent,
    data: {
      title: 'APP.MENU.FINANCIER.HISTORY'
    }
  },
  {
    path: 'report',
    component: TransactionReportComponent,
    data: {
      title: 'APP.MENU.FINANCIER.REPORT'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionRoutingModule {
  
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
