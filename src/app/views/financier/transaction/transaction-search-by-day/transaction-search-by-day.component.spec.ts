import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionSearchByDayComponent } from './transaction-search-by-day.component';

describe('TransactionSearchByDayComponent', () => {
  let component: TransactionSearchByDayComponent;
  let fixture: ComponentFixture<TransactionSearchByDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionSearchByDayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSearchByDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
