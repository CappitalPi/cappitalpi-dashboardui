import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AccountSearchDto } from '../../../../dto/account/account-search.dto';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { AccountService } from '../../../../service/account/account.service';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { FormValidationUtils } from '../../../../utils/form-validation.utils';
import { AccountSearchViewModel } from '../../account/view-model/account-search-view-model';
import { AccountSearchViewModelHelper } from '../../account/view-model/account-search-view-model.helper';
import { ChartOfAccountViewModelHelper } from '../../chart-of-account/view-model/chart-of-account-view-model.helper';
import { ChartOfAccountViewModel } from '../../chart-of-account/view-model/chart-of-account-view-model';
import { DateUtils } from '../../../../utils/date.utils';

@Component({
  selector: 'app-transaction-search-bar',
  templateUrl: './transaction-search-bar.component.html',
  styleUrls: ['./transaction-search.component.scss']
})
export class TransactionSearchBarComponent implements OnInit {

  public codeFilter: number;
  public startDateFilter: string;
  public endDateFilter: string;
  public accounts: AccountSearchViewModel[];
  public selectedAccount: string;
  public selectedChartOfAccount: string;
  public chartOfAccounts: ChartOfAccountViewModel[];
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{  
    startDateFilter: string,
    endDateFilter: String,
    codeFilter: number,
    accountUidFilter: string,
    chartOfAccountUidFilter: string }>();

  constructor(public accountService: AccountService, public chartOfAccountService: ChartOfAccountService) {
    this.startDateFilter = DateUtils.formatToShortDate(DateUtils.addDays(new Date(), -7));
    this.endDateFilter = DateUtils.formatToShortDate(new Date());
  }

  ngOnInit(): void {
    this.accounts = [];
    this.chartOfAccounts = [];
    this.findAccounts();
    this.findChartOfAccounts();
    this.search();
  }

  private findAccounts(): void {
    this.accountService.findAll().subscribe(
      (list: AccountSearchDto[]) => {
        this.accounts = AccountSearchViewModelHelper.loadModelList(list);
      }
    )
  }

  private findChartOfAccounts(): void {
    this.chartOfAccountService.findAll().subscribe(
      (list: ChartOfAccountDto[]) => {
        this.chartOfAccounts = ChartOfAccountViewModelHelper.loadModelList(list);
      }
    )
  }

  public clearAccount(): void {
    this.selectedAccount = "";
  }

  public clearChartOfAccount(): void {
    this.selectedChartOfAccount = "";
  }

  public onChangeChartOfAccount($event): void {
    $event.target.value = $event?.target?.value?.trim();
    this.selectedChartOfAccount = $event.target.value;
  }

  public clear(): void {
    this.codeFilter = null;
    this.selectedAccount = "";
    this.selectedChartOfAccount = "";
    this.search();
  }

  public search(): void {
    const selectedAccount: AccountSearchViewModel = AccountSearchViewModelHelper.getItemByName(this.accounts, this.selectedAccount);
    const selectedChartOfAccount: ChartOfAccountViewModel = ChartOfAccountViewModelHelper.getItemByName(this.chartOfAccounts, this.selectedChartOfAccount);
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        startDateFilter: this.startDateFilter,
        endDateFilter: this.endDateFilter,
        accountUidFilter: selectedAccount?.account?.uid,
        chartOfAccountUidFilter: selectedChartOfAccount?.chartOfAccount?.uid
      }
    );
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

}
