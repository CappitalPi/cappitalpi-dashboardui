import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { TransactionRepairDto } from '../../../../dto/transaction/transaction-repair.dto';
import { TransactionSearchDto } from '../../../../dto/transaction/transaction-search.dto';
import { TransactionDto } from '../../../../dto/transaction/transaction.dto';
import { TransactionBalance } from '../../../../enum/transaction-balance.enum';
import { TransactionCreationType } from '../../../../enum/transaction-creation-type.enum';
import { TransactionStatus } from '../../../../enum/transaction-status.enum';
import { TransactionService } from '../../../../service/transaction/transaction.service';
import { DateUtils } from '../../../../utils/date.utils';
import { EnumUtils } from '../../../../utils/enum-utils';
import { NumberUtils } from '../../../../utils/number-utils';
import { StringUtils } from '../../../../utils/string-utils';
import { TransactionRegisterComponent } from '../transaction-register/transaction-register.component';
import { TransactionTypeOptionComponent } from '../transaction-type-option/transaction-type-option.component';
import { TransferRegisterComponent } from '../transfer/transfer-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-transaction-search',
  templateUrl: './transaction-search.component.html',
  styleUrls: ['./transaction-search.component.scss']
})
export class TransactionSearchComponent implements OnInit {

  public isEditButtonDisabled : boolean = false;
  public disableRepairButton : boolean = false;
  public showBalance : boolean = false;
  public alertMessage : string;
  public warningMessage : string;
  public successMessage : string;
  public creditBalance: number;
  public debitBalance: number;
  public startDateFilter: string;
  public endDateFilter: string;
  public balance: number;
  public transactionPageResult : PageResultDto<TransactionSearchDto>;
  public codeFilter: number;
  public accountUidFilter: string;
  public chartOfAccountUidFilter: string;
  public itemsPerPage: number = 20;

  constructor(private modalService: BsModalService, 
              private transactionService: TransactionService,
              private translateService: TranslateService) {
    this.transactionPageResult = new PageResultDto<TransactionSearchDto>();
    this.transactionPageResult.totalElements = 0;
    this.creditBalance = NumberUtils.strToCurrency("0");
    this.debitBalance = NumberUtils.strToCurrency("0");
    this.balance = NumberUtils.strToCurrency("0");

  }

  ngOnInit(): void {
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.accountUidFilter = $event.accountUidFilter;
    this.chartOfAccountUidFilter = $event.chartOfAccountUidFilter;
    this.startDateFilter = $event.startDateFilter;
    this.endDateFilter = $event.endDateFilter;
    this.findList(FIRST_PAGE);
  }

  public findList(pageNumber: number, message: string = ""): void {
    this.transactionService.findHistory(pageNumber,
                                        this.itemsPerPage,
                                        DateUtils.strToDate(this.startDateFilter),
                                        DateUtils.strToDate(this.endDateFilter),
                                        this.codeFilter,
                                        this.accountUidFilter,
                                        this.chartOfAccountUidFilter).subscribe(
      (data: PageResultDto<TransactionSearchDto>) => {
        this.alertMessage = null;
        this.transactionPageResult  = data;
        this.creditBalance = data?.balances?.[TransactionBalance.CREDIT_BALANCE];
        this.debitBalance = data?.balances?.[TransactionBalance.DEBIT_BALANCE];
        this.balance = data?.balances?.[TransactionBalance.TOTAL];
        this.afterFilter(message);
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter(successMessage: string = "") {
    this.warningMessage = StringUtils.EMPTY;
    this.successMessage = successMessage;
    if (this.transactionPageResult && this.transactionPageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
    if (this.warningMessage?.length <= 0 && this.hasPendingTransaction()) {
      this.warningMessage = this.translateService.instant("TRANSACTION.MESSAGE.PENDING_TRANSACTION");
    }
  }

  public repair(transaction: TransactionDto): void {
    this.disableRepairButton = true;
    this.alertMessage = null;

    const repair: TransactionRepairDto = new TransactionRepairDto();
    repair.registerUid = transaction.registerUid;
    repair.uid = transaction.uid;
    this.transactionService.repair(repair).subscribe(
      (data: TransactionDto[]) => {
        this.disableRepairButton = false;
        const ids = [];
        data.forEach(element => {
          ids.push(element.code)
        });
        const message = this.translateService.instant('TRANSACTION.MESSAGE.REPAIRED_SUCCESS', {'code':ids.join(', ')});
        this.findList(this.transactionPageResult.currentPage, message);
      },
      error => {
        this.disableRepairButton = false;
        this.alertMessage = error;
        console.info(`Error repairing transaction ${transaction.code}: ${error}`);
      }
    );
  }

  public edit(transaction: TransactionSearchDto): void {
    try {
      this.isEditButtonDisabled = true;
      this.findAndOpenRegisterScreen(transaction, false);
    } finally {
      this.isEditButtonDisabled = false;
    }
  }

  public view(transaction: TransactionSearchDto): void {
    this.findAndOpenRegisterScreen(transaction, true);
  }
  
  public findAndOpenRegisterScreen(transaction: TransactionSearchDto, isViewMode: boolean) {
    try {
      this.isEditButtonDisabled = true;
      if (transaction) {
        this.transactionService.findByCode(transaction.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data, isViewMode);
            this.isEditButtonDisabled = false;
          },
          error => {
            this.alertMessage = error; 
            this.isEditButtonDisabled = false;
          }
          );
        }
      } catch {
        this.isEditButtonDisabled = false;
      }
  }

  public openTransferRegister() {
    const modalConfig: {} = { class: 'modal-xl', backdrop: 'static' };
    const registerModalRef = this.modalService.show(TransferRegisterComponent, Object.assign( {}, modalConfig ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });    
  }

  public openRegisterScreen(transaction: TransactionDto, isViewMode: boolean) {
    const initialState = { existingTransaction: transaction, isViewMode: isViewMode };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(TransactionRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public openTransactionTypeOption() {
    const modalConfig: {} = { class: 'modal-md modal-dialog-centered', backdrop: 'static' };
    const registerModalRef = this.modalService.show(TransactionTypeOptionComponent, Object.assign( {}, modalConfig, {} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
           this.findList(FIRST_PAGE);
        }
      });
  }

  public openRemoveDialog(transaction: TransactionSearchDto) {
    if (transaction) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':transaction.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':transaction.chartOfAccount.accountNumber+" - "+transaction.chartOfAccount.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(transaction);
          }
        });
    }
  }

  private remove(transaction: TransactionSearchDto) {
    this.transactionService.remove(transaction.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  private hasPendingTransaction(): boolean {
    if (this.transactionPageResult?.totalElements > 0) {
      for (var item of this.transactionPageResult.content) {
        if (item.status === EnumUtils.getNameFromValue(TransactionStatus, TransactionStatus.PENDING)) {
          return true;
        }
      }
    }
    return false;
  }

  public formatDescription(description: string): string{
    const max = 100;
    if (description?.length > max) {
      return StringUtils.formatLongTextMaxLength(description, max);
    }
    return description;
  }

  public dblclickOnTable(transaction: TransactionSearchDto): void {
    if (this.isPending(transaction) || this.isAuto(transaction)) {
      this.view(transaction);
    } else {
      this.edit(transaction);
    }
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

  public isPending(transaction: TransactionSearchDto): boolean {
    return transaction?.status == EnumUtils.getNameFromValue(TransactionStatus, TransactionStatus.PENDING);
  }

  public isAuto(transaction: TransactionSearchDto): boolean {
    return transaction?.creationType == EnumUtils.getNameFromValue(TransactionCreationType, TransactionCreationType.AUTO);
  }

  public isCredit(transaction: TransactionSearchDto): boolean {
    return transaction?.transactionType == 'CREDIT';
  }

  public showBalanceClick(): boolean {
    return this.showBalance = !this.showBalance;
  }

}
