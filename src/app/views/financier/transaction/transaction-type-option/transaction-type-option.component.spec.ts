import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionTypeOptionComponent } from './transaction-type-option.component';

describe('TransactionTypeOptionComponent', () => {
  let component: TransactionTypeOptionComponent;
  let fixture: ComponentFixture<TransactionTypeOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionTypeOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionTypeOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
