import { Component, OnInit } from '@angular/core';
import { EnumValueType } from 'enum-values/src/enumValues';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { TransactionRegisterComponent } from '../transaction-register/transaction-register.component';

@Component({
  selector: 'app-transaction-type-option',
  templateUrl: './transaction-type-option.component.html',
  styleUrls: ['./transaction-type-option.component.scss']
})
export class TransactionTypeOptionComponent implements OnInit {

  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, public modalService: BsModalService) {
  }
  
  ngOnInit(): void {
  }

  public close(): void {
    this.modalRef.hide();
  }

  public selectType(type: string) {
    const transactionType : { name: string, value: EnumValueType } = EnumUtils.getEnumTypeFromName(TransactionType, type);
    this.openRegisterScreen(transactionType);
  }

  public openRegisterScreen(transactionType: { name: string, value: EnumValueType }) {
    const initialState = { existingTransaction: null, selectedType: transactionType };
    const modalConfig: {} = { class: 'modal-lg modal-dialog-centered', backdrop: 'static' };
    const registerModalRef = this.modalService.show(TransactionRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.submitted = registerModalRef.content.submitted;
          this.close()
        }
      });
  }
}
  
  