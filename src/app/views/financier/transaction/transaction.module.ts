import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionRoutingModule } from './transaction-routing.module';
import { TransactionSearchByDayComponent } from './transaction-search-by-day/transaction-search-by-day.component';
import { TransactionSearchBarByDayComponent } from './transaction-search-by-day/transaction-search-bar-by-day.component';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TransactionRegisterComponent } from './transaction-register/transaction-register.component';
import { DirectivesModule } from '../../../directive/directives.module';
import { TransactionTypeOptionComponent } from './transaction-type-option/transaction-type-option.component';
import { DataListModule } from '../component/data-list/data-list.module';
import { TransactionSearchComponent } from './transaction-search/transaction-search.component';
import { TransactionSearchBarComponent } from './transaction-search/transaction-search-bar.component';
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { TransactionReportFilterComponent } from './transaction-report/transaction-report-filter.component';
import { GroupByChartOfAccountReportComponent } from './transaction-report/grouped-by-chart-of-account/group-by-chart-of-account-report.component';
import { TransactionByDateReportComponent } from './transaction-report/transaction-by-date/transaction-by-date-report.component';
import { TransferRegisterComponent } from './transfer/transfer-register.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    TransactionRoutingModule,
    ConfirmationDialogModule,
    DataListModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [ 
    TransactionSearchComponent,
    TransactionSearchBarComponent,
    TransactionSearchByDayComponent,
    TransactionSearchBarByDayComponent,
    TransactionReportComponent,
    TransactionReportFilterComponent,
    GroupByChartOfAccountReportComponent,
    TransactionByDateReportComponent,
    TransactionRegisterComponent,
    TransferRegisterComponent,
    TransactionTypeOptionComponent,
  ]
})
export class TransactionModule { }
