import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { AccountSearchDto } from '../../../../dto/account/account-search.dto';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { DataListViewModel } from '../../component/data-list/view-model/data-list-view-model';
import { AccountService } from '../../../../service/account/account.service';
import { AccountDataListViewHelper } from '../../account/view-model/account-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { ChartOfAccountDataListViewHelper } from '../../chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { DateUtils } from '../../../../utils/date.utils';
import { TranslateService } from '@ngx-translate/core';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { CurrencyPipe } from '@angular/common';
import { NumberUtils } from '../../../../utils/number-utils';
import { TransferRequestDto } from '../../../../dto/transaction/transfer-request.dto';
import { AccountDto } from '../../../../dto/account/account.dto';
import { TransferPersistenceContext } from '../../../../strategy/persistence/transaction/transfer-persistence-context.strategy';
import { TransactionType } from '../../../../enum/transaction-type.enum';

@Component({
  selector: 'app-transfer-register',
  templateUrl: './transfer-register.component.html',
  styleUrls: ['./transfer-register.component.scss']
})
export class TransferRegisterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public isViewMode: boolean;
  @Input() public existingTransfer: TransferRequestDto;

  public transfer: TransferRequestDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public transferDate : string;
  public amount : string;
  public selectedDebitAccount: DataListViewModel;
  public selectedCreditAccount: DataListViewModel;
  public selectedDebitChartOfAccount: DataListViewModel;
  public selectedCreditChartOfAccount: DataListViewModel;
  public debitAccountDataList: DataListDtoModel;
  public creditAccountDataList: DataListDtoModel;
  public debitChartOfAccountDataList: DataListDtoModel;
  public creditChartOfAccountDataList: DataListDtoModel;

  constructor(public modalRef: BsModalRef,
              private transferPersistenceContext: TransferPersistenceContext,
              private accountService: AccountService,
              private translateService: TranslateService,
              private chartOfAccountService: ChartOfAccountService,
              private accountDataListViewHelper: AccountDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper,
              private currencyPipe: CurrencyPipe) {
    this.transferDate = DateUtils.formatToShortDate(new Date());
    this.amount = NumberUtils.currencyToStr(0, currencyPipe);
    this.transfer = new TransferRequestDto();
    this.transfer.amount = 0;
    this.transfer.transferDate =  DateUtils.now();
    this.transfer.debitCashAccount = new AccountDto();
    this.transfer.creditCashAccount = new AccountDto();
    this.transfer.debitChartOfAccount = new ChartOfAccountDto();
    this.transfer.creditChartOfAccount = new ChartOfAccountDto();
    this.selectedDebitAccount = new DataListViewModel();
    this.selectedCreditAccount = new DataListViewModel();
    this.selectedDebitChartOfAccount = new DataListViewModel();
    this.selectedCreditChartOfAccount = new DataListViewModel();
  }
  
  ngOnInit(): void {
    if (this.existingTransfer) {
      this.transfer = this.existingTransfer;
      this.selectedDebitAccount.uid = this.existingTransfer.debitCashAccount.uid;
      this.selectedCreditAccount.uid = this.existingTransfer.creditCashAccount.uid;
      this.selectedDebitChartOfAccount.uid = this.existingTransfer.debitChartOfAccount.uid;
      this.selectedCreditChartOfAccount.uid = this.existingTransfer.creditChartOfAccount.uid;
      this.amount = NumberUtils.currencyToStr(this.existingTransfer.amount, this.currencyPipe);
      this.transferDate = DateUtils.formatToShortDate(this.transfer.transferDate);
    }
    this.findAccounts();
    this.findDebitChartOfAccounts();
    this.findCreditChartOfAccounts();
  }

  public findAccounts(): void{
    this.accountService.findAll().subscribe(
      (data: AccountSearchDto[]) => {
        this.debitAccountDataList = new DataListDtoModel();
        this.debitAccountDataList.list = this.accountDataListViewHelper.load(data);
        this.debitAccountDataList.selectedUid = this.selectedDebitAccount.uid;

        this.creditAccountDataList = new DataListDtoModel();
        this.creditAccountDataList.list = this.accountDataListViewHelper.load(data);
        this.creditAccountDataList.selectedUid = this.selectedCreditAccount.uid;
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  public findDebitChartOfAccounts(): void{
    this.chartOfAccountService.findByType(TransactionType.DEBIT.toString()).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.debitChartOfAccountDataList = new DataListDtoModel();
        this.debitChartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.debitChartOfAccountDataList.selectedUid = this.selectedDebitChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;
      }
    );
  }
  
  public findCreditChartOfAccounts(): void{
    this.chartOfAccountService.findByType(TransactionType.CREDIT.toString()).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.creditChartOfAccountDataList = new DataListDtoModel();
        this.creditChartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.creditChartOfAccountDataList.selectedUid = this.selectedCreditChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  public debitAccountSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.selectedDebitAccount = $event.selectedItems[0];
    }
  }

  public creditAccountSelected($event): void {
    if ($event.selectedItems?.length > 0) {
      this.selectedCreditAccount = $event.selectedItems[0];
    }
  }

  public onChangeAmount($event) {
    this.amount = $event.target.value;
  }

  public debitChartOfAccountSelected($event): void {
    this.transfer.debitChartOfAccount = new ChartOfAccountDto();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.transfer.debitChartOfAccount = $event.selectedItems[0].object;
      }
    }
  }
  
  public creditChartOfAccountSelected($event): void {
    this.transfer.creditChartOfAccount = new ChartOfAccountDto();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.transfer.creditChartOfAccount = $event.selectedItems[0].object;
      }
    }
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public persist(): void {
    try {
      this.disableSaveButton = true;
      this.submitted = false;
      this.alertMessage = null;
      if (!this.form.valid) {
        FormValidationUtils.markFieldsAsTouched(this.form);
        this.disableSaveButton = false;
        return;
      }
      if (DateUtils.isDateValid(this.transferDate)) {
        this.transfer.transferDate = DateUtils.strToDate(this.transferDate);
      } else {
        this.alertMessage = this.translateService.instant('APP.MESSAGE.INVALID_DATE');
        return;
      }
      this.transfer.amount = NumberUtils.strToCurrency(this.amount);
      this.transfer.debitCashAccount = this.selectedDebitAccount.object;
      this.transfer.creditCashAccount = this.selectedCreditAccount.object;
      const description = this.translateService.instant('TRANSACTION.REGISTER.TRANSFER.DEFAULT_DESCRIPTION') + " " + (this.transfer.description?.length > 0 ? this.transfer.description : "");
      this.transfer.description = description;
      this.transferPersistenceContext.context(PersistenceMode.CREATE).persist(this.transfer).subscribe(
        data => {
          console.debug(`Transferência criada com sucesso: ${data}`);
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
          console.error(`Erro na criação da transferência: ${error}`)
        }
        );
        this.submitted = true;
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }
  }
  
  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }
}
  
  