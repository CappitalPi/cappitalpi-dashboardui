import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionTypeRegisterComponent } from './inscription-type-register.component';

describe('InscriptionTypeRegisterComponent', () => {
  let component: InscriptionTypeRegisterComponent;
  let fixture: ComponentFixture<InscriptionTypeRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InscriptionTypeRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionTypeRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
