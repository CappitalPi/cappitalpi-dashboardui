import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { InscriptionTypeDto } from '../../../dto/inscription/inscription-type.dto';
import { InscriptionTypePersistenceContext } from '../../../strategy/persistence/inscription-type/inscription-type-persistence-context.strategy';
import { PersistenceMode } from '../../../strategy/persistence/persistence-mode.enum';
import { FormValidationUtils } from '../../../utils/form-validation.utils'

@Component({
  selector: 'app-inscription-type-register',
  templateUrl: './inscription-type-register.component.html',
  styleUrls: ['./inscription-type-register.component.scss']
})
export class InscriptionTypeRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingInscriptionType: InscriptionTypeDto;

  public inscriptionType: InscriptionTypeDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private inscriptionTypePersistenceContext: InscriptionTypePersistenceContext) {
    this.inscriptionType = new InscriptionTypeDto();
  }
  
  ngOnInit(): void {
    if (this.existingInscriptionType) {
      this.inscriptionType = this.existingInscriptionType;
    }
  }
  
  ngAfterViewInit(): void {
    setTimeout(()=>{
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public isNew(): boolean {
    return this.inscriptionType && !this.inscriptionType.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }
  
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    const mode = this.inscriptionType.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.inscriptionTypePersistenceContext.context(mode).persist(this.inscriptionType).subscribe(
      data => {
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
      }
      );
      this.submitted = true;
    }
    
    public close(): void {
      this.modalRef.content.submitted = this.submitted;
      this.modalRef.hide();
    }
  }
  