import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { InscriptionTypeSearchComponent } from './inscription-type-search/inscription-type-search.component';

const routes: Routes = [
  {
    path: '',
    component: InscriptionTypeSearchComponent,
    data: {
      title: 'APP.MENU.PERSON.INSCRIPTION_TYPES'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InscriptionTypeRoutingModule {
  
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
