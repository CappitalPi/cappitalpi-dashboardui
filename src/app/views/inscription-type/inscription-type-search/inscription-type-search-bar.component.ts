import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormValidationUtils } from '../../../utils/form-validation.utils';

@Component({
  selector: 'app-inscription-type-search-bar',
  templateUrl: './inscription-type-search-bar.component.html',
  styleUrls: ['./inscription-type-search.component.scss']
})
export class InscriptionTypeSearchBarComponent implements OnInit {

  public codeFilter: number;
  public nameFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, nameFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.nameFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        nameFilter: this.nameFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
