import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionTypeSearchComponent } from './inscription-type-search.component';

describe('InscriptionTypeSearchComponent', () => {
  let component: InscriptionTypeSearchComponent;
  let fixture: ComponentFixture<InscriptionTypeSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InscriptionTypeSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionTypeSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
