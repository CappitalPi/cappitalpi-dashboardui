import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../dto/page-result.dto';
import { InscriptionTypeDto } from '../../../dto/inscription/inscription-type.dto';
import { InscriptionTypeService } from '../../../service/inscription-type/inscription-type.service';
import { InscriptionTypeRegisterComponent } from '../inscription-type-register/inscription-type-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-inscription-type-search',
  templateUrl: './inscription-type-search.component.html',
  styleUrls: ['./inscription-type-search.component.scss']
})
export class InscriptionTypeSearchComponent implements OnInit {

  public alertMessage : string;
  public warningMessage : string;
  public itemsPerPage: number = 20;
  public inscriptionTypePageResult : PageResultDto<InscriptionTypeDto>;
  public codeFilter: number;
  public nameFilter: string;

  constructor(private modalService: BsModalService, 
              private inscriptionTypeService: InscriptionTypeService,
              private translateService: TranslateService) {
    this.inscriptionTypePageResult = new PageResultDto<InscriptionTypeDto>();
  }

  ngOnInit(): void {
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.nameFilter = $event.nameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.inscriptionTypeService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.nameFilter).subscribe(
      (data: PageResultDto<InscriptionTypeDto>) => {
        this.alertMessage = null;
        this.inscriptionTypePageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.inscriptionTypePageResult && this.inscriptionTypePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(inscriptionType: InscriptionTypeDto) {
    const initialState = { existingInscriptionType: inscriptionType };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(InscriptionTypeRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
    }

    public edit(inscriptionType: InscriptionTypeDto) {
      if (inscriptionType) {
        this.inscriptionTypeService.findByCode(inscriptionType.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data);
           },
          error => {
            this.alertMessage = error; 
          }
        );
      }
    }

  public openRemoveDialog(inscriptionType: InscriptionTypeDto) {
    if (inscriptionType) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':inscriptionType.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':inscriptionType.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(inscriptionType);
          }
        });
    }
  }

  private remove(inscriptionType: InscriptionTypeDto) {
    this.inscriptionTypeService.remove(inscriptionType.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

}
