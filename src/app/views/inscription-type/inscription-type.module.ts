import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InscriptionTypeRoutingModule } from './inscription-type-routing.module';
import { InscriptionTypeSearchComponent } from './inscription-type-search/inscription-type-search.component';
import { InscriptionTypeRegisterComponent } from './inscription-type-register/inscription-type-register.component';
import { InscriptionTypeSearchBarComponent } from './inscription-type-search/inscription-type-search-bar.component';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    InscriptionTypeRoutingModule,
    ConfirmationDialogModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [ 
    InscriptionTypeSearchComponent,
    InscriptionTypeRegisterComponent,
    InscriptionTypeSearchBarComponent
  ]
})
export class InscriptionTypeModule { }
