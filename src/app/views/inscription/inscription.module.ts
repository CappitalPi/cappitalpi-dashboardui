import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { InscriptionListComponent } from './list/inscription-list.component';
import { InscriptionRegisterComponent } from './register/inscription-register.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  declarations: [
    InscriptionListComponent,
    InscriptionRegisterComponent,
  ],
  exports: [
    InscriptionListComponent,
    InscriptionRegisterComponent
  ]
})
export class InscriptionModule { }
