import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { InscriptionViewModelHelper } from '../view-model/inscription-view-model.helper';
import { InscriptionRegisterComponent } from '../register/inscription-register.component';
import { InscriptionViewModel } from '../view-model/inscription-view-model';

@Component({
  selector: 'inscription-list',
  templateUrl: './inscription-list.component.html',
  styleUrls: ['./inscription-list.component.scss']
})
export class InscriptionListComponent implements OnInit {

  @Input()
  public inscriptionViewModelList : InscriptionViewModel[];
  @Output()
  paramsEmiter = new EventEmitter<{inscriptionViewModelList: InscriptionViewModel[]}>();

  constructor(private modalService: BsModalService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }
  
  public openRegisterScreen(inscription: InscriptionViewModel) {
    const initialState = { existingInscription: inscription };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(InscriptionRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          InscriptionViewModelHelper.addToModelList(registerModalRef.content.inscriptionViewModel, this.inscriptionViewModelList);
          this.refresh();
        }
      });
  }

  public openRemoveDialog(model: InscriptionViewModel) {
    const initialState = { 
      title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code': model.code}), 
      message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info': model.itemLabel}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.inscriptionViewModelList.splice(model.index, 1);
          InscriptionViewModelHelper.formatInscriptions(this.inscriptionViewModelList);
          this.refresh();
        }
      });
  }

  public refresh(): void {
    this.paramsEmiter.emit(
      {
        inscriptionViewModelList: this.inscriptionViewModelList
      }
    );
  }

  public async close() {
    this.modalService.hide();
  }

  public hasInscriptions(): boolean {
    return this.inscriptionViewModelList?.length > 0;
  }


}
