import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { InscriptionTypeDto } from '../../../dto/inscription/inscription-type.dto';
import { InscriptionDto } from '../../../dto/inscription/inscription.dto';
import { InscriptionViewModelHelper } from '../view-model/inscription-view-model.helper';
import { CustomerService } from '../../../service/customer/customer.service';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { InscriptionViewModel } from '../view-model/inscription-view-model';

@Component({
  selector: 'app-inscription-register',
  templateUrl: './inscription-register.component.html',
  styleUrls: ['./inscription-register.component.scss']
})
export class InscriptionRegisterComponent implements OnInit {

  @ViewChild('inscriptionForm')
  private form : NgForm;
  @Input()
  public existingInscription: InscriptionViewModel;

  public inscriptionViewModel: InscriptionViewModel;
  public selectedTypeItem: string;
  public inscriptionTypes: InscriptionTypeDto[];
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private customerService: CustomerService) {
    this.inscriptionTypes = [];
    this.inscriptionViewModel = new InscriptionViewModel();
    this.inscriptionViewModel.inscription = new InscriptionDto();
  }
  
  ngOnInit(): void {
    this.load();
    this.findInscriptionTypes();
  }
  
  private load(): void {
    if (this.existingInscription) {
      this.inscriptionViewModel = this.existingInscription;
    }
  }

  public onChangeType($event) {
    this.selectType($event.target.value);
  }

  private selectType(itemName: string): void {
    this.selectedTypeItem = null;
    this.inscriptionViewModel.inscription.type = null;
    const selectedItem = InscriptionViewModelHelper.findTypeByName(this.inscriptionTypes, itemName);
    if (selectedItem) {
      this.selectedTypeItem = selectedItem.name;
      this.inscriptionViewModel.inscription.type = selectedItem;
    }
  }
  
  public clearType(): void {
    this.selectedTypeItem = null;
  }

  public isNew(): boolean {
    return !this.inscriptionViewModel?.code;
  }

  public findInscriptionTypes() {
    this.customerService.findInscriptionTypes().subscribe(
      (list: InscriptionTypeDto[]) => {
        this.inscriptionTypes = list;
        list.forEach(element => {
          if (this.inscriptionViewModel?.inscription?.type?.code == element.code) {
            this.inscriptionViewModel.inscription.type = element;
            this.selectedTypeItem = element.name;
          }
        });
      }, error => {
        this.alertMessage = error;
      }
    )
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  private prepare(): void {
    this.selectType(this.selectedTypeItem);
  }

  public save(): void {
    this.submitted = false;
    this.disableSaveButton = true;
    this.prepare();
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.submitted = true;
    this.close();
  }
    
  public close(): void {
    this.modalRef.hide();
  }
}
  