import { InscriptionTypeDto } from "../../../dto/inscription/inscription-type.dto";
import { InscriptionDto } from "../../../dto/inscription/inscription.dto";
import { InscriptionViewModel } from "./inscription-view-model";
import { AppHelper } from "../../../helper/app.helper";

export class InscriptionViewModelHelper {


  public static getInscriptions(models: InscriptionViewModel[]) {
    const inscriptions: InscriptionDto[] = [];
    models?.forEach(model => {
      inscriptions.push(model.inscription);
    });
    return inscriptions;
  }

  public static loadModelList(inscriptionList: InscriptionDto[]): InscriptionViewModel[] {
    const inscriptionModelViewList: InscriptionViewModel[] = [];
    inscriptionList?.forEach(inscription => {
      const model: InscriptionViewModel = new InscriptionViewModel();
      model.inscription = inscription;
      inscriptionModelViewList.push(model);
    });
    this.formatInscriptions(inscriptionModelViewList);
    return inscriptionModelViewList;
  }

  public static addToModelList(inscriptionViewModel: InscriptionViewModel, inscriptionList: InscriptionViewModel[]): void {
    if (inscriptionList && inscriptionViewModel) {
      if (inscriptionViewModel.index >= 0) {
        inscriptionList[inscriptionViewModel.index] = inscriptionViewModel;
      } else {
        inscriptionList.push(inscriptionViewModel);
      }
      this.formatInscriptions(inscriptionList);
    }
  }

  public static formatInscriptions(inscriptions: InscriptionViewModel[]): void {
    if (inscriptions != null) {
      let index: number = 0;
      for (let inscriptionViewModel of inscriptions) {
        inscriptionViewModel.index = index++;
        this.formatInscription(inscriptionViewModel);
      }
    }
  }

  public static formatInscription(model: InscriptionViewModel): void {
    if (model == null || model.inscription == null) {
      return null;
    }
    let inscriptionDescription = model.inscription.type?.name + ` ${model.inscription.value}`;
    model.code = AppHelper.generateUniqueCode(model.index, model.code);
    model.itemLabel = inscriptionDescription;
  }

  public static findTypeByName(list: InscriptionTypeDto[], name: string) {
    if (list && name) {
      const itemByCode = list.find(x => x?.name === name);
      if (itemByCode) {
        return itemByCode;
      }
    }
    return null;
  }

}