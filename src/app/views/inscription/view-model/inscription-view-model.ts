import { InscriptionTypeDto } from "../../../dto/inscription/inscription-type.dto";
import { InscriptionDto } from "../../../dto/inscription/inscription.dto";

export class InscriptionViewModel {
    code: string;
    index: number;
    inscription: InscriptionDto;
    itemLabel: string;
}

