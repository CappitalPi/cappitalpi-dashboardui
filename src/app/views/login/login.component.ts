import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LoginRequestDto } from '../../dto/auth/login-request.dto';
import { AuthService } from '../../service/auth/auth-service';
import { ROUTE_DASHBOARD } from '../../service/navigation-constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent { 

  @ViewChild('form') form : NgForm;

  public loginRequest: LoginRequestDto;
  public submitted : boolean;
  public alertMessage : string;

  constructor(private router: Router, public authService: AuthService, public translateService: TranslateService) {
    this.loginRequest = new LoginRequestDto();
  }

  public login(): void {
    this.submitted = false;
    this.alertMessage = null;
    if (this.form.valid) {
      this.authService.login(this.loginRequest)
      .subscribe(
        () => {
          this.router.navigate([`/${ROUTE_DASHBOARD}`]);
      },
      err => {
        this.alertMessage = this.translateService.instant('APP.LOGIN.INVALID_USER_MSG');
        console.debug('Error logging in to system' + JSON.stringify(err));
      });
    }
    this.submitted = true;
  } 
  
}
