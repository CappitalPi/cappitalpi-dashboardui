import { Injectable } from "@angular/core";
import { PersonSearchDetailDto } from "../../../dto/person/person-search-detail.dto";
import { DataListLazyViewModel } from "../../base/component/data-list-lazy/view-model/data-list-lazy-view-model";
import { DataListLazyViewModelHelper } from "../../base/component/data-list-lazy/view-model/data-list-lazy-view-model.helper";

@Injectable({
    providedIn: 'root'
  })
export class PersonDataListViewHelper implements DataListLazyViewModelHelper {

    public load(list: PersonSearchDetailDto[]): DataListLazyViewModel[] {
        const result: DataListLazyViewModel[] = [];
        if (list?.length > 0) {
            let index: number = 0;
            list.forEach(person => {
                const data: DataListLazyViewModel = new DataListLazyViewModel();
                data.index = index++;
                data.object = person;
                data.uid = person.uid;
                data.code = person.code;
                data.description = person.name;
                result.push(data);
            });
        } 
        return result;
    }

}