import { ChartOfAccountDto } from "../../../../dto/chart-of-account/chart-of-account.dto";
import { StringUtils } from "../../../../utils/string-utils";
import { ChartOfAccountViewModel } from "./chart-of-account-view-model";

const SEPARATOR = "-";
export class ChartOfAccountViewModelHelper {

  public static getItemByName(list: any[], targetName: string) {
    if (list && targetName?.length > 0) {
      const item = targetName.split(SEPARATOR);
      return list.find(x => x?.chartOfAccount.accountNumber === item[0].trim());
    }
    return null;
  }

  public static loadModelList(list: ChartOfAccountDto[]): ChartOfAccountViewModel[] {
    const result: ChartOfAccountViewModel[] = [];
    if (list?.length > 0) {
        let index: number = 0;
        list.forEach(account => {
            const data: ChartOfAccountViewModel = new ChartOfAccountViewModel();
            data.index = index++;
            data.chartOfAccount = account;
            data.code = account.code;
            data.label = this.getTab(account.accountNumber) +""+ this.formatLabel(account);
            result.push(data);
        });
    } 
    return result;
  }

  public static getTab(accountNumber: string) {
      if (accountNumber.length > 1) {
          let tabSpace = "";
          for (let index = 0; index < accountNumber.length; index++) {
              tabSpace = tabSpace + '\xa0\xa0\xa0';
          }
          return tabSpace;
      }
      return "";
  }

  public static formatLabel(account: ChartOfAccountDto) {
    if (account) {
      return account.accountNumber+" "+SEPARATOR+" "+account.name;
    }
    return StringUtils.EMPTY;
  }
}