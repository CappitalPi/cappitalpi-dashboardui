import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PhoneViewModelHelper } from '../view-model/phone-view-model.helper';
import { AddressViewModel } from '../../address/view-model/address-view-model';
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { PhoneRegisterComponent } from '../register/phone-register.component';
import { PhoneViewModel } from '../view-model/phone-view-model';

@Component({
  selector: 'phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.scss']
})
export class PhoneListComponent implements OnInit {

  @Input()
  public phoneViewModelList : PhoneViewModel[];
  @Input()
  public contactViewModelList: ContactViewModel[];
  @Input()
  public addressViewModelList: AddressViewModel[];
  @Input()
  public personType : string;
  @Output()
  paramsEmiter = new EventEmitter<{phoneViewModelList: PhoneViewModel[]}>();

  constructor(private modalService: BsModalService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }
  
  public openRegisterScreen(phone: PhoneViewModel) {
    const initialState = { existingPhone: phone, contactViewModelList: this.contactViewModelList, addressViewModelList: this.addressViewModelList, personType: this.personType };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(PhoneRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          PhoneViewModelHelper.addToModelList(registerModalRef.content.phoneViewModel, this.phoneViewModelList);
          this.refresh();
        }
      });
  }

  public openRemoveDialog(model: PhoneViewModel) {
    const initialState = { 
      title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code': model.code}), 
      message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info': model.phoneNumber}), 
      okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
      closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
    };
    const modalConfig: {} = { class: 'modal-md, modal-danger' };
    const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
    removeModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (removeModalRef.content.isOk) {
          this.phoneViewModelList.splice(model.index, 1);
          PhoneViewModelHelper.formatPhones(this.phoneViewModelList);
          this.refresh();
        }
      });
  }

  public refresh(): void {
    this.paramsEmiter.emit(
      {
        phoneViewModelList: this.phoneViewModelList
      }
    );
  }

  public isEmptyList() {
    return this.phoneViewModelList?.length <= 0;
  }

  public async close() {
    this.modalService.hide();
  }

}
