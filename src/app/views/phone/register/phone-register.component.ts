import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ContactDto } from '../../../dto/contact/contact.dto';
import { PhoneTypeDto } from '../../../dto/phone/phone-type.dto';
import { PhoneDto } from '../../../dto/phone/phone.dto';
import { AddressViewModelHelper } from '../../address/view-model/address-view-model.helper';
import { ContactViewModelHelper } from '../../contact/view-model/contact-view-model.helper';
import { PhoneService } from '../../../service/phone/phone.service';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { AddressViewModel } from '../../address/view-model/address-view-model';
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { PhoneViewModel } from '../view-model/phone-view-model';
import { PersonType } from '../../../dto/person/person-type.enum';

@Component({
  selector: 'app-phone-register',
  templateUrl: './phone-register.component.html',
  styleUrls: ['./phone-register.component.scss']
})
export class PhoneRegisterComponent implements OnInit {

  @ViewChild('phoneForm')
  private form : NgForm;
  @Input()
  public existingPhone: PhoneViewModel;
  @Input()
  public contactViewModelList: ContactViewModel[];
  @Input()
  public addressViewModelList: AddressViewModel[];
  @Input()
  public personType: string;

  public phoneViewModel: PhoneViewModel;
  public selectedContactItem: string;
  public selectedContactName: string;
  public selectedContactRole: string;
  public selectedAddressLine: string;
  public phoneTypes: PhoneTypeDto[];
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public isContactPrepared: boolean = false;

  constructor(public modalRef: BsModalRef, private phoneService: PhoneService) {
    this.phoneViewModel = new PhoneViewModel();
    this.phoneViewModel.phone = new PhoneDto();
  }
  
  ngOnInit(): void {
    this.phoneTypes = [];
    this.load();
    this.findPhoneTypes();
  }
  
  private load(): void {
    if (this.existingPhone) {
      this.phoneViewModel = this.existingPhone;
      if (this.existingPhone.contactModel) {
        this.selectContact(this.existingPhone.contactModel?.itemLabel);
      }
      if (this.existingPhone.addressModel) {
        this.selectAddress(this.existingPhone.addressModel?.itemLabel);
      }
    }
    if (!this.contactViewModelList) {
      this.contactViewModelList = [];
    }
  }
  
  public onChangeContact($event) {
    this.selectContact($event.target.value);
  }

  public onChangeAddress($event) {
    this.selectAddress($event.target.value);
  }

  private selectContact(itemValue: string): void {
    this.selectedContactItem = null;
    const selectedItem : ContactViewModel= this.getContact(itemValue);
    if (selectedItem && selectedItem.contact) {
      this.selectedContactItem = itemValue;
      this.selectedContactName = selectedItem.contact.name;
      this.selectedContactRole = selectedItem.contact.role;
    } else {
      if (!this.isContactPrepared) {
        this.clearSelectedContact();
      }
    }
  }

  private getContact(itemValue: string): ContactViewModel {
    if (this.isContactPrepared) {
      return ContactViewModelHelper.findItemValue(this.contactViewModelList, itemValue, this.selectedContactRole);
    }
    return ContactViewModelHelper.findItemValue(this.contactViewModelList, itemValue, null);
  }
  
  private selectAddress(itemValue: string): void {
    this.selectedAddressLine = null;
    this.phoneViewModel.addressModel = null;
    const selectedItem: AddressViewModel = AddressViewModelHelper.findItemByValue(this.addressViewModelList, itemValue);
    if (selectedItem && selectedItem.address) {
      this.selectedAddressLine = selectedItem.addressLine;
      this.phoneViewModel.addressModel = selectedItem;
    }
  }

  public clearAddress(): void {
    this.selectedAddressLine = null;
    this.phoneViewModel.addressModel = null;
  }

  public addContact() {
    //TODO: Add question.
    this.clearSelectedContact();
    this.isContactPrepared = this.isContactPrepared ? false : true;
  }

  private clearSelectedContact() {
    this.phoneViewModel.contactModel = null;
    this.selectedContactName = null;
    this.selectedContactRole = null;
    this.selectedContactItem = null;
  }
  
  public isNew(): boolean {
    return !this.phoneViewModel?.code;
  }

  public findPhoneTypes() {
    this.phoneService.findAllTypes().subscribe(
      (list: PhoneTypeDto[]) => {
        this.phoneTypes = list;
        list.forEach(element => {
          if (this.phoneViewModel?.phone?.type?.code == element.code) {
            this.phoneViewModel.phone.type = element;
          }
        });
      }, error => {
        this.alertMessage = error;
      }
    )
  }

  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public isLegalPerson(): boolean {
    return this.personType === PersonType.LEGAL_PERSON;
  }

  private prepare(): void {
    this.phoneViewModel.contactModel = null;
    const selectedItem : ContactViewModel= this.getContact(this.selectedContactItem);
    if (selectedItem) {
      this.phoneViewModel.contactModel = selectedItem;
    } else {
      if (this.selectedContactName?.length > 0) {
        this.phoneViewModel.contactModel = new ContactViewModel();
        this.phoneViewModel.contactModel.contact = new ContactDto();
        this.phoneViewModel.contactModel.contact.name = this.selectedContactName;
        this.phoneViewModel.contactModel.contact.role = this.selectedContactRole;
      }
    }
  }
  //TODO: Corrigir a desassociação de endereço.
  public save(): void {
    this.submitted = false;
    this.disableSaveButton = true;
    this.prepare();
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    this.submitted = true;
    this.close();
  }
    
  public close(): void {
    this.modalRef.hide();
  }
}
  