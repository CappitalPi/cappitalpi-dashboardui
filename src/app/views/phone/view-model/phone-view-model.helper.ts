import { PhoneDto } from "../../../dto/phone/phone.dto";
import { AddressViewModel } from "../../address/view-model/address-view-model";
import { ContactViewModel } from "../../contact/view-model/contact-view-model";
import { PhoneViewModel } from "./phone-view-model";
import { AddressViewModelHelper } from "../../address/view-model/address-view-model.helper";
import { AppHelper } from "../../../helper/app.helper";
import { ContactViewModelHelper } from "../../contact/view-model/contact-view-model.helper";

export class PhoneViewModelHelper {


  public static getItemByName(list: any[], targetName: string) {
    if (list) {
      return list.find(x => x?.name.toUpperCase() === targetName.toUpperCase());
    }
    return null;
  }

  public static getPhones(models: PhoneViewModel[]) {
    const phones: PhoneDto[] = [];
    models?.forEach(model => {
      if (model.contactModel?.contact) {
        model.phone.contact = model.contactModel.contact;
      } else {
        model.phone.contact = null;
      }
      if (model.addressModel?.address) {
        model.phone.address = model.addressModel.address;
      } else {
        model.phone.address = null;
      }
      phones.push(model.phone);
    });
    return phones;
  }

  public static refreshList(phoneViewModel: PhoneViewModel[], contactViewModelList: ContactViewModel[], addressViewModelList: AddressViewModel[]): void {
    phoneViewModel?.forEach(phone => {
      if (phone.contactModel) {
        const contactModel: ContactViewModel = ContactViewModelHelper.findItemValue(contactViewModelList, phone.contactModel.code, null);
        phone.contactModel = contactModel;
      }
      if (phone.addressModel) {
        const addressModel: AddressViewModel = AddressViewModelHelper.findItemByValue(addressViewModelList, phone.addressModel.itemLabel);
        phone.addressModel = addressModel;
      }
    });
    this.formatPhones(phoneViewModel);
  }

  public static loadModelList(phoneList: PhoneDto[], contactViewModelList: ContactViewModel[], addressViewModelList: AddressViewModel[]): PhoneViewModel[] {
    const phoneModelViewList: PhoneViewModel[] = [];
    phoneList?.forEach(phone => {
      const model: PhoneViewModel = new PhoneViewModel();
      model.phone = phone;
      if (phone.contact) {
        const contactModel: ContactViewModel = ContactViewModelHelper.findItemByCode(contactViewModelList, phone.contact.code);
        model.contactModel = contactModel;
      }
      if (phone.address) {
        const addressModel: AddressViewModel = AddressViewModelHelper.findItemByCode(addressViewModelList, phone.address.code);
        model.addressModel = addressModel;
      }
      phoneModelViewList.push(model);
    });
    this.formatPhones(phoneModelViewList);
    return phoneModelViewList;
  }

  public static addToModelList(phoneViewModel: PhoneViewModel, phoneList: PhoneViewModel[]): void {
    if (phoneList && phoneViewModel) {
      if (phoneViewModel.index >= 0) {
        phoneList[phoneViewModel.index] = phoneViewModel;
      } else {
        phoneList.push(phoneViewModel);
      }
      this.formatPhones(phoneList);
    }
  }

  public static formatPhones(phones: PhoneViewModel[]): void {
    if (phones != null) {
      let index: number = 0;
      for (let phoneViewModel of phones) {
        phoneViewModel.index = index++;
        this.formatPhone(phoneViewModel);
      }
    }
  }

  public static formatPhone(model: PhoneViewModel): void {
    if (model == null || model.phone == null) {
      return null;
    }
    model.phoneNumber = "";
    if (model.phone.areaCode) {
      model.phoneNumber += `(${model.phone.areaCode})`;
    }
    if(model.phone.phoneNumber) {
      model.phoneNumber += ` ${model.phone.phoneNumber}`;
    }
    if(model.phone.extension) {
      model.phoneNumber += ` / ${model.phone.extension}`;
    }
    if (model.contactModel && model.contactModel.contact) {
      model.phoneNumber += ` - ${model.contactModel.contact.name}`
    }
    model.code = AppHelper.generateUniqueCode(model.index, model.code);
  }

}