import { PhoneDto } from "../../../dto/phone/phone.dto";
import { AddressViewModel } from "../../address/view-model/address-view-model";
import { ContactViewModel } from "../../contact/view-model/contact-view-model";

export class PhoneViewModel {
    code: string;
    index: number;
    contactModel: ContactViewModel;
    addressModel: AddressViewModel;
    phone: PhoneDto;
    phoneNumber: string;
}

