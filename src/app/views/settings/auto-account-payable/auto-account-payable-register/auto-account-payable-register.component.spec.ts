import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AutoAccountPayableRegisterComponent } from './auto-account-payable-register.component';

describe('AutoAccountPayableRegisterComponent', () => {
  let component: AutoAccountPayableRegisterComponent;
  let fixture: ComponentFixture<AutoAccountPayableRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoAccountPayableRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAccountPayableRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
