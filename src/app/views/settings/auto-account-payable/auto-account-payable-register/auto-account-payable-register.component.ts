import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { DataListViewModel } from '../../component/data-list/view-model/data-list-view-model';
import { DateUtils } from '../../../../utils/date.utils';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { CurrencyPipe } from '@angular/common';
import { NumberUtils } from '../../../../utils/number-utils';
import { DataListLazyDtoModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-dto-model';
import { PersonService } from '../../../../service/person/person.service';
import { PersonSearchDetailDto } from '../../../../dto/person/person-search-detail.dto';
import { DataListLazyViewModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-view-model';
import { PersonDataListViewHelper } from '../../../person/view-model/person-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { StringUtils } from '../../../../utils/string-utils';
import { ChartOfAccountDataListViewHelper } from '../../../financier/chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { AutoAccountPayRecRequestDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { AutoAccountPayRecDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec.dto';
import { AutoAccountPayRecSearchDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-search.dto';
import { AutoAccountPayablePersistenceContext } from '../../../../strategy/persistence/account-payable/auto-account-payable-persistence-context.strategy';
import { FrequencyType } from '../../../../dto/account-pay-rec/frequency-type.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { RepeatType } from '../../../../dto/account-pay-rec/repeat-type.enum';
import { CostType } from '../../../../dto/account-pay-rec/cost-type.enum';

@Component({
  selector: 'app-auto-account-payable-register',
  templateUrl: './auto-account-payable-register.component.html',
  styleUrls: ['./auto-account-payable-register.component.scss']
})
export class AutoAccountPayableRegisterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public isViewMode: boolean;
  @Input() public existingAccountPayable: AutoAccountPayRecDto;
  @Input() public accountPayableSearch: AutoAccountPayRecSearchDto;

  public accountPayable: AutoAccountPayRecRequestDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public frequencyStartDate : string;
  public frequencyEndDate : string;
  public latestDate : string;
  public amount : string;
  public discount : string;
  public interest : string;
  public total : string;
  public amountPaid : string;
  public amountToPay : string;
  public frequencyTypes: FrequencyType[];
  public repeatTypes: RepeatType[];
  public costTypes: CostType[];
  public selectedChartOfAccount: DataListViewModel;
  public selectedPerson: DataListLazyViewModel;
  public accountDataList: DataListDtoModel;
  public chartOfAccountDataList: DataListDtoModel;
  public personDataList: DataListLazyDtoModel;

  constructor(public modalRef: BsModalRef,
              private currencyPipe: CurrencyPipe,
              private accountPayablePersistenceContext: AutoAccountPayablePersistenceContext,
              private personService: PersonService,
              private chartOfAccountService: ChartOfAccountService,
              private personDataListLazyViewHelper: PersonDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper) {
    this.frequencyStartDate = DateUtils.formatToShortDate(new Date());
    this.accountPayable = new AutoAccountPayRecRequestDto();
    this.accountPayable.amount = 0;
    this.accountPayable.discount = 0;
    this.accountPayable.interest = 0;
    this.amount = NumberUtils.currencyToStr(this.accountPayable.amount, this.currencyPipe);
    this.discount = NumberUtils.currencyToStr(this.accountPayable.discount, this.currencyPipe);
    this.interest = NumberUtils.currencyToStr(this.accountPayable.interest, this.currencyPipe);
    this.calcAmount();
    this.selectedChartOfAccount = new DataListViewModel();
    this.selectedPerson = new DataListLazyViewModel();
  }
  
  ngOnInit(): void {
    if (this.existingAccountPayable) {
      this.accountPayable = new AutoAccountPayRecRequestDto();
      this.accountPayable.uid = this.existingAccountPayable.uid;
      this.accountPayable.code = this.existingAccountPayable.code;
      this.accountPayable.registerUid = this.existingAccountPayable.registerUid;
      this.accountPayable.appAccountUid = this.existingAccountPayable.appAccountUid;
      this.accountPayable.personUid = this.existingAccountPayable.personUid;
      this.accountPayable.chartOfAccountUid = this.existingAccountPayable.chartOfAccountUid;
      this.accountPayable.description = this.existingAccountPayable.description;
      this.accountPayable.documentNumber = this.existingAccountPayable.documentNumber;
      this.accountPayable.frequencyType = this.existingAccountPayable.frequencyType;
      this.accountPayable.repeatType = this.existingAccountPayable.repeatType;
      this.accountPayable.costType = this.existingAccountPayable.costType;
      this.accountPayable.amount = this.existingAccountPayable.amount;
      this.accountPayable.discount = this.existingAccountPayable.discount;
      this.accountPayable.interest = this.existingAccountPayable.interest;
      this.accountPayable.frequencyStartDate = this.existingAccountPayable.frequencyStartDate;
      this.accountPayable.frequencyEndDate = this.existingAccountPayable.frequencyEndDate;
      this.accountPayable.frequencyAmount = this.existingAccountPayable.frequencyAmount;
      this.accountPayable.sunday = this.existingAccountPayable.sunday;
      this.accountPayable.monday = this.existingAccountPayable.monday;
      this.accountPayable.tuesday = this.existingAccountPayable.tuesday;
      this.accountPayable.wednesday = this.existingAccountPayable.wednesday;
      this.accountPayable.thursday = this.existingAccountPayable.thursday;
      this.accountPayable.friday = this.existingAccountPayable.friday;
      this.accountPayable.saturday = this.existingAccountPayable.saturday;
      this.accountPayable.skipWeekend = this.existingAccountPayable.skipWeekend;
      this.accountPayable.disabled = this.existingAccountPayable.disabled;
      this.selectedPerson.uid = this.existingAccountPayable.personUid;
      this.selectedChartOfAccount.uid = this.existingAccountPayable.chartOfAccountUid;
      this.frequencyStartDate = DateUtils.formatToShortDate(this.existingAccountPayable.frequencyStartDate);
      this.frequencyEndDate = DateUtils.formatToShortDate(this.existingAccountPayable.frequencyEndDate);
      this.latestDate = DateUtils.formatToShortDate(this.existingAccountPayable.latestDate);
      this.amount = NumberUtils.currencyToStr(this.existingAccountPayable.amount, this.currencyPipe);
      this.discount = NumberUtils.currencyToStr(this.existingAccountPayable.discount, this.currencyPipe);
      this.interest = NumberUtils.currencyToStr(this.existingAccountPayable.interest, this.currencyPipe);
      this.calcAmount();
      const selectedPerson : PersonSearchDetailDto[] = [this.accountPayableSearch.person]
      this.loadPerson(selectedPerson);
    }
    this.findFrequencyTypes();
    this.findRepeatTypes();
    this.findCostTypes();
    this.findChartOfAccounts();
  }

  public findFrequencyTypes(): void{
    if (!this.frequencyTypes) {
      this.frequencyTypes = EnumUtils.getList(FrequencyType, false);
    }
  }
  
  public findRepeatTypes(): void{
    if (!this.repeatTypes) {
      this.repeatTypes = EnumUtils.getList(RepeatType, false);
    }
  }
  
  public findCostTypes(): void{
    if (!this.costTypes) {
      this.costTypes = EnumUtils.getList(CostType, false);
    }
  }
  
  public findChartOfAccounts(): void{
    this.chartOfAccountService.findByType(TransactionType.DEBIT).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountDataList = new DataListDtoModel();
        this.chartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.chartOfAccountDataList.selectedUid = this.selectedChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  public chartOfAccountSelected($event): void {
    this.selectedChartOfAccount = new DataListViewModel();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.selectedChartOfAccount = $event.selectedItems[0];
      }
    }
  }

  public personSelected($event): void {
    this.selectedPerson = $event.selectedItem;
  }

  public filterPerson($event) {
    this.selectedPerson = new DataListLazyViewModel();
    this.personService.filter($event.searchTerm).subscribe(
      (data: PersonSearchDetailDto[]) => {
        this.loadPerson(data);
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  private loadPerson(data: PersonSearchDetailDto[]) {
    this.personDataList = new DataListLazyDtoModel();
    this.personDataList.list = this.personDataListLazyViewHelper.load(data);
    this.personDataList.selectedUid = this.selectedPerson.uid;
  }

  public formatNotes(notes: string): string{
    return StringUtils.formatLongText(notes);
  }

  /**
   * Método necessário pois existe um bug na diretiva ao incluir aos trilhões.
   * Por algum motivo, a aplicação está aceitando um dígito a mais.
   * TODO: Tentar corrigir a diretiva ou criar outra.
   * @param $event 
   */
  public onChangeAmount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.amount = $event.target.value;
    this.calcAmount();
  }

  public onChangeDiscount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.discount = $event.target.value;
    this.calcAmount();
  }

  public onChangeInterest($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.interest = $event.target.value;
    this.calcAmount();
  }

  public calcAmount(): void {
    const total : number = NumberUtils.strToCurrency(this.amount) - NumberUtils.strToCurrency(this.discount) + NumberUtils.strToCurrency(this.interest);
    this.total = NumberUtils.currencyToStr(total, this.currencyPipe);

    const amountToPay : number = NumberUtils.strToCurrency(this.total) - NumberUtils.strToCurrency(this.amountPaid);
    this.amountToPay = NumberUtils.currencyToStr(amountToPay, this.currencyPipe);
  }

  public isNew(): boolean {
    return this.accountPayable && !this.accountPayable.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public persist(): void {
    try {
      this.disableSaveButton = true;
      this.submitted = false;
      this.alertMessage = null;
      if (!this.form.valid) {
        FormValidationUtils.markFieldsAsTouched(this.form);
        this.disableSaveButton = false;
        return;
      }
      this.accountPayable.personUid = this.selectedPerson.uid;
      this.accountPayable.chartOfAccountUid = this.selectedChartOfAccount.uid;
      this.accountPayable.frequencyStartDate = DateUtils.strToDate(this.frequencyStartDate);
      this.accountPayable.frequencyEndDate = DateUtils.strToDate(this.frequencyEndDate);
      this.accountPayable.amount = NumberUtils.strToCurrency(this.amount);
      this.accountPayable.discount = NumberUtils.strToCurrency(this.discount);
      this.accountPayable.interest = NumberUtils.strToCurrency(this.interest);
      this.accountPayable.frequencyAmount = this.accountPayable.frequencyAmount ? Number(this.accountPayable.frequencyAmount) : null;
      const mode = this.accountPayable.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
      this.accountPayablePersistenceContext.context(mode).persist(this.accountPayable).subscribe(
        data => {
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
        }
      );
        this.submitted = true;
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }  
  }
  
  public isUpdate(): boolean {
    return this.accountPayable?.uid?.length > 0
  }

  public showFrequencyAmount(): boolean {
    if (this.accountPayable?.frequencyType) {
      return RepeatType.BY_AMOUNT_OF_DAYS == this.accountPayable.repeatType;
    }
    return false;
  }

  public showWeekDays(): boolean {
    return FrequencyType.DAY == this.accountPayable?.frequencyType && RepeatType.BY_DUE_DATE == this.accountPayable.repeatType;
  }

  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }

}