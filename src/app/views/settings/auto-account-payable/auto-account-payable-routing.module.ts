import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AutoAccountPayableSearchComponent } from './auto-account-payable-search/auto-account-payable-search.component';

const routes: Routes = [
  {
    path: '',
    component: AutoAccountPayableSearchComponent,
    data: {
      title: 'APP.MENU.SETTINGS.ACCOUNTS-PAYABLE'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutoAccountPayableRoutingModule {

  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
