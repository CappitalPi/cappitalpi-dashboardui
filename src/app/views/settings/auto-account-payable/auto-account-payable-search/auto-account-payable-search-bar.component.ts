import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { AccountService } from '../../../../service/account/account.service';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { FormValidationUtils } from '../../../../utils/form-validation.utils';
import { StringUtils } from '../../../../utils/string-utils';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { ChartOfAccountViewModel } from '../../../financier/chart-of-account/view-model/chart-of-account-view-model';
import { ChartOfAccountViewModelHelper } from '../../../financier/chart-of-account/view-model/chart-of-account-view-model.helper';

@Component({
  selector: 'app-auto-account-payable-search-bar',
  templateUrl: './auto-account-payable-search-bar.component.html',
  styleUrls: ['./auto-account-payable-search.component.scss']
})
export class AutoAccountPayableSearchBarComponent implements OnInit {

  public codeFilter: number;
  public personSearchFilter: string;
  public documentNumberFilter: string
  public selectedChartOfAccount: string;
  public chartOfAccounts: ChartOfAccountViewModel[];
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<
  { 
    codeFilter: number;
    personSearchFilter: string;
    chartOfAccountUidFilter: string;
    documentNumberFilter: string;
  }>();

  constructor(public accountService: AccountService, public chartOfAccountService: ChartOfAccountService) {
  }

  ngOnInit(): void {
    this.chartOfAccounts = [];
    this.findChartOfAccounts();
    this.search();
  }

  private findChartOfAccounts(): void {
    this.chartOfAccountService.findByType(TransactionType.DEBIT).subscribe(
      (list: ChartOfAccountDto[]) => {
        this.chartOfAccounts = ChartOfAccountViewModelHelper.loadModelList(list);
      }
    )
  }

  public clearChartOfAccount(): void {
    this.selectedChartOfAccount = StringUtils.EMPTY;;
  }

  public onChangeChartOfAccount($event): void {
    $event.target.value = $event?.target?.value?.trim();
    this.selectedChartOfAccount = $event.target.value;
  }

  public clear(): void {
    this.codeFilter = null;
    this.selectedChartOfAccount = StringUtils.EMPTY;
    this.personSearchFilter = StringUtils.EMPTY;
    this.documentNumberFilter = StringUtils.EMPTY;
    this.search();
  }

  public search(): void {
    const selectedChartOfAccount: ChartOfAccountViewModel = ChartOfAccountViewModelHelper.getItemByName(this.chartOfAccounts, this.selectedChartOfAccount);
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        personSearchFilter: this.personSearchFilter,
        chartOfAccountUidFilter: selectedChartOfAccount?.chartOfAccount?.uid,
        documentNumberFilter: this.documentNumberFilter
      }
    );
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

}
