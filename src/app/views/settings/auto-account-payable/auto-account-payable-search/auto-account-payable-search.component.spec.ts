import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AutoAccountPayableSearchComponent } from './auto-account-payable-search.component';


describe('AutoAccountPayableSearchComponent', () => {
  let component: AutoAccountPayableSearchComponent;
  let fixture: ComponentFixture<AutoAccountPayableSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoAccountPayableSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAccountPayableSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
