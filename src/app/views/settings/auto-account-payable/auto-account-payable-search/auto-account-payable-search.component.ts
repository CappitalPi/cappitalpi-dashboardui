import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { NumberUtils } from '../../../../utils/number-utils';
import { StringUtils } from '../../../../utils/string-utils';
import { AutoAccountPayableRegisterComponent } from '../auto-account-payable-register/auto-account-payable-register.component';
import { AutoAccountPayRecSearchDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-search.dto';
import { AutoAccountPayableService } from '../../../../service/account-payable/auto-account-payable.service';
import { AutoAccountPayRecDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec.dto';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-auto-account-payable-search',
  templateUrl: './auto-account-payable-search.component.html',
  styleUrls: ['./auto-account-payable-search.component.scss']
})
export class AutoAccountPayableSearchComponent implements OnInit {

  public isEditButtonDisabled : boolean = false;
  public alertMessage : string;
  public warningMessage : string;
  public successMessage : string;
  public accountPayablePageResult : PageResultDto<AutoAccountPayRecSearchDto>;
  public codeFilter: number;
  public personSearchFilter: string;
  public chartOfAccountUidFilter: string;
  public documentNumberFilter: string
  public itemsPerPage: number = 20;
  public selectedAccount: AutoAccountPayRecSearchDto;
  public tooltipAmountDetail: string;
  public tooltipSupplierDetail: string;

  constructor(private modalService: BsModalService,
              private currencyPipe: CurrencyPipe,
              private accountPayableService: AutoAccountPayableService,
              private translateService: TranslateService,
              private sanitizer: DomSanitizer) {
    this.accountPayablePageResult = new PageResultDto<AutoAccountPayRecSearchDto>();
    this.accountPayablePageResult.totalElements = 0;
    this.selectedAccount = new AutoAccountPayRecSearchDto();
  }

  ngOnInit(): void {
  }

  
  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.codeFilter = $event.codeFilter;
    this.personSearchFilter = $event.personSearchFilter;
    this.chartOfAccountUidFilter = $event.chartOfAccountUidFilter;
    this.documentNumberFilter = $event.documentNumberFilter;
    this.findList(FIRST_PAGE);
  }
  
  public findList(pageNumber: number, message: string = ""): void {
    this.accountPayableService.filter(pageNumber,
      this.itemsPerPage,
      this.codeFilter,
      this.personSearchFilter,
      this.chartOfAccountUidFilter,
      this.documentNumberFilter).subscribe(
        (data: PageResultDto<AutoAccountPayRecSearchDto>) => {
          this.alertMessage = null;
          this.accountPayablePageResult  = data;
          this.afterFilter(message);
        },
        error => {
          this.alertMessage = error;
        }
      );
    }
      
  private afterFilter(successMessage: string = "") {
    this.warningMessage = StringUtils.EMPTY;
    this.successMessage = successMessage;
    if (this.accountPayablePageResult && this.accountPayablePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public view(accountPayable: AutoAccountPayRecSearchDto): void {
    this.findAndOpenRegisterScreen(accountPayable, true);
  }

  public edit(accountPayable: AutoAccountPayRecSearchDto): void {
    try {
      this.isEditButtonDisabled = true;
      this.findAndOpenRegisterScreen(accountPayable, false);
    } finally {
      this.isEditButtonDisabled = false;
    }
  }

  public findAndOpenRegisterScreen(accountPayable: AutoAccountPayRecSearchDto, isViewMode: boolean): void {
    try {
      if (accountPayable) {
        this.accountPayableService.findByCode(accountPayable.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data, accountPayable, isViewMode);
          },
          error => {
            this.alertMessage = this.translateService.instant("ACCOUNT_PAYABLE.MESSAGE.COULD_NOT_OPEN") + ` (${error})`; 
          }
        );
      }
    } catch {
      throw new Error(this.translateService.instant("ACCOUNT_PAYABLE.MESSAGE.COULD_NOT_OPEN"));
    }
  }

  public openRegisterScreen(accountPayable: AutoAccountPayRecDto, accountPayableSearch: AutoAccountPayRecSearchDto, isViewMode: boolean) {
    const initialState = { existingAccountPayable: accountPayable, accountPayableSearch: accountPayableSearch, isViewMode: isViewMode };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AutoAccountPayableRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public openRemoveDialog(accountPayable: AutoAccountPayRecSearchDto) {
    if (accountPayable) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':accountPayable.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':accountPayable.person.name + " ($"+NumberUtils.currencyToStr(accountPayable.amount, this.currencyPipe)+")"}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(accountPayable);
          }
        });
    }
  }

  private remove(accountPayable: AutoAccountPayRecSearchDto) {
    this.accountPayableService.remove(accountPayable.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public formatDescription(description: string): string{
    return StringUtils.formatLongText(description);
  }
    
  public pageChanged(event: any): void {
    this.findList(event.page);
  }
  
  public isSelected(account: AutoAccountPayRecSearchDto): boolean {
    return this.selectedAccount?.code == account.code;
  }

  public isDisabled(accountPayable: AutoAccountPayRecSearchDto): boolean {
    return accountPayable.disabled;
  }

  public selectItem(account: AutoAccountPayRecSearchDto): void {
    this.selectedAccount = new AutoAccountPayRecSearchDto();
    if (account) {
      this.selectedAccount = account;
    }
  }

  public getTooltipSupplier(accountPayable: AutoAccountPayRecSearchDto): string {
    const codeLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.CODE');
    const businessNameLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.BUSINESS_NAME');
    const phoneLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.PHONE');
    let html = codeLabel+ ": " +accountPayable.person.code.toString();
    if (accountPayable.person.businessName) {
      html += "<br/>";
      html += businessNameLabel+ ": " +accountPayable.person.businessName;
    }
    if (accountPayable.person?.phones) {
      html += "<br/>";
      html += phoneLabel+ ": ";
      accountPayable.person.phones.forEach(element => {
        html += element.areaCode+"-"+element.phoneNumber + "  ";  
      });
      html = html.trim();
    }
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipBalance(accountPayable: AutoAccountPayRecSearchDto): string {
    const amountLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.AMOUNT');
    const discountLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.DISCOUNT');
    const interestLabel: string = this.translateService.instant('ACCOUNT_PAYABLE.SEARCH.TABLE.TOOLTIP.INTEREST');
    let html = amountLabel+ ": " +NumberUtils.currencyToStr(accountPayable.amount, this.currencyPipe);
    html += "<br/>";
    html += discountLabel+ ": " +NumberUtils.currencyToStr(accountPayable.discount, this.currencyPipe);
    html += "<br/>";
    html += interestLabel+ ": " +NumberUtils.currencyToStr(accountPayable.interest, this.currencyPipe);
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

}
