import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';
import { AutoAccountPayableRegisterComponent } from './auto-account-payable-register/auto-account-payable-register.component';
import { AutoAccountPayableRoutingModule } from './auto-account-payable-routing.module';
import { AutoAccountPayableSearchBarComponent } from './auto-account-payable-search/auto-account-payable-search-bar.component';
import { AutoAccountPayableSearchComponent } from './auto-account-payable-search/auto-account-payable-search.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { DataListModule } from '../component/data-list/data-list.module';
import { DataListLazyModule } from '../../base/component/data-list-lazy/data-list-lazy.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    AutoAccountPayableRoutingModule,
    ConfirmationDialogModule,
    DataListModule,
    DataListLazyModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    AutoAccountPayableRegisterComponent,
    AutoAccountPayableSearchComponent,
    AutoAccountPayableSearchBarComponent
  ]
})
export class AutoAccountPayableModule { }
