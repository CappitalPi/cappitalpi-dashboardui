import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AutoAccountReceivableRegisterComponent } from './auto-account-receivable-register.component';

describe('AutoAccountReceivableRegisterComponent', () => {
  let component: AutoAccountReceivableRegisterComponent;
  let fixture: ComponentFixture<AutoAccountReceivableRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoAccountReceivableRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAccountReceivableRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
