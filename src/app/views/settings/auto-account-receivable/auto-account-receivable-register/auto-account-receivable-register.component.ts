import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormValidationUtils } from '../../../../utils/form-validation.utils'
import { DataListViewModel } from '../../component/data-list/view-model/data-list-view-model';
import { DateUtils } from '../../../../utils/date.utils';
import { DataListDtoModel } from '../../component/data-list/view-model/data-list-dto-model';
import { CurrencyPipe } from '@angular/common';
import { NumberUtils } from '../../../../utils/number-utils';
import { DataListLazyDtoModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-dto-model';
import { PersonService } from '../../../../service/person/person.service';
import { PersonSearchDetailDto } from '../../../../dto/person/person-search-detail.dto';
import { DataListLazyViewModel } from '../../../base/component/data-list-lazy/view-model/data-list-lazy-view-model';
import { PersonDataListViewHelper } from '../../../person/view-model/person-data-list-view.helper';
import { ChartOfAccountService } from '../../../../service/chart-of-account/chart-of-account.service';
import { TransactionType } from '../../../../enum/transaction-type.enum';
import { ChartOfAccountDto } from '../../../../dto/chart-of-account/chart-of-account.dto';
import { PersistenceMode } from '../../../../strategy/persistence/persistence-mode.enum';
import { StringUtils } from '../../../../utils/string-utils';
import { ChartOfAccountDataListViewHelper } from '../../../financier/chart-of-account/view-model/chart-of-account-data-list-view.helper';
import { AutoAccountPayRecRequestDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-request.dto';
import { AutoAccountPayRecDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec.dto';
import { AutoAccountPayRecSearchDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-search.dto';
import { AutoAccountReceivablePersistenceContext } from '../../../../strategy/persistence/account-receivable/auto-account-receivable-persistence-context.strategy';
import { FrequencyType } from '../../../../dto/account-pay-rec/frequency-type.enum';
import { EnumUtils } from '../../../../utils/enum-utils';
import { RepeatType } from '../../../../dto/account-pay-rec/repeat-type.enum';
import { CostType } from '../../../../dto/account-pay-rec/cost-type.enum';

@Component({
  selector: 'app-auto-account-receivable-register',
  templateUrl: './auto-account-receivable-register.component.html',
  styleUrls: ['./auto-account-receivable-register.component.scss']
})
export class AutoAccountReceivableRegisterComponent implements OnInit {

  @ViewChild('form') form : NgForm;
  @Input() public isViewMode: boolean;
  @Input() public existingAccountReceivable: AutoAccountPayRecDto;
  @Input() public accountReceivableSearch: AutoAccountPayRecSearchDto;

  public accountReceivable: AutoAccountPayRecRequestDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;
  public frequencyStartDate : string;
  public frequencyEndDate : string;
  public latestDate : string;
  public amount : string;
  public discount : string;
  public interest : string;
  public total : string;
  public amountPaid : string;
  public amountToPay : string;
  public frequencyTypes: FrequencyType[];
  public repeatTypes: RepeatType[];
  public costTypes: CostType[];
  public selectedChartOfAccount: DataListViewModel;
  public selectedPerson: DataListLazyViewModel;
  public accountDataList: DataListDtoModel;
  public chartOfAccountDataList: DataListDtoModel;
  public personDataList: DataListLazyDtoModel;

  constructor(public modalRef: BsModalRef,
              private currencyPipe: CurrencyPipe,
              private accountReceivablePersistenceContext: AutoAccountReceivablePersistenceContext,
              private personService: PersonService,
              private chartOfAccountService: ChartOfAccountService,
              private personDataListLazyViewHelper: PersonDataListViewHelper,
              private chartOfAccountDataListViewHelper: ChartOfAccountDataListViewHelper) {
    this.frequencyStartDate = DateUtils.formatToShortDate(new Date());
    this.accountReceivable = new AutoAccountPayRecRequestDto();
    this.accountReceivable.amount = 0;
    this.accountReceivable.discount = 0;
    this.accountReceivable.interest = 0;
    this.amount = NumberUtils.currencyToStr(this.accountReceivable.amount, this.currencyPipe);
    this.discount = NumberUtils.currencyToStr(this.accountReceivable.discount, this.currencyPipe);
    this.interest = NumberUtils.currencyToStr(this.accountReceivable.interest, this.currencyPipe);
    this.calcAmount();
    this.selectedChartOfAccount = new DataListViewModel();
    this.selectedPerson = new DataListLazyViewModel();
  }
  
  ngOnInit(): void {
    if (this.existingAccountReceivable) {
      this.accountReceivable = new AutoAccountPayRecRequestDto();
      this.accountReceivable.uid = this.existingAccountReceivable.uid;
      this.accountReceivable.code = this.existingAccountReceivable.code;
      this.accountReceivable.registerUid = this.existingAccountReceivable.registerUid;
      this.accountReceivable.appAccountUid = this.existingAccountReceivable.appAccountUid;
      this.accountReceivable.personUid = this.existingAccountReceivable.personUid;
      this.accountReceivable.chartOfAccountUid = this.existingAccountReceivable.chartOfAccountUid;
      this.accountReceivable.description = this.existingAccountReceivable.description;
      this.accountReceivable.documentNumber = this.existingAccountReceivable.documentNumber;
      this.accountReceivable.frequencyType = this.existingAccountReceivable.frequencyType;
      this.accountReceivable.repeatType = this.existingAccountReceivable.repeatType;
      this.accountReceivable.costType = this.existingAccountReceivable.costType;
      this.accountReceivable.amount = this.existingAccountReceivable.amount;
      this.accountReceivable.discount = this.existingAccountReceivable.discount;
      this.accountReceivable.interest = this.existingAccountReceivable.interest;
      this.accountReceivable.frequencyStartDate = this.existingAccountReceivable.frequencyStartDate;
      this.accountReceivable.frequencyEndDate = this.existingAccountReceivable.frequencyEndDate;
      this.accountReceivable.frequencyAmount = this.existingAccountReceivable.frequencyAmount;
      this.accountReceivable.sunday = this.existingAccountReceivable.sunday;
      this.accountReceivable.monday = this.existingAccountReceivable.monday;
      this.accountReceivable.tuesday = this.existingAccountReceivable.tuesday;
      this.accountReceivable.wednesday = this.existingAccountReceivable.wednesday;
      this.accountReceivable.thursday = this.existingAccountReceivable.thursday;
      this.accountReceivable.friday = this.existingAccountReceivable.friday;
      this.accountReceivable.saturday = this.existingAccountReceivable.saturday;
      this.accountReceivable.skipWeekend = this.existingAccountReceivable.skipWeekend;
      this.accountReceivable.disabled = this.existingAccountReceivable.disabled;
      this.selectedPerson.uid = this.existingAccountReceivable.personUid;
      this.selectedChartOfAccount.uid = this.existingAccountReceivable.chartOfAccountUid;
      this.frequencyStartDate = DateUtils.formatToShortDate(this.existingAccountReceivable.frequencyStartDate);
      this.frequencyEndDate = DateUtils.formatToShortDate(this.existingAccountReceivable.frequencyEndDate);
      this.latestDate = DateUtils.formatToShortDate(this.existingAccountReceivable.latestDate);
      this.amount = NumberUtils.currencyToStr(this.existingAccountReceivable.amount, this.currencyPipe);
      this.discount = NumberUtils.currencyToStr(this.existingAccountReceivable.discount, this.currencyPipe);
      this.interest = NumberUtils.currencyToStr(this.existingAccountReceivable.interest, this.currencyPipe);
      this.calcAmount();
      const selectedPerson : PersonSearchDetailDto[] = [this.accountReceivableSearch.person]
      this.loadPerson(selectedPerson);
    }
    this.findFrequencyTypes();
    this.findRepeatTypes();
    this.findCostTypes();
    this.findChartOfAccounts();
  }

  public findFrequencyTypes(): void{
    if (!this.frequencyTypes) {
      this.frequencyTypes = EnumUtils.getList(FrequencyType, false);
    }
  }
  
  public findRepeatTypes(): void{
    if (!this.repeatTypes) {
      this.repeatTypes = EnumUtils.getList(RepeatType, false);
    }
  }
  
  public findCostTypes(): void{
    if (!this.costTypes) {
      this.costTypes = EnumUtils.getList(CostType, false);
    }
  }
  
  public findChartOfAccounts(): void{
    this.chartOfAccountService.findByType(TransactionType.CREDIT).subscribe(
      (data: ChartOfAccountDto[]) => {
        this.chartOfAccountDataList = new DataListDtoModel();
        this.chartOfAccountDataList.list = this.chartOfAccountDataListViewHelper.load(data);
        this.chartOfAccountDataList.selectedUid = this.selectedChartOfAccount.uid;
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  public chartOfAccountSelected($event): void {
    this.selectedChartOfAccount = new DataListViewModel();
    if ($event.selectedItems?.length > 0) {
      const isUnselectable = $event.selectedItems[0].unselectable
      if (!isUnselectable) {
        this.selectedChartOfAccount = $event.selectedItems[0];
      }
    }
  }

  public personSelected($event): void {
    this.selectedPerson = $event.selectedItem;
  }

  public filterPerson($event) {
    this.selectedPerson = new DataListLazyViewModel();
    this.personService.filter($event.searchTerm).subscribe(
      (data: PersonSearchDetailDto[]) => {
        this.loadPerson(data);
      },
      error => {
        this.alertMessage = error;//TODO
      }
    );
  }

  private loadPerson(data: PersonSearchDetailDto[]) {
    this.personDataList = new DataListLazyDtoModel();
    this.personDataList.list = this.personDataListLazyViewHelper.load(data);
    this.personDataList.selectedUid = this.selectedPerson.uid;
  }

  public formatNotes(notes: string): string{
    return StringUtils.formatLongText(notes);
  }

  /**
   * Método necessário pois existe um bug na diretiva ao incluir aos trilhões.
   * Por algum motivo, a aplicação está aceitando um dígito a mais.
   * TODO: Tentar corrigir a diretiva ou criar outra.
   * @param $event 
   */
  public onChangeAmount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.amount = $event.target.value;
    this.calcAmount();
  }

  public onChangeDiscount($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.discount = $event.target.value;
    this.calcAmount();
  }

  public onChangeInterest($event) {
    if (!$event.target?.value) {
      $event.target.value = 0;
    }
    this.interest = $event.target.value;
    this.calcAmount();
  }

  public calcAmount(): void {
    const total : number = NumberUtils.strToCurrency(this.amount) - NumberUtils.strToCurrency(this.discount) + NumberUtils.strToCurrency(this.interest);
    this.total = NumberUtils.currencyToStr(total, this.currencyPipe);

    const amountToPay : number = NumberUtils.strToCurrency(this.total) - NumberUtils.strToCurrency(this.amountPaid);
    this.amountToPay = NumberUtils.currencyToStr(amountToPay, this.currencyPipe);
  }

  public isNew(): boolean {
    return this.accountReceivable && !this.accountReceivable.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public persist(): void {
    try {
      this.disableSaveButton = true;
      this.submitted = false;
      this.alertMessage = null;
      if (!this.form.valid) {
        FormValidationUtils.markFieldsAsTouched(this.form);
        this.disableSaveButton = false;
        return;
      }
      this.accountReceivable.personUid = this.selectedPerson.uid;
      this.accountReceivable.chartOfAccountUid = this.selectedChartOfAccount.uid;
      this.accountReceivable.frequencyStartDate = DateUtils.strToDate(this.frequencyStartDate);
      this.accountReceivable.frequencyEndDate = DateUtils.strToDate(this.frequencyEndDate);
      this.accountReceivable.amount = NumberUtils.strToCurrency(this.amount);
      this.accountReceivable.discount = NumberUtils.strToCurrency(this.discount);
      this.accountReceivable.interest = NumberUtils.strToCurrency(this.interest);
      this.accountReceivable.frequencyAmount = this.accountReceivable.frequencyAmount ? Number(this.accountReceivable.frequencyAmount) : null;
      const mode = this.accountReceivable.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
      this.accountReceivablePersistenceContext.context(mode).persist(this.accountReceivable).subscribe(
        data => {
          this.close();
        },
        error => {
          this.disableSaveButton = false;
          this.alertMessage = error;
        }
      );
        this.submitted = true;
    } catch (e) {
      this.submitted = false;
      this.alertMessage = e.message;
    } finally {
      this.disableSaveButton = false;
    }  
  }
  
  public isUpdate(): boolean {
    return this.accountReceivable?.uid?.length > 0
  }

  public showFrequencyAmount(): boolean {
    if (this.accountReceivable?.frequencyType) {
      return RepeatType.BY_AMOUNT_OF_DAYS == this.accountReceivable.repeatType;
    }
    return false;
  }

  public showWeekDays(): boolean {
    return FrequencyType.DAY == this.accountReceivable?.frequencyType && RepeatType.BY_DUE_DATE == this.accountReceivable.repeatType;
  }

  public close(): void {
    if (this.modalRef && this.modalRef.content && this.modalRef.content.submitted) {
      this.modalRef.content.submitted = this.submitted;
    }
    this.modalRef.hide();
  }

}
  
  