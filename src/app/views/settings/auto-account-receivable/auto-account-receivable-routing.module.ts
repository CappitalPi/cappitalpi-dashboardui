import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AutoAccountReceivableSearchComponent } from './auto-account-receivable-search/auto-account-receivable-search.component';

const routes: Routes = [
  {
    path: '',
    component: AutoAccountReceivableSearchComponent,
    data: {
      title: 'APP.MENU.SETTINGS.ACCOUNTS-RECEIVABLE'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutoAccountReceivableRoutingModule {

  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
