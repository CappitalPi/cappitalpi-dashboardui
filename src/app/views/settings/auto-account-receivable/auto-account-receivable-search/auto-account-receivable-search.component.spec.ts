import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AutoAccountReceivableSearchComponent } from './auto-account-receivable-search.component';


describe('AutoAccountReceivableSearchComponent', () => {
  let component: AutoAccountReceivableSearchComponent;
  let fixture: ComponentFixture<AutoAccountReceivableSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutoAccountReceivableSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoAccountReceivableSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
