import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../../dto/page-result.dto';
import { NumberUtils } from '../../../../utils/number-utils';
import { StringUtils } from '../../../../utils/string-utils';
import { AutoAccountReceivableRegisterComponent } from '../auto-account-receivable-register/auto-account-receivable-register.component';
import { AutoAccountPayRecSearchDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec-search.dto';
import { AutoAccountReceivableService } from '../../../../service/account-receivable/auto-account-receivable.service';
import { AutoAccountPayRecDto } from '../../../../dto/account-pay-rec/auto-account-pay-rec.dto';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-auto-account-receivable-search',
  templateUrl: './auto-account-receivable-search.component.html',
  styleUrls: ['./auto-account-receivable-search.component.scss']
})
export class AutoAccountReceivableSearchComponent implements OnInit {

  public isEditButtonDisabled : boolean = false;
  public alertMessage : string;
  public warningMessage : string;
  public successMessage : string;
  public accountReceivablePageResult : PageResultDto<AutoAccountPayRecSearchDto>;
  public codeFilter: number;
  public personSearchFilter: string;
  public chartOfAccountUidFilter: string;
  public documentNumberFilter: string
  public itemsPerPage: number = 20;
  public selectedAccount: AutoAccountPayRecSearchDto;
  public tooltipAmountDetail: string;
  public tooltipSupplierDetail: string;

  constructor(private modalService: BsModalService,
              private currencyPipe: CurrencyPipe,
              private accountReceivableService: AutoAccountReceivableService,
              private translateService: TranslateService,
              private sanitizer: DomSanitizer) {
    this.accountReceivablePageResult = new PageResultDto<AutoAccountPayRecSearchDto>();
    this.accountReceivablePageResult.totalElements = 0;
    this.selectedAccount = new AutoAccountPayRecSearchDto();
  }

  ngOnInit(): void {
  }

  
  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.codeFilter = $event.codeFilter;
    this.personSearchFilter = $event.personSearchFilter;
    this.chartOfAccountUidFilter = $event.chartOfAccountUidFilter;
    this.documentNumberFilter = $event.documentNumberFilter;
    this.findList(FIRST_PAGE);
  }
  
  public findList(pageNumber: number, message: string = ""): void {
    this.accountReceivableService.filter(pageNumber,
      this.itemsPerPage,
      this.codeFilter,
      this.personSearchFilter,
      this.chartOfAccountUidFilter,
      this.documentNumberFilter).subscribe(
        (data: PageResultDto<AutoAccountPayRecSearchDto>) => {
          this.alertMessage = null;
          this.accountReceivablePageResult  = data;
          this.afterFilter(message);
        },
        error => {
          this.alertMessage = error;
        }
      );
    }
      
  private afterFilter(successMessage: string = "") {
    this.warningMessage = StringUtils.EMPTY;
    this.successMessage = successMessage;
    if (this.accountReceivablePageResult && this.accountReceivablePageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public view(accountReceivable: AutoAccountPayRecSearchDto): void {
    this.findAndOpenRegisterScreen(accountReceivable, true);
  }

  public edit(accountReceivable: AutoAccountPayRecSearchDto): void {
    try {
      this.isEditButtonDisabled = true;
      this.findAndOpenRegisterScreen(accountReceivable, false);
    } finally {
      this.isEditButtonDisabled = false;
    }
  }

  public findAndOpenRegisterScreen(accountReceivable: AutoAccountPayRecSearchDto, isViewMode: boolean): void {
    try {
      if (accountReceivable) {
        this.accountReceivableService.findByCode(accountReceivable.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data, accountReceivable, isViewMode);
          },
          error => {
            this.alertMessage = this.translateService.instant("ACCOUNT_RECEIVABLE.MESSAGE.COULD_NOT_OPEN") + ` (${error})`; 
          }
        );
      }
    } catch {
      throw new Error(this.translateService.instant("ACCOUNT_RECEIVABLE.MESSAGE.COULD_NOT_OPEN"));
    }
  }

  public openRegisterScreen(accountReceivable: AutoAccountPayRecDto, accountReceivableSearch: AutoAccountPayRecSearchDto, isViewMode: boolean) {
    const initialState = { existingAccountReceivable: accountReceivable, accountReceivableSearch: accountReceivableSearch, isViewMode: isViewMode };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static', keyboard: false };
    const registerModalRef = this.modalService.show(AutoAccountReceivableRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public openRemoveDialog(accountReceivable: AutoAccountPayRecSearchDto) {
    if (accountReceivable) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':accountReceivable.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':accountReceivable.person.name + " ($"+NumberUtils.currencyToStr(accountReceivable.amount, this.currencyPipe)+")"}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(accountReceivable);
          }
        });
    }
  }

  private remove(accountReceivable: AutoAccountPayRecSearchDto) {
    this.accountReceivableService.remove(accountReceivable.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public formatDescription(description: string): string{
    return StringUtils.formatLongText(description);
  }
  
  public pageChanged(event: any): void {
    this.findList(event.page);
  }
  
  public isSelected(account: AutoAccountPayRecSearchDto): boolean {
    return this.selectedAccount?.code == account.code;
  }

  public isDisabled(accountReceivable: AutoAccountPayRecSearchDto): boolean {
    return accountReceivable.disabled;
  }

  public selectItem(account: AutoAccountPayRecSearchDto): void {
    this.selectedAccount = new AutoAccountPayRecSearchDto();
    if (account) {
      this.selectedAccount = account;
    }
  }

  public getTooltipSupplier(accountReceivable: AutoAccountPayRecSearchDto): string {
    const codeLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.CODE');
    const businessNameLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.BUSINESS_NAME');
    const phoneLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.PHONE');
    let html = codeLabel+ ": " +accountReceivable.person.code.toString();
    if (accountReceivable.person.businessName) {
      html += "<br/>";
      html += businessNameLabel+ ": " +accountReceivable.person.businessName;
    }
    if (accountReceivable.person?.phones) {
      html += "<br/>";
      html += phoneLabel+ ": ";
      accountReceivable.person.phones.forEach(element => {
        html += element.areaCode+"-"+element.phoneNumber + "  ";  
      });
      html = html.trim();
    }
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

  public getTooltipBalance(accountReceivable: AutoAccountPayRecSearchDto): string {
    const amountLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.AMOUNT');
    const discountLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.DISCOUNT');
    const interestLabel: string = this.translateService.instant('ACCOUNT_RECEIVABLE.SEARCH.TABLE.TOOLTIP.INTEREST');
    let html = amountLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.amount, this.currencyPipe);
    html += "<br/>";
    html += discountLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.discount, this.currencyPipe);
    html += "<br/>";
    html += interestLabel+ ": " +NumberUtils.currencyToStr(accountReceivable.interest, this.currencyPipe);
    html = html.trim();
    return this.sanitizer.sanitize(SecurityContext.HTML, `<span class="table-tooltip">${html}</span>`);
  }

}
