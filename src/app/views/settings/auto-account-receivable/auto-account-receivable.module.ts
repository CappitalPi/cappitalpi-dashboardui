import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DirectivesModule } from '../../../directive/directives.module';
import { AutoAccountReceivableRegisterComponent } from './auto-account-receivable-register/auto-account-receivable-register.component';
import { AutoAccountReceivableRoutingModule } from './auto-account-receivable-routing.module';
import { AutoAccountReceivableSearchBarComponent } from './auto-account-receivable-search/auto-account-receivable-search-bar.component';
import { AutoAccountReceivableSearchComponent } from './auto-account-receivable-search/auto-account-receivable-search.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { DataListModule } from '../component/data-list/data-list.module';
import { DataListLazyModule } from '../../base/component/data-list-lazy/data-list-lazy.module';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    DirectivesModule,
    AutoAccountReceivableRoutingModule,
    ConfirmationDialogModule,
    DataListModule,
    DataListLazyModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    AutoAccountReceivableRegisterComponent,
    AutoAccountReceivableSearchComponent,
    AutoAccountReceivableSearchBarComponent
  ]
})
export class AutoAccountReceivableModule { }
