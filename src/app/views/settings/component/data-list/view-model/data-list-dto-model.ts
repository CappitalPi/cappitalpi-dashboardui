import { DataListViewModel } from "./data-list-view-model";

export class DataListDtoModel {
    list: DataListViewModel[];
    selectedUid: string;
}

