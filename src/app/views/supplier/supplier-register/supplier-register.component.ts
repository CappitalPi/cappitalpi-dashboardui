import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LegalPersonType } from '../../../dto/person/legal-person-type.enum';
import { LegalPersonDto } from '../../../dto/person/legal-person.dto';
import { MaritalStatus } from '../../../dto/person/marital-status.enum';
import { PersonType } from '../../../dto/person/person-type.enum';
import { PersonDto } from '../../../dto/person/person.dto';
import { PhysicalPersonDto } from '../../../dto/person/physical-person.dto';
import { Sex } from '../../../dto/person/sex.enum';
import { AddressViewModelHelper } from '../../address/view-model/address-view-model.helper';
import { ContactViewModelHelper } from '../../contact/view-model/contact-view-model.helper';
import { EmailViewModelHelper } from '../../email/view-model/email-view-model.helper';
import { InscriptionViewModelHelper } from '../../inscription/view-model/inscription-view-model.helper';
import { PersonHelper } from '../../../helper/person/person.helper';
import { PhoneViewModelHelper } from '../../phone/view-model/phone-view-model.helper';
import { SupplierPersistenceContext } from '../../../strategy/persistence/supplier/supplier-persistence-context.strategy';
import { PersistenceMode } from '../../../strategy/persistence/persistence-mode.enum';
import { EnumUtils } from '../../../utils/enum-utils';
import { FormValidationUtils } from '../../../utils/form-validation.utils'
import { AddressViewModel } from '../../address/view-model/address-view-model';
import { ContactViewModel } from '../../contact/view-model/contact-view-model';
import { EmailViewModel } from '../../email/view-model/email-view-model';
import { InscriptionViewModel } from '../../inscription/view-model/inscription-view-model';
import { PhoneViewModel } from '../../phone/view-model/phone-view-model';

@Component({
  selector: 'app-supplier-register',
  templateUrl: './supplier-register.component.html',
  styleUrls: ['./supplier-register.component.scss']
})
export class SupplierRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingSupplier: PersonDto;

  public supplier: PersonDto;
  private legalPerson: LegalPersonDto;
  private physicalPerson: PhysicalPersonDto;
  public personTypes: PersonType[];
  private legalPersonTypes: LegalPersonType[];
  private sexs: Sex[];
  private maritalStatuses: MaritalStatus[];
  public addressViewModelList: AddressViewModel[];
  public phoneViewModelList: PhoneViewModel[]; 
  public emailViewModelList: EmailViewModel[];
  public inscriptionViewModelList: InscriptionViewModel[];
  public contactViewModelList: ContactViewModel[];

  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private supplierPersistenceContext: SupplierPersistenceContext) {
    this.supplier = new PersonDto();
    this.supplier.type = PersonType.LEGAL_PERSON;
    this.legalPerson = new LegalPersonDto();
    this.physicalPerson = new PhysicalPersonDto();
    this.addressViewModelList = [];
    this.phoneViewModelList = [];
    this.emailViewModelList = [];
    this.inscriptionViewModelList = [];
    this.contactViewModelList = [];
  }
  
  ngOnInit(): void {
    if (this.existingSupplier) {
      this.supplier = this.existingSupplier;
      if (PersonType.LEGAL_PERSON == this.supplier.type) {
        this.legalPerson = this.supplier.detail as LegalPersonDto;
      }
      if (PersonType.PHYSICAL_PERSON == this.supplier.type) {
        this.physicalPerson = this.supplier.detail as PhysicalPersonDto;
      }
      this.contactViewModelList = ContactViewModelHelper.loadModelList(this.supplier.contacts);
      this.addressViewModelList = AddressViewModelHelper.loadModelList(this.supplier.addresses);
      this.inscriptionViewModelList = InscriptionViewModelHelper.loadModelList(this.supplier.inscriptions);
      this.emailViewModelList = EmailViewModelHelper.loadModelList(this.supplier.emails, this.contactViewModelList);
      this.phoneViewModelList = PhoneViewModelHelper.loadModelList(this.supplier.phones, this.contactViewModelList, this.addressViewModelList);
    }
    this.findPersonTypes();
    this.findLegalPersonTypes();
    this.findSexs();
    this.findMaritalStatus();
  }

  public findPersonTypes(): void{
    if (!this.personTypes) {
      this.personTypes = EnumUtils.getList(PersonType, false);
    }
  }

  public findLegalPersonTypes(): void {
    if (!this.legalPersonTypes) {
      this.legalPersonTypes = EnumUtils.getList(LegalPersonType, false);
    }
  }

  public findSexs(): void {
    if (!this.sexs) {
      this.sexs = EnumUtils.getList(Sex, false);
    }
  }

  public findMaritalStatus(): void {
    if (!this.maritalStatuses) {
      this.maritalStatuses = EnumUtils.getList(MaritalStatus, false);
    }
  }
  
  ngAfterViewInit(): void {
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public isPhysicalPerson() {
    return this.supplier && PersonType.PHYSICAL_PERSON == this.supplier.type;
  }

  public isNew(): boolean {
    return this.supplier && !this.supplier.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }

  public refreshAddress($event): void {
    this.addressViewModelList = $event.addressViewModelList;
  }

  public refreshPhones($event): void {
    this.phoneViewModelList = $event.phoneViewModelList;
    if (this.phoneViewModelList?.length > 0) {
      this.phoneViewModelList.forEach(element => {
        if (element) {
          this.updateContactViewModelList(element.contactModel);
        }
      });
    }
  }

  public refreshEmails($event): void {
    this.emailViewModelList = $event.emailViewModelList;
    if (this.emailViewModelList?.length > 0) {
      this.emailViewModelList.forEach(element => {
        if (element) {
          this.updateContactViewModelList(element.contactModel);
        }
      });
    }
  }

  public refreshInscriptions($event): void {
    this.inscriptionViewModelList = $event.inscriptionViewModelList;
  }

  public refreshContacts($event): void {
    this.contactViewModelList = $event.contactViewModelList;
    EmailViewModelHelper.refreshList(this.emailViewModelList, this.contactViewModelList);
    PhoneViewModelHelper.refreshList(this.phoneViewModelList, this.contactViewModelList, this.addressViewModelList);
  }

  private updateContactViewModelList(model: ContactViewModel): void {
    if (model) {
      const newContact = ContactViewModelHelper.findItemValue(this.contactViewModelList, model.code, null);
      if (!newContact) {
        if (!this.contactViewModelList) {
          this.contactViewModelList = [];
        }
        ContactViewModelHelper.addToModelList(model, this.contactViewModelList);
      }
    }
  }

  //TODO: Erro ao atualizar o tipo de pessoa (Pessoa fisica para pessoa juridica)
  // Rever campo tipo empresa: Uma empresa com vários endereços terá o mesmo tipo? 
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    this.supplier.emails = EmailViewModelHelper.getEmails(this.emailViewModelList);
    this.supplier.phones = PhoneViewModelHelper.getPhones(this.phoneViewModelList);
    this.supplier.contacts = ContactViewModelHelper.getContacts(this.contactViewModelList);
    this.supplier.addresses = AddressViewModelHelper.getAddresses(this.addressViewModelList);
    this.supplier.inscriptions = InscriptionViewModelHelper.getInscriptions(this.inscriptionViewModelList);
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    const mode = this.supplier.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.supplier = PersonHelper.personByType(this.supplier, this.legalPerson, this.physicalPerson);
    this.supplierPersistenceContext.context(mode).persist(this.supplier).subscribe(
      data => {
        console.debug(`Cliente criado com sucesso: ${data}`);
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
        console.error(`Erro na criação de cliente: ${error}`)
      }
      );
      this.submitted = true;
  }
    
  public close(): void {
    this.modalRef.content.submitted = this.submitted;
    this.modalRef.hide();
  }
}
  
  