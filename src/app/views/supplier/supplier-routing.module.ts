import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SupplierSearchComponent } from './supplier-search/supplier-search.component';

const routes: Routes = [
  {
    path: '',
    component: SupplierSearchComponent,
    data: {
      title: 'APP.MENU.PERSON.SUPPLIERS'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierRoutingModule {
  
  constructor(private translateService: TranslateService) {
    routes.forEach(element => {
      element.data.title = this.translateService.instant(element.data.title);
    });
  }

}
