import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../dto/page-result.dto';
import { PersonSearchDto } from '../../../dto/person/person-search.dto';
import { PersonDto } from '../../../dto/person/person.dto';
import { SupplierService } from '../../../service/supplier/supplier.service';
import { SupplierRegisterComponent } from '../supplier-register/supplier-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-supplier-search',
  templateUrl: './supplier-search.component.html',
  styleUrls: ['./supplier-search.component.scss']
})
export class SupplierSearchComponent implements OnInit {

  public itemsPerPage: number = 20;
  public isEditButtonDisabled = false;
  public alertMessage : string;
  public warningMessage : string;
  public supplierPageResult : PageResultDto<PersonSearchDto>;
  public codeFilter: number;
  public supplierNameFilter: string;

  constructor(private modalService: BsModalService, 
              private supplierService: SupplierService,
              private translateService: TranslateService) {
    this.supplierPageResult = new PageResultDto<PersonSearchDto>();
  }

  ngOnInit(): void {
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.supplierNameFilter = $event.suppliernameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.supplierService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.supplierNameFilter).subscribe(
      (data: PageResultDto<PersonSearchDto>) => {
        this.alertMessage = null;
        this.supplierPageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.supplierPageResult && this.supplierPageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(supplier: PersonDto) {
    const initialState = { existingSupplier: supplier };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(SupplierRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
  }

  public edit(supplier: PersonSearchDto) {
    try {
      this.isEditButtonDisabled = true;
      if (supplier) {
        this.supplierService.findByCode(supplier.code).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data);
            this.isEditButtonDisabled = false;
            },
          error => {
            this.alertMessage = error; 
            this.isEditButtonDisabled = false;
          }
        );
      }
    } catch {
      this.isEditButtonDisabled = false;
    }
  }

  public openRemoveDialog(supplier: PersonSearchDto) {
    if (supplier) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_TITLE', {'code':supplier.code}), 
        message: this.translateService.instant('APP.DIALOG.DISABLE_RECORD_MESSAGE', {'info':supplier.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.disable(supplier);
          }
        });
    }
  }

  public disableOrEnable(supplier: PersonSearchDto): void {
    if (supplier.disabled) {
      this.enable(supplier)
    } else {
      this.openRemoveDialog(supplier);
    }
  }

  private disable(supplier: PersonSearchDto) {
    this.supplierService.disable(supplier.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  private enable(supplier: PersonSearchDto) {
    this.supplierService.enable(supplier.code).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

}
