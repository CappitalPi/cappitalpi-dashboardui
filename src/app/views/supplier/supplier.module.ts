import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SupplierRoutingModule } from './supplier-routing.module';
import { SupplierSearchComponent } from './supplier-search/supplier-search.component';
import { SupplierRegisterComponent } from './supplier-register/supplier-register.component';
import { SupplierSearchBarComponent } from './supplier-search/supplier-search-bar.component';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogModule } from '../../containers/default-layout/confirmation-dialog/confirmation-dialog.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AddressModule } from '../address/address.module';
import { PhoneModule } from '../phone/phone.module';
import { EmailModule } from '../email/email.module';
import { InscriptionModule } from '../inscription/inscription.module';
import { ContactModule } from '../contact/contact.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    TabsModule,
    SupplierRoutingModule,
    ConfirmationDialogModule,
    AddressModule,
    PhoneModule,
    EmailModule,
    InscriptionModule,
    ContactModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [ 
    SupplierSearchComponent,
    SupplierSearchBarComponent,
    SupplierRegisterComponent
  ]
})
export class SupplierModule { }
