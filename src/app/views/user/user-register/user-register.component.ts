import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserDto } from '../../../dto/user/user.dto';
import { UserPersistenceContext } from '../../../strategy/persistence/user/user-persistence-context.strategy';
import { PersistenceMode } from '../../../strategy/persistence/persistence-mode.enum';
import { FormValidationUtils } from '../../../utils/form-validation.utils'

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit, AfterViewInit {

  @ViewChild('form') form : NgForm;
  @ViewChild("nameInput") nameInput : ElementRef;
  @Input() public existingUser: UserDto;

  public user: UserDto;
  public submitted : boolean = false;
  public alertMessage : string;
  public disableSaveButton: boolean = false;

  constructor(public modalRef: BsModalRef, private userPersistenceContext: UserPersistenceContext) {
    this.user = new UserDto();
  }
  
  ngOnInit(): void {
    if (this.existingUser) {
      this.user = this.existingUser;
    }
  }
  
  ngAfterViewInit(): void {
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.nameInput.nativeElement.focus();
    },0);  
  }

  public isNew(): boolean {
    return this.user && !this.user.uid;
  }
  
  public isFieldInvalid(field: FormControl): boolean {
    return FormValidationUtils.isFieldInvalid(field);
  }
  
  public persist(): void {
    this.disableSaveButton = true;
    this.submitted = false;
    this.alertMessage = null;
    if (!this.form.valid) {
      FormValidationUtils.markFieldsAsTouched(this.form);
      this.disableSaveButton = false;
      return;
    }
    const mode = this.user.uid ? PersistenceMode.UPDATE : PersistenceMode.CREATE;
    this.userPersistenceContext.context(mode).persist(this.user).subscribe(
      data => {
        console.debug(`Usuário criado com sucesso: ${data}`);
        this.close();
      },
      error => {
        this.disableSaveButton = false;
        this.alertMessage = error;
        console.error(`Erro na criação de usuário: ${error}`)
      }
      );
      this.submitted = true;
    }
    
    public close(): void {
      this.modalRef.content.submitted = this.submitted;
      this.modalRef.hide();
    }
  }
  