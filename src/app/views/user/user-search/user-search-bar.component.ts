import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PageResultDto } from '../../../dto/page-result.dto';
import { UserSearchDto } from '../../../dto/user/user-search.dto';
import { FormValidationUtils } from '../../../utils/form-validation.utils';

@Component({
  selector: 'app-user-search-bar',
  templateUrl: './user-search-bar.component.html',
  styleUrls: ['./user-search.component.scss']
})
export class UserSearchBarComponent implements OnInit {

  public codeFilter: number;
  public usernameFilter: string;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  @Output() paramsEmiter = new EventEmitter<{codeFilter: number, usernameFilter: string}>();

  constructor() { }

  ngOnInit(): void {
  }

  public clear(): void {
    this.codeFilter = null;
    this.usernameFilter = null;
    this.search();
  }

  public search(): void {
    this.paramsEmiter.emit(
      {
        codeFilter: this.codeFilter, 
        usernameFilter: this.usernameFilter
      }
    );
  }

  collapsed(event: any): void {
    console.debug(event);
  }

  expanded(event: any): void {
    console.debug(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

  public keyPressNumbers(event) {
    return FormValidationUtils.keyPressNumbers(event);
  }

}
