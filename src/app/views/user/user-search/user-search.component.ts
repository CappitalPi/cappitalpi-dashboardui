import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../../containers/default-layout/confirmation-dialog/confirmation-dialog.component';
import { PageResultDto } from '../../../dto/page-result.dto';
import { UserSearchDto } from '../../../dto/user/user-search.dto';
import { UserDto } from '../../../dto/user/user.dto';
import { SessionService } from '../../../service/auth/session-service';
import { UserSession } from '../../../service/auth/user-session';
import { UserService } from '../../../service/user/user.service';
import { UserRegisterComponent } from '../user-register/user-register.component';

const FIRST_PAGE = 0;

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss']
})
export class UserSearchComponent implements OnInit {

  public alertMessage : string;
  public warningMessage : string;
  private userSession : UserSession;
  public itemsPerPage: number = 20;
  public userPageResult : PageResultDto<UserSearchDto>;
  public codeFilter: number;
  public usernameFilter: string;

  constructor(private modalService: BsModalService, 
              private userService: UserService,
              private sessionService: SessionService,
              private translateService: TranslateService) {
    this.userPageResult = new PageResultDto<UserSearchDto>();
  }

  ngOnInit(): void {
    this.userSession = this.sessionService.getUser();
    this.findList(FIRST_PAGE);
  }

  public filter($event) {
    this.codeFilter = $event.codeFilter;
    this.usernameFilter = $event.usernameFilter;
    this.findList(FIRST_PAGE);
  }

  private findList(pageNumber: number): void {
    this.userService.filter(pageNumber, this.itemsPerPage, this.codeFilter, this.usernameFilter).subscribe(
      (data: PageResultDto<UserSearchDto>) => {
        this.alertMessage = null;
        this.userPageResult  = data;
        this.afterFilter();
      },
      error => {
        this.alertMessage = error;
      }
    );
  }

  private afterFilter() {
    this.warningMessage = "";
    if (this.userPageResult && this.userPageResult.totalElements <= 0) {
      this.warningMessage = this.translateService.instant("APP.WARNING.DATA_NOT_FOUND");
    } 
  }

  public openRegisterScreen(user: UserDto) {
    const initialState = { existingUser: user };
    const modalConfig: {} = { class: 'modal-lg', backdrop: 'static' };
    const registerModalRef = this.modalService.show(UserRegisterComponent, Object.assign( {}, modalConfig, {initialState} ));
    registerModalRef.onHide.pipe(take(1)).subscribe(
      () => {
        if (registerModalRef.content.submitted) {
          this.findList(FIRST_PAGE);
        }
      });
    }

    public edit(user: UserSearchDto) {
      if (user) {
        this.userService.findById(user.uid).subscribe(
          data => {
            this.alertMessage = null;
            this.openRegisterScreen(data);
           },
          error => {
            this.alertMessage = error; 
          }
        );
      }
    }

  public openRemoveDialog(user: UserSearchDto) {
    if (user) {
      const initialState = { 
        title: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_TITLE', {'code':user.code}), 
        message: this.translateService.instant('APP.DIALOG.REMOVE_RECORD_MESSAGE', {'info':user.name}), 
        okButtonLabel: this.translateService.instant('APP.ACTION.YES'),
        closeButtonLabel: this.translateService.instant('APP.ACTION.NO')
      };
      const modalConfig: {} = { class: 'modal-md, modal-danger' };
      const removeModalRef = this.modalService.show(ConfirmationDialogComponent, Object.assign( {}, modalConfig, {initialState} ));
      removeModalRef.onHide.pipe(take(1)).subscribe(
        () => {
          if (removeModalRef.content.isOk) {
            this.remove(user);
          }
        });
    }
  }

  private remove(user: UserSearchDto) {
    this.userService.remove(user.uid).subscribe(
      () => {
        this.alertMessage = null;
        this.findList(FIRST_PAGE);
       },
      error => {
        this.alertMessage = error; 
      }
    );
  }

  public canRemove(userSearch: UserSearchDto): boolean {
    return this.userSession && userSearch && this.userSession.code != userSearch.code;  
  }

  public pageChanged(event: any): void {
    this.findList(event.page);
  }

}
