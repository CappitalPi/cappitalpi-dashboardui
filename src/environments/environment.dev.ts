// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const server = {
  host: 'datasoft-qa',
  port: '8443',
  endpoint: '/'
}

export const environment = {
  api_key: 'asdhgfhjsgdfjhagsjhfgahsjdgfjhasgdfjhgasd',
  production: false,
  port: server.port,
  serverHost: server.host,
  serverPort: server.port,
  serverUrl: 'https://'+server.host+':'+server.port+server.endpoint
};
